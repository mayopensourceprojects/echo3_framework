/**
 * Component rendering peer: PDWindow.
 * This class should not be extended by developers, the implementation is subject to change.
 */
 
if (!Core.get(window, ["PD", "Sync"])) {
        Core.set(window, ["PD", "Sync"], {});
}
 
PD.PDWindowPane = Core.extend(Echo.WindowPane, {
		$load : function() {
        	Echo.ComponentFactory.registerType("PDWindowPane", this);
		},
		componentType : "PDWindowPane",
		_sidebarDiv: null,
		_toolbarDiv: null,
	    _sidebarShown: false,
	    _sidebarExpanded: false,
	    _sidebarWidth: null,
		_toolbar: null,
		_sidebar: null
});
 
 
PD.Sync.PDWindowPane = Core.extend(Echo.Sync.WindowPane, {

	    $static: {
        	SIDEBAR_PROPERTIES: { sidebarExpanded: true },
        	
    	},
    	
		$load : function() {
        	Echo.Render.registerPeer("PDWindowPane", this);
		},
		
//		_bbbRef: null,
//		
//	    $construct: function() {
//	        this._bbbRef = Core.method(this, this._bbb);
//	    },
    
        /** this method overrides WindowPane.renderAdd */
		renderAdd: function(update, parentElement) {

			if (!this._toolbar)  {
				this._toolbar = this.component.getComponent(0);
				this._sidebar = this.component.getComponent(1);
			}
	        
	        // WindowPane only allows 1 element, so we must remove both toolbar and sidebar before 
	        // rendering the window to avoid the "Too many children" error.
	        // Also, we must remove the components using the remove method and not directly from the 
	        // component's children as before, that way the id's are regenerated when they are re-added,
	        // avoiding the "Component already exists with id" error.
	        this.component.remove(this._toolbar);
	        this.component.remove(this._sidebar);
	        
	        //call the super method
			Echo.Sync.WindowPane.prototype.renderAdd.call(this, update, parentElement);
	        
	        // Re-adding components, it's important that they are added to those specific indexes
			// This keeps the components in the hierarchy and regenerate their respective id's.
	        this.component.add(this._toolbar, 0);
	        this.component.add(this._sidebar, 1);
	        
	        //add the header toolbar
	        this._toolbarDiv = document.createElement("div");
			this._toolbarDiv.style.cssText = "float:right;margin-right:12px;overflow:hidden;";
			this._toolbarDiv.style.zIndex = 10;
			this._controlDiv.appendChild(this._toolbarDiv);
			Echo.Render.renderComponentAdd(update, this._toolbar, this._toolbarDiv);			
			//work-around. otherwise the child events will not work,
			//as they are inhibited in _processTitleBarMouseDown()
        		this._toolbarDiv._controlData = {};
        	
			//add the sidebar	
			this._sidebarDiv = document.createElement("div");
			this._sidebarDiv.style.cssText = "margin-top:10px;position:absolute;z-index:11;overflow:hidden;";
			this._contentDiv.appendChild(this._sidebarDiv);
			Echo.Render.renderComponentAdd(update, this._sidebar, this._sidebarDiv);  
				
			this._sidebarWidth = 200;
			var measuredWidth = new Core.Web.Measure.Bounds(this._sidebarDiv).width;
            if (measuredWidth && measuredWidth > 45) {
            	this._sidebarWidth = measuredWidth - 22;
				this._sidebarDiv.style.marginLeft = (0 - this._sidebarWidth) + "px";  
      		} else {
      			//sidebar is hidden
				this._sidebarDiv.style.marginLeft = "-100px";  
      		}
		},
		
		renderDisplay: function() {
	        //call the super method
			Echo.Sync.WindowPane.prototype.renderDisplay.call(this);
			
			// I'm not sure this is needed, but it's not currently hurting the application 
			Echo.Render.renderComponentDisplay(this._toolbar);
			Echo.Render.renderComponentDisplay(this._sidebar);	        
    	},
		
		/** @see Echo.Render.ComponentSync#renderUpdate */
	    renderUpdate: function(update) {
	        if (update.hasAddedChildren() || update.hasRemovedChildren()) {
	            // Children added/removed: perform full render.
	        } else if (update.isUpdatedPropertySetIn(Echo.Sync.WindowPane.NON_RENDERED_PROPERTIES)) {
	            // Do nothing.
	            return false;
	        } else if (update.isUpdatedPropertySetIn(Echo.Sync.WindowPane.PARTIAL_PROPERTIES_POSITION_SIZE)) {
	            this._loadPositionAndSize();
	            return false;
	        } else if (update.isUpdatedPropertySetIn(Echo.Sync.WindowPane.PARTIAL_PROPERTIES)) {
	            this._renderUpdateFrame();
	            return false;
	        } else if (update.isUpdatedPropertySetIn(PD.Sync.PDWindowPane.SIDEBAR_PROPERTIES)) {
				this._renderSidebarExpanded();
				return false;
	        }

	        var element = this._div;
	        var containerElement = element.parentNode;
	        Echo.Render.renderComponentDispose(update, update.parent);
	        containerElement.removeChild(element);
	        this.renderAdd(update, containerElement);
	        return true;
	    },
		
		renderDispose: function(update) {
	        this._overlayRemove();
            Core.Web.Event.removeAll(this._toolbarDiv);
            
    	    		this._renderDisposeFrame();
	        this._maskDiv = null;
    	    		this._contentDiv = null;
    	    		this._controlDiv = null;
	        this._toolbarDiv = null;
	        this._sidebarDiv = null;
	        this._toolbar = null;
	        this._sidebar = null;
	    },
	    
		_renderSidebarExpanded: function() {
			this._sidebarExpanded = this.component.render("sidebarExpanded", false);			
            //var fadeAnimation = new PD.Sync.PDWindowPane.FadeAnimation(this);
   	        //fadeAnimation.start();   	       	
		}
});
