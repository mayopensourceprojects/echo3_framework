/**
 * A button which copies a text to the clipboard
 */
 
if (!Core.get(window, ["PD", "Sync"])) {
        Core.set(window, ["PD", "Sync"], {});
}
 
PD.PDCopyButton = Core.extend(Echo.Button, {
	$load : function() {
       	Echo.ComponentFactory.registerType("PDCopyButton", this);
	},
	componentType : "PDCopyButton"
});
 
 
PD.Sync.PDCopyButton = Core.extend(Echo.Sync.Button, {

    $load: function() {
        Echo.Render.registerPeer("PDCopyButton", this);
    },
    
    _clipText: null,
        
    renderAdd: function(update, parentElement) {
        Echo.Sync.Button.prototype.renderAdd.call(this, update, parentElement);
        this._clipText = this.component.render("clipText");
    },
        
    //overwrites parent action    
    doAction: function() {
        //this._navigator.clipboard.writeText(this._clipText);
        
        var textArea = document.createElement("textarea");
	    textArea.value = this._clipText;
    	textArea.style.position="fixed";  //avoid scrolling to bottom
	    document.body.appendChild(textArea);
	    textArea.focus();
	    textArea.select();
	    try {
	        var successful = document.execCommand('copy');
	        var msg = successful ? 'successful' : 'unsuccessful';
	        console.log(msg);
	    } catch (err) {
	        console.log('Was not possible to copy te text: ', err);
	    }
		document.body.removeChild(textArea)    
        
		var bg = this.div.style.background;
        this.div.style.background = 'orange';
        var elem = this.div; 
        var fadeEffect = setInterval(function() {
            elem.style.background = bg;
			clearInterval(fadeEffect);
		}, 300);
    }    
});