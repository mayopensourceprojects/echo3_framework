/**
 * Component rendering peer: PDShortcut.
 * This class should not be extended by developers, the implementation is subject to change.
 */
 
if (!Core.get(window, ["PD", "Sync"])) {
        Core.set(window, ["PD", "Sync"], {});
}
 
PD.PDShortcut = Core.extend(Echo.Component, {
		$load : function() {
        	Echo.ComponentFactory.registerType("PDShortcut", this);
		},
		componentType : "PDShortcut",
		
	    $virtual: {
   			doAction: function() {
            	this.fireEvent({
            		type: "action", 
            		source: this, 
            		actionCommand: this.get("actionCommand")
            	});
			}
    	}
});
 
 
PD.Sync.PDShortcut = Core.extend(Echo.Render.ComponentSync, {
    
    $load: function() {
        Echo.Render.registerPeer("PDShortcut", this);
    },
    
    _node: null,
    
    _dragOriginX: null,
    _dragOriginY: null,
    _dragInitPositionX: null,
    _dragInitPositionY: null,
    
    _icon: null,
    _height: 48,
    _width: 48,
    _positionX: null,
    _positionY: null,
    _title: null,    
	_firstClickTime: null,


    $construct: function() {
        this._processMouseMoveRef = Core.method(this, this._processMouseMove);
        this._processMouseUpRef = Core.method(this, this._processMouseUp);
    },
    
    /** @see Echo.Render.ComponentSync#renderAdd */
    renderAdd: function(update, parentElement) {
        this._containerElement = parentElement;
        var icon = this.component.render("icon");
		var title = this.component.render("title");
		this._positionX = Echo.Sync.Extent.toPixels(this.component.render("positionX"));
		this._positionY = Echo.Sync.Extent.toPixels(this.component.render("positionY"));

		//the main node
        this._node = document.createElement("span");
        this._node.id = this.component.renderId;
		this._node.style.position = "absolute";	
       	this._node.style.left = this._positionX + "px";
		this._node.style.top = this._positionY + "px";
		this._node.style.width = "48px";
		this._node.style.height ="60px";
		
		this._node.style.zIndex = 0;		
		this._node.style.cursor = "pointer";
		parentElement.appendChild(this._node);
		
		//the icon
		var imgElement = document.createElement("img");
        Echo.Sync.ImageReference.renderImg(icon, imgElement);
   		this._node.appendChild(imgElement);

		//the title text
	    var textElement = document.createElement("div");
	    textElement.style.verticalAlign = "top";
	    textElement.style.position = "relative";
	    textElement.style.top = "-6px";
		textElement.style.left = "-12px";
	   	textElement.style.textAlign = "center";
	    textElement.style.width = "60px";    
	    textElement.style.fontSize = "9px";
	    textElement.appendChild(document.createTextNode(title));
	    this._node.appendChild(textElement);

		//event listeners		
        Core.Web.Event.add(this._node, "mousedown", Core.method(this, this._processMouseDown), true);
        Core.Web.Event.add(this._node, "click", Core.method(this, this._processClick), true);		
    },

	//used by Firefox
	_processClick: function(echoEvent) {
        if (!this.client || !this.client.verifyInput(this.component)) {
            return true;
        }
	    var d = new Date();
	   	var now = d.getTime();
	    if ((now - this._firstClickTime) < 450) {  //450ms timeout
	        //double click 
			this.component.doAction();
	    } else {
	       //single click
			this._firstClickTime = d.getTime();
		}	
	},

    
    /** @see Echo.Render.ComponentSync#renderDispose */
    renderDispose: function(update) {
        this._containerElement = null;
        this._node = null;
    },
    
    /** @see Echo.Render.ComponentSync#renderUpdate */
    renderUpdate: function(update) {
        if (this._node) {
            this._node.parentNode.removeChild(this._node);
        }
        // Note: this.renderDispose() is not invoked (it does nothing).
        this.renderAdd(update, this._containerElement);
        return false; // Child elements not supported: safe to return false.
    },
    
    _processMouseDown: function(echoEvent) {
    	
    	//alert('Mouse down');
        if (!this.client || !this.client.verifyInput(this.component)) {
            return true;
        }
        
	    this.dragInitPositionX = this._positionX;
	    this.dragInitPositionY = this._positionY;
	    this.dragOriginX = echoEvent.clientX;
	    this.dragOriginY = echoEvent.clientY;
	    
        // Prevent selections.
        Core.Web.dragInProgress = true;
        Core.Web.DOM.preventEventDefault(echoEvent);
    
        Core.Web.Event.add(document.body, "mousemove", this._processMouseMoveRef, true);
        Core.Web.Event.add(document.body, "mouseup", this._processMouseUpRef, true);
	},

	_processMouseMove: function(e) {
		var posX = this._dragInitPositionX + e.clientX - this._dragOriginX;
		var posY = this._dragInitPositionY + e.clientY - this._dragOriginY;
		
		this._positionX = posX;
		this._positionY = posY;
   
	    this._redraw();
	},

	_processMouseUp: function(e) {
		Core.Web.dragInProgress = false;
        
        Core.Web.Event.remove(document.body, "mousemove", this._processMouseMoveRef, true);
        Core.Web.Event.remove(document.body, "mouseup", this._processMouseUpRef, true);
    
        this.component.set("positionX", this._positionX);
        this.component.set("positionY", this._positionY);
	},

	_redraw: function() {
	 	this._node.style.left = this._positionX + "px";
		this._node.style.top = this._positionY + "px";
	}    
});
