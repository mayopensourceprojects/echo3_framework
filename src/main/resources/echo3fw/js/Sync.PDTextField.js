/**
 * Component rendering peer: PDWindow.
 * This class should not be extended by developers, the implementation is subject to change.
 */
 
if (!Core.get(window, ["PD", "Sync"])) {
        Core.set(window, ["PD", "Sync"], {});
}
 
PD.PDTextField = Core.extend(Echo.TextField, {
	$load : function() {
       	Echo.ComponentFactory.registerType("PDTextField", this);
	},
	componentType : "PDTextField"
});
 
 
PD.Sync.PDTextField = Core.extend(Echo.Sync.TextField, {

    $load: function() {
        Echo.Render.registerPeer("PDTextField", this);
    },
    
    /** @see Echo.Render.ComponentSync#renderAdd */
    renderAdd: function(update, parentElement) {
        this.input = document.createElement("input");
        this.input.id = this.component.renderId;
        if (!this.component.render("editable", true)) {
            this.input.readOnly = true;
        }
        this.input.type = this._type;
        var maximumLength = this.component.render("maximumLength", -1);
        if (maximumLength >= 0) {
            this.input.maxLength = maximumLength;
        }
        this._renderStyle(this.input);
        this._addPDEventHandlers();
        if (this.component.get("text")) {
            this.input.value = this.component.get("text");
        }
        
        this.renderAddToParent(parentElement);
    },
    
    /**
     * Registers event handlers on the text component.
     */
    _addPDEventHandlers: function() {
        Core.Web.Event.add(this.input, "click", Core.method(this, this._processClick), false);
        Core.Web.Event.add(this.input, "focus", Core.method(this, this._processFocus), false);        
		Core.Web.Event.add(this.input, "blur", Core.method(this, this._processPDBlur), false);  
		Core.Web.Event.add(this.input, "keypress", Core.method(this, this._processPDKeyPress), false);
        Core.Web.Event.add(this.input, "keyup", Core.method(this, this._processPDKeyUp), false);
    },
    
    _processPDBlur: function(e) {
        this._focused = false;
        return this._storeValue();
    },
    
    _processPDKeyPress: function(e) {
        return this._storeValue(e);
    },
    
    _processPDKeyUp: function(e) {
        this._storeValue(e);
        var keyActionEnabled = this.component.render("keyAction", false);
		if (keyActionEnabled && e.keyCode != 13) {
	        var that = this.component;
			var doActionDelayed = function() {
				that.doAction();
	    	};
			window.setTimeout(doActionDelayed, 300);  
        }
        return true;
    }
});