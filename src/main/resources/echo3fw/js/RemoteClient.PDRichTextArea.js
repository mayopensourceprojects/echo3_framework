/**
 * RemoteClient-hosted RichTextArea component.
 */
PD.PDRemoteRichTextArea = Core.extend(PD.PDRichTextArea, {

    $load: function() {
        Echo.ComponentFactory.registerType("PD.PDRemoteRichTextArea", this);
    },
    
    componentType: "PD.PDRemoteRichTextArea"
});

/**
 * RemoteClient-hosted RichTextArea component synchronization peer.
 */
PD.Sync.PDRemoteRichTextArea = Core.extend(PD.Sync.PDRichTextArea, {
    
    $static: {

        /**
         * Serialization peer for <code>EditedHtml</code> instances.
         * The toString() method of the object is invoked.
         */
        EditedHtmlSerialPeer: Core.extend(Echo.Serial.PropertyTranslator, {
            
            $static: {
                
                /** @see Echo.Serial.PropertyTranslator#toXml */
                toXml: function(client, pElement, value) {
                    pElement.appendChild(pElement.ownerDocument.createTextNode(value.toString()));
                }
            },
            
            $load: function() {
                Echo.Serial.addPropertyTranslator("PD.PDRichTextInput.EditedHtml", this);
            }
        })
    },
    
    processInsertHyperlink: function() {
        if (this.component.render("overrideInsertHyperlink")) {
            this.component.fireEvent({ source: this.component, type: "operation", data: "insertHyperlink" });
        } else {
            PD.Sync.PDRichTextArea.prototype.processInsertHyperlink.call(this);
        }
    },
    
    processInsertImage: function() {
        if (this.component.render("overrideInsertImage")) {
            this.component.fireEvent({ source: this.component, type: "operation", data: "insertImage" });
        } else {
            PD.Sync.PDRichTextArea.prototype.processInsertImage.call(this);
        }
    },
    
    processInsertTable: function() {
        if (this.component.render("overrideInsertTable")) {
            this.component.fireEvent({ source: this.component, type: "operation", data: "insertTable" });
        } else {
            PD.Sync.PDRichTextArea.prototype.processInsertTable.call(this);
        }
    },

    $load: function() {
        Echo.Render.registerPeer("PD.PDRemoteRichTextArea", this);
    }
});

/**
 * Command peer for InsertHtmlCommand.
 */
PD.Sync.PDRemoteRichTextArea.InsertHtmlCommand = Core.extend(Echo.RemoteClient.CommandExec, {
    
    $static: {

        /** @see Echo.RemoteClient.CommandExecProcessor#execute */
        execute: function(client, commandData) {
            // Delay execution, such that update will be stored in outgoing client message.
            Core.Web.Scheduler.run(Core.method(this, function() {
                var rta = client.application.getComponentByRenderId(commandData.renderId);
                rta.insertHtml(commandData.html);
            }));
        }
     },
     
     $load: function() {
        Echo.RemoteClient.CommandExecProcessor.registerPeer("nextapp.echo.extras.app.richtext.InsertHtmlCommand", this);
     }
});
