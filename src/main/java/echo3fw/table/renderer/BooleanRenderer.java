/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table.renderer;

import echo3fw.table.PDTableModel;
import nextapp.echo.app.CheckBox;
import nextapp.echo.app.Component;
import nextapp.echo.app.Table;


public class BooleanRenderer extends PDCellRenderer {

	public boolean editable;
	protected boolean isHeader;

	public BooleanRenderer(boolean editable) {
		this.editable = editable;
	}

	public BooleanRenderer(boolean editable, boolean isHeader) {
		this.editable = editable;
		this.isHeader = isHeader;
	}

	public Component getTableCellRendererComponent(final Table table, Object value, final int col, final int row) {
		final PDTableModel model = (PDTableModel) table.getModel();

		final CheckBox checkBox = new CheckBox();
		checkBox.setVisible(isEnable(row));
		checkBox.addActionListener(e -> setSelected(table, model, checkBox.isSelected(), col, row));
		if (value instanceof Boolean) {
			checkBox.setSelected((Boolean) value);
		}
		setBackground(checkBox, row);
		return checkBox;
	}

	// Override in children to have custom behaving
	protected boolean isEnable(int value) {
		return true;
	}

	protected void setSelected(Table table, PDTableModel model, boolean selected, int col, int row) {
		if (isHeader) {
			table.getColumnModel().getColumn(col).setHeaderValue(selected);
			for (int i = 0; i < model.getTotalRowCount(); i++) {
				model.setValueAt(selected, col, i);
			}
			model.fireTableDataChanged();
		} else {
			model.setValueAt(selected, col, row);
		}
	}
}