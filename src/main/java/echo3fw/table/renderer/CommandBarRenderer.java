/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table.renderer;

import echo3fw.table.PDTableModel;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Button;
import nextapp.echo.app.Component;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Row;
import nextapp.echo.app.Table;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.table.TableCellRenderer;


public class CommandBarRenderer extends Row implements TableCellRenderer {

	private Button btnDelete;
	private Button btnDown;
	private Button btnEdit;
	private Button btnUp;
	private IRowEditListener editListener;
	private int row;

	public CommandBarRenderer(IRowEditListener editListener) {
		this.editListener = editListener;
	}

	public CommandBarRenderer(IRowEditListener editListener, int row, int rowCount) {
		this.editListener = editListener;
		this.row = row;
		initGUI(rowCount);
	}

	public Button getBtnDelete() {
		return btnDelete;
	}

	public Button getBtnDown() {
		return btnDown;
	}

	public Button getBtnEdit() {
		return btnEdit;
	}

	public Button getBtnUp() {
		return btnUp;
	}

	public int getRow() {
		return row;
	}

	public Component getTableCellRendererComponent(Table table, Object value, final int col, final int row) {
		final PDTableModel tableModel = (PDTableModel) table.getModel();
		CommandBarRenderer ret = new CommandBarRenderer(editListener, row, tableModel.getRowCount());
		init(tableModel, ret, col, row);
		return ret;
	}

	protected void init(PDTableModel tableModel, CommandBarRenderer cbr, int col, int row) {
		// To be overridden
	}

	private void initGUI(int rowCount) {
		btnEdit = new Button(PDUtil.getImage("/img/edite.gif"));
		btnEdit.setDisabledIcon(PDUtil.getImage("/img/editd.gif"));
		btnEdit.addActionListener(e -> editListener.btnEditClicked(new ActionEvent(CommandBarRenderer.this, null)));
		add(btnEdit);

		btnUp = new Button(PDUtil.getImage("/img/upe.gif"));
		btnUp.setDisabledIcon(PDUtil.getImage("/img/upd.gif"));
		btnUp.addActionListener(e -> {
			editListener.btnUpClicked(new ActionEvent(CommandBarRenderer.this, null));
		});
		btnUp.setEnabled(row != 0);
		add(btnUp);

		btnDown = new Button(PDUtil.getImage("/img/dwe.gif"));
		btnDown.setDisabledIcon(PDUtil.getImage("/img/dwd.gif"));
		btnDown.addActionListener(e -> {
			editListener.btnDownClicked(new ActionEvent(CommandBarRenderer.this, null));
		});
		btnDown.setEnabled(row < rowCount - 1);
		add(btnDown);

		btnDelete = new Button(PDUtil.getImage("/img/deletee.gif"));
		btnDelete.setDisabledIcon(PDUtil.getImage("/img/deleted.gif"));
		btnDelete.addActionListener(e -> {
			editListener.btnDeleteClicked(new ActionEvent(CommandBarRenderer.this, null));
		});
		add(btnDelete);

		setInsets(new Insets(5, 0));
	}	
}