/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table;

import nextapp.echo.app.event.TableModelListener;
import nextapp.echo.app.table.DefaultTableModel;

public abstract class PDTableModel extends DefaultTableModel {

	protected TableModelListener cellChangeListener;

	public abstract void setValueAt(Object value, int column, int row);

	/**
	 * Set the listener which fires an event when the content of a cell changes
	 * This is meant for updating the model, and save the change to the
	 * database. This is in order to avoid that fireTableDataChanged() is called
	 * which would re-draw the whole table, and mess up with the focus tab order
	 */
	public void setCellChangeListener(TableModelListener cellChangeListener) {
		this.cellChangeListener = cellChangeListener;
	}
	
	public int getTotalRowCount() {
		return getRowCount();
    }
}
