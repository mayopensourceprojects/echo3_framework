/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table;

import nextapp.echo.app.Component;
import nextapp.echo.app.Label;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import echo3fw.gui.PDMasterDataView;
import echo3fw.internal.PDTitleBar;
import echo3fw.model.MBase;
import echo3fw.util.HeaderValue;
import echo3fw.util.ICrud;
import echo3fw.util.PDUtil;

public abstract class PDPageableFactory {

	private Class modelClass;
	private PDMasterDataView parentForm;
	
	public PDPageableFactory(PDMasterDataView parentForm, Class modelClass) {
		this.modelClass = modelClass;
		this.parentForm = parentForm;
	}

	public abstract ICrud getEditor();
	
	public Component getTableCellComponent(HeaderValue headerValues) {
		Label lbl = new Label(PDUtil.isEmpty(headerValues.title) ? "..." : headerValues.title);
		return lbl;
	}

	public PDTitleBar getHeaderComponent(HeaderValue headerValues) {
		PDTitleBar titleBar = new PDTitleBar(parentForm, headerValues, true, true);
		return titleBar;
	}

	public abstract Criteria getCriteria(Session session);

	public ProjectionList getProjectionList() {
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.id(), "id");
		projectionList.add(Projections.property("cachedTitle"),  "title");  //Model must be of type MBaseWithTitle!
		return projectionList;
	}	

	//Override if applicable
	public HeaderValue createHeaderValue(Object[] data) {
		HeaderValue headerValues = new HeaderValue();
		headerValues.id = (Integer)(data)[0];
		headerValues.title = (String)(data)[1];
		return headerValues;
	}
	
	public Class getModelClass() {
		return modelClass;
	}

	public MBase getModel(HeaderValue headerValue) {
		return MBase.loadById(modelClass, headerValue.id);
	}

	public int getRowsPerPage() {
	    return 20;
    }
}
