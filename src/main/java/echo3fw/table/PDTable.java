/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;

import echo3fw.gui.PDCheckBox;
import echo3fw.util.ColorScheme;
import echo3fw.util.IImage;
import echo3fw.util.PDUtil;
import echo3fw.widgets.PDCopyButton;
import echo3fw.widgets.PDLabel;
import nextapp.echo.app.Border;
import nextapp.echo.app.Border.Side;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Font;
import nextapp.echo.app.Font.Typeface;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Table;
import nextapp.echo.app.table.TableCellRenderer;
import nextapp.echo.app.table.TableColumn;
import nextapp.echo.app.table.TableColumnModel;

public class PDTable<T> extends Table {

	private Map<T, PDCheckBox<T>> chkSelections = new HashMap<>();
	private String currentSortField;
	private long lastClick;
	private boolean selectable = false;
	private List<IntConsumer> selectionListeners = new ArrayList<>();
	private List<Consumer<String>> sortListeners = new ArrayList<>();

	public PDTable() {
		this(false);
	}

	public PDTable(boolean selectable) {
		this.selectable = selectable;
		
		setModel(new PDTableModel2<>());
		setInsets(new Insets(3));
		setWidth(new Extent(100, Extent.PERCENT));
		Side[] sides = new Side[4];
		sides[0] = new Side(0, Color.LIGHTGRAY, Border.STYLE_SOLID);
		sides[1] = new Side(0, Color.LIGHTGRAY, Border.STYLE_SOLID);
		sides[2] = new Side(1, Color.LIGHTGRAY, Border.STYLE_SOLID);
		sides[3] = new Side(0, Color.LIGHTGRAY, Border.STYLE_SOLID);		
		setBorder(new Border(sides));
		setSelectionBackground(ColorScheme.LIGHT_BLUE);
		setForeground(Color.BLACK);
		setSelectionEnabled(true);
		setHeaderVisible(true);
		setDefaultHeaderRenderer(new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
				if (PDTable.this.selectable && column == 0) {
					PDCheckBox chkAll = new PDCheckBox<>();
					chkAll.addActionListener(a -> doSelectAll(chkAll.isSelected()));
					return chkAll;
				}
				return new PDLabel(value + "", PDLabel.FIELD_LABEL);
			}
		});
		
		if (selectable) {
			addEditorColumn("", l -> getSelectionBox(l));
		}
	}

	public void addActionColumn(String tooltTipText, IImage image, Consumer<T> action) {
		addActionColumn(tooltTipText, image, action, null);
	}
	
	public void addActionColumn(String tooltTipText, IImage image, Consumer<T> action, Function<T, Boolean> enabledChecker) {
		PDTableModel2<T> tm = getPDModel();
		tm.addActionColumn(tooltTipText, image, action, enabledChecker);
		setModel(tm);
	}

	public PDColumnDef addColumn(String title, Function<T, Object> dataFetch) {
		return addColumn(title, dataFetch, null, false);
	}

	public PDColumnDef addColumn(String title, Function<T, Object> dataFetch, String sortField, boolean editorField) {
		PDTableModel2<T> tm = getPDModel();
		PDColumnDef cd = tm.addColumn(title, dataFetch, sortField, editorField);
		setModel(tm);
		return cd;
	}

	public void addCopyColumn(String tooltTipText, IImage image, Function<T, Object> action) {
		PDTableModel2<T> tm = getPDModel();
		tm.addCopyColumn(tooltTipText, image, action);
		setModel(tm);		
	}

	public void addDoubleClickAction(Consumer<T> c) {
		addActionListener(e -> {
			if (System.currentTimeMillis() - lastClick < 300) {
				c.accept(getSelectedItem());
			}
			lastClick = System.currentTimeMillis();
		});
	}

	public PDColumnDef addEditorColumn(String title, Function<T, Object> dataFetch) {
		return addColumn(title, dataFetch, null, true);
	}
	
	public void addItem(T item) {
		getPDModel().addItem(item);
	}

	public PDColumnDef addLinkColumn(String title, Function<T, Object> dataFetch, Consumer<T> linkListener) {		
		return addLinkColumn(title, null, dataFetch, linkListener);
	}

	public PDColumnDef addLinkColumn(String title, String sortField, Function<T, Object> dataFetch, Consumer<T> linkListener) {		
		Function<T, Object> buttonFetch = item -> getLinkButton(item, dataFetch.apply(item), linkListener);		
		return addColumn(title, buttonFetch, sortField, true);
	}
	
	public void addSelectionChangedListener(IntConsumer a) {
		selectionListeners.add(a);	
	}

	public void addSelectionListener(Consumer<T> c) {
		addActionListener(e -> {
			c.accept(getSelectedItem());
		});
	}

	public void addSortListener(Consumer<String> sortListener) {
		sortListeners.add(sortListener);
	}

	public void clear() {
		getPDModel().clear();
	}
	
	@Override
    public void createDefaultColumnsFromModel() {
		if (!(getModel() instanceof PDTableModel2)) {
			super.createDefaultColumnsFromModel();
			return;
		}
		
		PDTableModel2<T> tm = getPDModel();
        if (tm != null) {
        	TableColumnModel columnModel = getColumnModel();
            while (columnModel.getColumnCount() > 0) {
                columnModel.removeColumn(columnModel.getColumn(0));
            }
            
            int columnCount = tm.getColumnCount();
            for (int index = 0; index < columnCount; ++index) {
            	TableColumn tc = new TableColumn(index);
            	final int col = index;
            	if (tm.isEditorField(index)) {
	            	tc.setCellRenderer(new TableCellRenderer() {
						@Override
						public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
							Object o = tm.getValueAt(col, row);
							return (Component)o;
						}
					});
            	} else if (tm.isAction(index) || tm.isCopyAction(index)) {
            		tc.setWidth(new Extent(26));
	            	tc.setCellRenderer(new TableCellRenderer() {
						@Override
						public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
							boolean enabled = true;
							Function<T, Boolean> enabledChecker = tm.getEnabledChecker(col);
							if (enabledChecker != null) {
								enabled = enabledChecker.apply((T)value);
							}
								
							Button btn;
							if (tm.isCopyAction(col)) {
								btn = new PDCopyButton();
								Object copyValue = tm.getValueAt(column, row);
								((PDCopyButton)btn).setClipText(copyValue + "");
							} else {
								btn = new Button();
								if (enabled) {
									btn.addActionListener(a -> tm.getActionListener(col).accept(value));
								}
							}
							if (enabled) {
								btn.setIcon(tm.getActionIcon(col));
							} else {
								btn.setIcon(tm.getDisabledActionIcon(col));
							}
							btn.setToolTipText(tm.getColumnTitle(column));
							return btn;
						}
					});
            	}
            	
            	String sortField = tm.getSortField(index);
            	if (sortField != null) {
	            	tc.setHeaderRenderer(new TableCellRenderer() {						
						@Override
						public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
							Button btn = new Button(tm.getColumnName(column));
							btn.setFont(PDUtil.getFontBold(12));
							btn.setForeground(ColorScheme.GREY);
							if (sortField.equals(currentSortField)) {
								btn.setIcon(PDUtil.getImage("img/icons16/down.gif"));
							} else if (("@" + sortField).equals(currentSortField)) {
								btn.setIcon(PDUtil.getImage("img/icons16/up.gif"));
							}

							btn.addActionListener(a -> {
								String sortField2 = sortField;
								if (sortField2.equals(currentSortField)) {
									sortField2 = sortField2.startsWith("@") ? sortField2.substring(1) : "@" + sortField2;
								}
								currentSortField = sortField2;
								sortListeners.stream().forEach(l -> l.accept(currentSortField));
							});
							return btn;
						}
					});
            	}

                columnModel.addColumn(tc);
            }
        }
    }

	private void doSelectAll(boolean selected) {
		chkSelections.values().forEach(chk -> chk.setSelected(selected));
		selectionListeners.forEach(l -> l.accept(getSelectedItems().size()));
	}
	
	public void fireTableDataChanged() {
		getPDModel().fireTableDataChanged();
	}

	public T getItemAt(int row) {
		return getPDModel().getItemAt(row);
	}

	private Button getLinkButton(T item, Object caption, Consumer<T> linkListener) {
		Button btnContainer = new Button(caption + "");
		Font font = new Font(new Typeface("SansaProNormal"), Font.UNDERLINE, new Extent(13));
		btnContainer.setFont(font);
		btnContainer.addActionListener(a -> linkListener.accept(item));
		return btnContainer;
	}

	@SuppressWarnings("unchecked")
	protected PDTableModel2<T> getPDModel() {
		return (PDTableModel2<T>)getModel();
	}

	public T getSelectedItem() {
		int row = getSelectionModel().getMinSelectedIndex();
		return getItemAt(row);
	}

	public List<T> getSelectedItems() {
		return chkSelections.values().stream().filter(chk -> chk.isSelected()).map(chk -> chk.getTag()).collect(Collectors.toList());
	}

	private PDCheckBox<T> getSelectionBox(T item) {		
		PDCheckBox<T> chk = chkSelections.get(item);
		if (chk == null) {
			chk = new PDCheckBox<>();
			chk.addActionListener(a -> selectionListeners.forEach(l -> l.accept(getSelectedItems().size())));
			chk.setTag(item);
			chkSelections.put(item, chk);
		}
		return chk;
	}

	public void selectFirst() {
		getSelectionModel().setSelectedIndex(0, true);		
	}
	
	public void setValues(List<T> values) {
		clear();
		chkSelections.clear();
		values.stream().forEach(item -> addItem(item));
		fireTableDataChanged();
	}
}
