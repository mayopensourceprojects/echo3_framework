/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table;

import java.util.List;
import java.util.Vector;

import echo3fw.util.PDUtil;
import echopoint.Strut;
import nextapp.echo.app.Button;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;

public class PDNavigationBar extends Row {

	protected Label lblPosition;
	protected Button btnFirst;
	protected Button btnPrevious;
	protected Button btnNext;
	protected Button btnLast;
	private int currentPage;
	private int rowsPerPage;
	private int totalRowCount;
	private int maxPage; 
	private List<ActionListener> listeners = new Vector<ActionListener>();
	
	public PDNavigationBar(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
		initGUI();
	}
	
	private void initGUI() {
		setCellSpacing(new Extent(6));
		add(new Strut(6, 0));
		
		btnFirst = new Button(PDUtil.getImage("img/first_white.gif"));
		btnFirst.setDisabledIcon(PDUtil.getImage("img/first_grey.gif"));
		btnFirst.addActionListener(e -> {
			currentPage = 0;
			reloadData();
		});
		add(btnFirst);
		
		btnPrevious = new Button(PDUtil.getImage("img/previous_white.gif"));
		btnPrevious.setDisabledIcon(PDUtil.getImage("img/previous_grey.gif"));
		btnPrevious.addActionListener(e -> {
			currentPage--;
			if (currentPage < 0) currentPage = 0;
			reloadData();
		});
		add(btnPrevious);

		//add(new Strut(6, 0));

		lblPosition = new Label("");
		lblPosition.setFont(PDUtil.getFont(11));
		lblPosition.setLineWrap(false);
		add(lblPosition);

		//add(new Strut(6, 0));

		btnNext = new Button(PDUtil.getImage("img/next_white.gif"));
		btnNext.setDisabledIcon(PDUtil.getImage("img/next_grey.gif"));
		btnNext.addActionListener(e -> {
			currentPage++;
			if (currentPage > maxPage) {
				currentPage = maxPage;
			}
			reloadData();
		});
		add(btnNext);

		btnLast = new Button(PDUtil.getImage("img/last_white.gif"));
		btnLast.setDisabledIcon(PDUtil.getImage("img/last_grey.gif"));
		btnLast.addActionListener(e -> {
			currentPage = maxPage;
			reloadData();
		});
		add(btnLast);
	}
	
	public void updateNavigation(int totalRowCount, int relativePosition) {
		this.totalRowCount = totalRowCount;
		maxPage = totalRowCount / rowsPerPage; 
		if (totalRowCount < rowsPerPage) {
			btnFirst.setVisible(false);
			btnPrevious.setVisible(false);
			btnNext.setVisible(false);
			btnLast.setVisible(false);
		} else {
			btnFirst.setVisible(true);
			btnPrevious.setVisible(true);
			btnNext.setVisible(true);
			btnLast.setVisible(true);
			
			btnFirst.setEnabled(currentPage > 0);
			btnPrevious.setEnabled(currentPage > 0);
			boolean hasMore = currentPage < maxPage;
			btnNext.setEnabled(hasMore);
			btnLast.setEnabled(hasMore);
		}
		updateNavigationLabel(relativePosition);
	}

	public void updateNavigationLabel(int relativePosition) {
		String s = "";
		if (totalRowCount == 0) {
			s = "(Keine Daten)";
		} else {
			s = "Seite " + (currentPage + 1) + " / " + (maxPage + 1) + " [Records: " + totalRowCount + "]";
		}
		lblPosition.setText(s);
	}

	public int getCurrentPos() {
	    return currentPage * rowsPerPage;
    }

	// #Bus-30
	public int getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
    }
	
	private void reloadData() {
		updateNavigation(totalRowCount, 0);
		ActionEvent e = new ActionEvent(this, "");
		for (ActionListener l: listeners) {
			l.actionPerformed(e);
		}
	}
	
	public void addActionListener(ActionListener l) {
		listeners.add(l);
	}
}
