/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.function.Function;

import echo3fw.util.IImage;
import echo3fw.util.SelectableObjectBean;
import nextapp.echo.app.ImageReference;

public class PDTableModel2<T> extends PDTableModel {

	protected List<PDColumnDef> columns = new ArrayList<>();
	protected int currentPos = 0;
	protected List<T> values = new Vector<T>();
	
	public void addActionColumn(String tooltTipText, IImage icon, Consumer<T> action, Function<T, Boolean> enabledChecker) {
		PDColumnDef<?> cd = new PDColumnDef<>();
		cd.title = tooltTipText;
		cd.icon = icon;
		cd.action = action;
		cd.enabledChecker = enabledChecker;
		columns.add(cd);	    
	}

	public void addCopyColumn(String tooltTipText, IImage icon, Function<?, Object> dataFetch) {
		PDColumnDef<?> cd = new PDColumnDef<>();
		cd.title = tooltTipText;
		cd.icon = icon;
		cd.dataFetch = dataFetch;
		cd.copyAction = true;
		columns.add(cd);	    
	}

	public PDColumnDef<?> addColumn(String title, Function<T, Object> dataFetch, String sortField, boolean editorField) {
		PDColumnDef<?> cd = new PDColumnDef<>();
		cd.title = title;
		cd.dataFetch = dataFetch;
		cd.sortField = sortField;
		cd.editorField = editorField;
		columns.add(cd);
		return cd;
	}

	public PDColumnDef<?> addColumn(String title, Function<T, Object> dataFetch, String sortField) {
		return addColumn(title, dataFetch, sortField, false); 
    }

	public PDColumnDef<?> addColumn(String title, Function<T, Object> dataFetch) {
		PDColumnDef<?> cd = new PDColumnDef<>();
		cd.title = title;
		cd.dataFetch = dataFetch;
		columns.add(cd);
		return cd;
    }

	public void addItem(T item) {
		values.add(item);
	}

	public void clear() {
		values.clear();		
	}
	
	public ImageReference getActionIcon(int col) {
		return columns.get(col).icon.getImage();
	}

	public ImageReference getDisabledActionIcon(int col) {
		return columns.get(col).icon.getDisabledImage();
	}

	public Consumer getActionListener(int col) {
		return columns.get(col).action;
	}

	public Function<?, Object> getDataFetch(int col) {
		return columns.get(col).dataFetch;
	}

	public boolean isCopyAction(int col) {
		return columns.get(col).copyAction;
	}
	
	@Override
	public int getColumnCount() {
	    return columns.size();
    }

	@Override
	public String getColumnName(int col) {
		if(columns.get(col).action != null || columns.get(col).copyAction) {
			return "";
		}
		return columns.get(col).title;
	}

	public String getColumnTitle(int col) {
		return columns.get(col).title;
	}
	
	public T getItemAt(int row) {
		return values.get(row);
	}

	@Override
	public int getRowCount() {
		if (values.size() > 20) return 20;
	    return values.size();
    }


	@Override
	public int getTotalRowCount() {
	    return values.size();
    }

	@Override
	public Object getValueAt(int col, int row) {
		if (values.size() <= currentPos + row) {
			return null;
		}
		T o = values.get(currentPos + row);
		if (columns.get(col).dataFetch == null) {
			return o;
		}
		return columns.get(col).dataFetch.apply(o);
    }

	public List<T> getValues() {
		return values;
	}

	public boolean isAction(int col) {
		return columns.get(col).action != null;
	}

	public void setColumns(List<PDColumnDef> newColumns) {
		columns.clear();
		columns.addAll(newColumns);
    }
	
	public void resetColumns() {
		columns.clear();
    }

	@Override
	public void setValueAt(Object value, int col, int row) {
		Object o = values.get(row);
		if (o instanceof SelectableObjectBean && col == 0) {
			((SelectableObjectBean)o).setSelected((Boolean) value);
		}
	}

	public void setValues(List<T> values) {
		this.values = values;
	}

	public String getSortField(int col) {
		return columns.get(col).sortField;
	}

	public boolean isEditorField(int col) {
		return columns.get(col).editorField;
	}

	@SuppressWarnings("unchecked")
	public Function<T, Boolean> getEnabledChecker(int col) {
		return columns.get(col).enabledChecker;
	}
}
