/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.WeakHashMap;

import echo3fw.events.IAddListener;
import echo3fw.events.IDeleteListener;
import echo3fw.events.ISelectionListener;
import echo3fw.events.IUpdateListener;
import echo3fw.model.MBase;


public class PDEventBus  {

	private static HashMap<PDAppInstance, PDEventBus> dataListeners = new HashMap<>();
	
	public static PDEventBus getInstance() {
		PDEventBus instance = dataListeners.get(PDAppInstance.getActivePD());
		if (instance == null) {
			instance = new PDEventBus();
			dataListeners.put(PDAppInstance.getActivePD(), instance);
		}
		return instance;
	}
	
	private HashMap<Class<?>, Object> currentValue = new HashMap <>();
	private HashMap <Class<?>, Set<ISelectionListener>> selectionListeners = new HashMap <>();
	private HashMap <Class<?>, Set<IAddListener>> addListeners = new HashMap <>();
	private HashMap <String, Set<IUpdateListener>> updateListenersById = new HashMap <>();
	private HashMap <Class<?>, Set<IUpdateListener>> updateListenersByClass = new HashMap <>();
	private HashMap <Class<?>, Set<IDeleteListener>> deleteListeners = new HashMap <>();

	
	public <T> void addAddListener(Class<T> clazz, IAddListener<T> addListener) {
		getAddListenersPerClass(clazz, true).add(addListener);
	}

	public <T> void addUpdateListener(T observedObject, IUpdateListener<T> addListener) {
		getUpdateListenersPerId(getUniqueId(observedObject)).add(addListener);
	}

	public <T> void addUpdateListener(Class<T> clazz, IUpdateListener<T> addListener) {
		getUpdateListenersPerClass(clazz).add(addListener);
	}

	public  <T> void addSelectionListener(Class<T> clazz, ISelectionListener<T> selectionListener) {
		getSelectionListenersPerClass(clazz).add(selectionListener);
	}

	public  <T> void addDeleteListener(Class<T> clazz, IDeleteListener<T> deleteListener) {
		getDeleteListenersPerClass(clazz, true).add(deleteListener);
	}

	private <T> Set<IAddListener> getAddListenersPerClass(Class<T> realClass, boolean create) {
		Set<IAddListener> addListenersPerClass = addListeners.get(realClass);
		if (addListenersPerClass == null && create) {
			addListenersPerClass = Collections.newSetFromMap(new WeakHashMap<IAddListener, Boolean>());
			addListeners.put(realClass, addListenersPerClass);
		}
		return addListenersPerClass;
	}

	private <T> Set<IUpdateListener> getUpdateListenersPerId(String uniqueId) {
		Set<IUpdateListener> changeListenersPerClass = updateListenersById.get(uniqueId);
		if (changeListenersPerClass == null) {
			changeListenersPerClass = Collections.newSetFromMap(new WeakHashMap<IUpdateListener, Boolean>());
			updateListenersById.put(uniqueId, changeListenersPerClass);
		}
		return changeListenersPerClass;
	}

	private <T> Set<IUpdateListener> getUpdateListenersPerClass(Class<T> clazz) {
		Set<IUpdateListener> changeListenersPerClass = updateListenersByClass.get(clazz);
		if (changeListenersPerClass == null) {
			changeListenersPerClass = Collections.newSetFromMap(new WeakHashMap<IUpdateListener, Boolean>());
			updateListenersByClass.put(clazz, changeListenersPerClass);
		}
		return changeListenersPerClass;
	}

	private <T> Set<IDeleteListener> getDeleteListenersPerClass(Class<T> clazz, boolean create) {
		Set<IDeleteListener> deleteListenersPerClass = deleteListeners.get(clazz);
		if (deleteListenersPerClass == null && create) {
			deleteListenersPerClass = Collections.newSetFromMap(new WeakHashMap<IDeleteListener, Boolean>());
			deleteListeners.put(clazz, deleteListenersPerClass);
		}
		return deleteListenersPerClass;
	}

	private <T> Set<ISelectionListener> getSelectionListenersPerClass(Class<T> clazz) {
		Set<ISelectionListener> selectionListenersPerClass = selectionListeners.get(clazz);
		if (selectionListenersPerClass == null) {
			selectionListenersPerClass = Collections.newSetFromMap(new WeakHashMap<ISelectionListener, Boolean>());
			selectionListeners.put(clazz, selectionListenersPerClass);
		}
		return selectionListenersPerClass;
	}

	public Locale getLocale() {
		return Locale.GERMAN;
	}

	public void init() {
		addListeners.clear();
		selectionListeners.clear();
	}

	@SuppressWarnings("unchecked")
	public <T> T getSelectedValue(Class<T> clazz) {
		return (T)currentValue.get(clazz);
	}

	@SuppressWarnings("unchecked")
	public <T> void notifyAdd(T addedObject) {
		currentValue.put(getRealClass(addedObject), addedObject);
		for (Class clazz : getClasses(addedObject)) {
			Set<IAddListener> listeners = getAddListenersPerClass(clazz, false);
			if (listeners != null) {
				listeners.stream().forEach(l -> l.added(addedObject));
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <T> void notifySelection(T updatedObject) {
		if (updatedObject == null) {
			return;
		}
		currentValue.put(getRealClass(updatedObject), updatedObject);
		for (Class clazz : getClasses(updatedObject)) {
			List<ISelectionListener> listeners = new ArrayList(getSelectionListenersPerClass(clazz));
			for (ISelectionListener listener : listeners) {
				listener.selected(updatedObject);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void notifyNullSelection(Class clazz) {
		currentValue.put(clazz, null);		
		List<ISelectionListener> listeners = new ArrayList(getSelectionListenersPerClass(clazz));
		for (ISelectionListener listener : listeners) {
			listener.selected(null);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> void notifyUpdate(T changedObject) {
		Iterator<IUpdateListener> it = getUpdateListenersPerId(getUniqueId(changedObject)).iterator();
		while (it.hasNext()) {
			IUpdateListener listener = it.next();
			listener.updated(changedObject);
		}
		for (Class clazz : getClasses(changedObject)) {
			it = getUpdateListenersPerClass(clazz).iterator();
			while (it.hasNext()) {
				IUpdateListener listener = it.next();
				listener.updated(changedObject);
			}
		}		
	}

	@SuppressWarnings("unchecked")
	public <T> void notifyDelete(T deletedObject) {
		for (Class clazz : getClasses(deletedObject)) {			
			Set<IDeleteListener> listeners = getDeleteListenersPerClass(clazz, false);
			if (listeners != null) {
				listeners.stream().forEach(l -> l.deleted(deletedObject));
			}
		}
	}

	private List<Class<?>> getClasses(Object o) {
		List<Class<?>> classes = new ArrayList<>();
		Class clazz = o.getClass();
		while (clazz != Object.class) {
			classes.add(clazz);
			clazz = clazz.getSuperclass();
		}		
		return classes;
	}
	
	@SuppressWarnings("unchecked")
	private <T> Class<T> getRealClass(T o) {
		Class clazz = o.getClass();
		return clazz.getSimpleName().contains("$") ? (Class<T>)clazz.getSuperclass() : clazz;
	}

	private String getUniqueId(Object o) {
		if (o instanceof MBase) {
			return ((MBase)o).getUniqueId();
		}
		return o + "";
	}
	
	public void reset() {
		selectionListeners.clear();
		addListeners.clear();
		updateListenersById.clear();
		updateListenersByClass.clear();
		deleteListeners.clear();		
	}
}