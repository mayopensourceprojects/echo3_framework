/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nextapp.echo.webcontainer.WebContainerServlet;

/**
 * Basic servlet, provides the key of the session
 * 
 */
public abstract class PDServlet extends WebContainerServlet {

	private static final String USER_INSTANCE_SESSION_KEY_PREFIX = "echoUserInstance";
	private static String SERVLET_NAME = null;
	public final static String HIBERNATE_FACTORY = "HIBERNATE_FACTORY";
	public final static String BROWSER_SESSION = "BROWSER_SESSION";
	
	@Override
	protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if (SERVLET_NAME == null) {
			SERVLET_NAME = getServletName();
		}
		
		if ("Echo.Sync".equals(request.getParameter(SERVICE_ID_PARAMETER))) {
			
			PDHibernateFactory factory = new PDHibernateFactory();
			request.getSession().setAttribute(HIBERNATE_FACTORY, factory);

			super.process(request, response);
			
			if (factory.hasOpenSession()) {
				factory.closeSession();
			}
			request.getSession().setAttribute(HIBERNATE_FACTORY, null);

		} else {
			super.process(request, response);
		}
	}

	public String getSessionKey() {
		return USER_INSTANCE_SESSION_KEY_PREFIX + ":" + SERVLET_NAME;
	}
}
