/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import echo3fw.PDAppInstance;
import echo3fw.PDDesktop;
import echo3fw.gui.PDOkCancelDialog;
import echo3fw.gui.PDSimpleDialog;
import echo3fw.model.MBase;
import echo3fw.model.MSystemSetting;
import echo3fw.model.MSystemSetting.SETTING_KEY;
import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.Border;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Font;
import nextapp.echo.app.Font.Typeface;
import nextapp.echo.app.ImageReference;
import nextapp.echo.app.Label;
import nextapp.echo.app.ResourceImageReference;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.filetransfer.app.DownloadCommand;
import nextapp.echo.filetransfer.app.DownloadProvider;

public class PDUtil {
	
	private static final char[] arrayChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	public static final String PDF_CONTENT_TYPE = "application/pdf";
	public static final String XLS_CONTENT_TYPE = "application/xls";
	public static final String PROPERTY_PATH_FILES = "path.documents";
	public static Map<String, ImageReference> imageReferenceMap = new HashMap<String, ImageReference>();
	private static Properties props;
	public static long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
	private static final int LOGIN_CHARS_COUNT = 9;
	public static final String NOT_APPLICABLE = "N/A";
	public static NumberFormat NF0 = DecimalFormat.getIntegerInstance();
	public static NumberFormat NF1 = DecimalFormat.getNumberInstance();
	public static NumberFormat NF2 = DecimalFormat.getNumberInstance();
	
	static {
		NF1.setMaximumFractionDigits(1);
		NF1.setMinimumFractionDigits(1);
		NF2.setMaximumFractionDigits(2);
		NF2.setMinimumFractionDigits(2);
	}

	
	public static Font getFontBold(int size) {
		return new Font(new Typeface("SansaProSemiBold"), Font.PLAIN, new Extent(size));
	}
	
	public static Font getFontLight(int size) {
		return new Font(new Typeface("SansaProLight"), Font.PLAIN, new Extent(size));
	}

	public static Font getFont(int size) {
		return new Font(new Typeface("SansaProNormal"), Font.PLAIN, new Extent(size));
	}

	public static Color getGreen() {
		return new Color(147, 179, 45); //93B32D
	}
	
	public static Object[] addEmpty(Object[] src) {
		Object[] values = new Object[src.length + 1];
		System.arraycopy(src, 0, values, 1, src.length);
		values[0] = "";
		return values;
	}

	private static String createLogin(int charCount) {
		StringBuilder login = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < charCount; i++) {
			if (i % 3 == 0 && i != 0) {
				login.append('-');
			}
			login.append(arrayChars[r.nextInt(arrayChars.length)]);
		}
		return login.toString();
	}

	public static String createNewLogin() {
		Session session = PDAppInstance.getHibernateSession();
		Query query = session.createQuery("FROM MUser AS u WHERE u.login =:login");
		String login;
		do {
			login = createLogin(LOGIN_CHARS_COUNT);
			query.setString("login", login);
		} while (query.list().size() > 0);

		return login;
	}

	public static void doDownload(final byte[] content, final String fileName, final String contentType) {
		DownloadProvider d = new DownloadProvider() {
			public String getContentType() {
				return contentType;
			}
			public String getFileName() {
				return fileName;
			}
			public long getSize() {
				return content.length;
			}
			public void writeFile(OutputStream os) throws IOException {
				os.write(content);
			}
		};
		DownloadCommand cmd = new DownloadCommand(d);
		ApplicationInstance.getActive().enqueueCommand(cmd);
	}

	public static String emptyString(Object o) {
		return o == null ? "" : o.toString();
	}
	
	public static Calendar getCalendar(int d, int m, int y) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, d);
		cal.set(Calendar.MONTH, m);
		cal.set(Calendar.YEAR, y);
		return cal;
	}

	public static Calendar getDateOnlyCalendar() {
		return PDUtil.getDateOnlyCalendar((Calendar)null);
	}

	public static Calendar getDateOnlyCalendar(Calendar cal) {
		Calendar ret;

		if (cal == null) {
			ret = Calendar.getInstance();
		} else {
			ret = (Calendar) cal.clone();
		}

		ret.set(Calendar.HOUR_OF_DAY, 0);
		ret.set(Calendar.MINUTE, 0);
		ret.set(Calendar.SECOND, 0);
		ret.set(Calendar.MILLISECOND, 0);

		return ret;
	}

	public static boolean isBefore(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.HOUR, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.HOUR, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		return cal1.before(cal2);
	}

	public static boolean isEmpty(Object o) {
		if (o == null) return true;
		if (o.toString() == null) return true;
		return o.toString().trim().length() == 0;
	}

	/**
	 * The base path of the application In a deployed environment this would be:
	 * $CATALINA_BASE/name_of_application
	 */
	public File getBasePath() {
		return null;
	}

	//https://flaticons.net/
	public static ImageReference getImage(String name) {
		String key = ImageReference.class.getName() + ":" + name;
		ImageReference img = imageReferenceMap.get(key);
		if (img == null) {
			img = new ResourceImageReference(name);
			imageReferenceMap.put(key, img);
		}
		return img;
	}

	public static int[] htmlColorToIntRGBArray(String str) throws ParseException {
		if (str == null) {
			throw new NullPointerException("str == null");
		}

		str = str.trim();

		if (str.length() != 7) {
			throw new ParseException("str.length() != 7", 0);
		}

		if (!str.startsWith("#")) {
			throw new ParseException("!str.startsWith(\"#\")", 0);
		}

		int[] ret = new int[3];

		ret[0] = Integer.parseInt(str.substring(1, 3), 16);
		ret[1] = Integer.parseInt(str.substring(3, 5), 16);
		ret[2] = Integer.parseInt(str.substring(5, 7), 16);

		return ret;
	}

	public static String intRGBArrayToHtmlColor(int[] rgbArray) {
		if (rgbArray == null) {
			throw new NullPointerException("rgbArray == null");
		}

		if (rgbArray.length != 3) {
			throw new IllegalArgumentException("rgbArray.length != 3");
		}

		StringBuffer ret = new StringBuffer("#");

		for (int i = 0; i < 3; i++) {
			String curr = Integer.toHexString(rgbArray[i]);

			if (curr.length() == 1) {
				ret.append("0");
			}

			ret.append(curr);
		}

		return ret.toString();
	}

	public static byte[] readBytesFromFile(File file) throws IOException {
		byte[] bytesOfFile = new byte[(int) file.length()];
		DataInputStream dis = new DataInputStream(new FileInputStream(file));
		dis.readFully(bytesOfFile);
		dis.close();
		return bytesOfFile;
	}

	public static String null2empty(Object s) {
		if (s == null)
			return "";
		return s.toString();
	}

	public static String null2dots(Object s) {
		if (s == null)
			return "...";
		return s.toString();
	}

	public static Border getGreyBorder() {
		Border border = new Border(1, Color.LIGHTGRAY, Border.STYLE_DOTTED);
		return border;
	}

	public static String getLimitedText(String text, int length) {
		if (text == null)
			return "";
		if (text.length() < length) {
			return text;
		}
		return text.substring(0, length - 3) + "...";
	}

	public static void initProperties() {
//		URL url = PDAppInstance.getActivePD().getConfigPath();
//		if (url == null) {
//			System.out.println("WARN: config path not found at " + PDAppInstance.getActivePD().getConfigPath());
//			return;
//		}
//		File configFile = new File(url.getFile());
//		initProperties(configFile);
//	}
//
//	public static void initProperties(File configFile) {
		InputStream is = PDUtil.class.getClassLoader().getResourceAsStream("project.properties");
//		
//		if (!configFile.exists()) {
//			System.out.println("Config file " + configFile.getAbsolutePath() + " not found!!!");
//			return;
//		}
		try {
			props = new Properties();
//			InputStream is = new FileInputStream(configFile);
			props.load(is);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		if (props == null) {
			initProperties();
		}
		return props.getProperty(key);
	}

	public static String getProperty(String key, String defaultValue) {
		String result = getProperty(key);
		return result == null ? defaultValue : result;
	}

	public static int getDaysDifference(Date refDate, Date date) {
		if (date == null) return 0;
		if (refDate == null) return 0;
		long delta = date.getTime() - refDate.getTime();
		long deltaDays = delta / MILLIS_PER_DAY;
		return (int) deltaDays;
	}

	public static Border emptyBorder() {
		return new Border(0, Color.BLACK, Border.STYLE_NONE);
	}
	
	public static String load(SETTING_KEY key) {
		Criteria criteria = PDAppInstance.getHibernateSession().createCriteria(MSystemSetting.class);
		criteria.add(Restrictions.eq("idKey", key.name()));
		MSystemSetting setting = (MSystemSetting)criteria.uniqueResult();
		if (setting == null) return "";
	    return setting.getValue();
    }

	public static void store(SETTING_KEY key, String value) {
		Session session = PDAppInstance.getHibernateSession();
		Criteria criteria = session.createCriteria(MSystemSetting.class);
		criteria.add(Restrictions.eq("idKey", key.name()));
		MSystemSetting setting = (MSystemSetting)criteria.uniqueResult();
		if (setting == null) {
			setting = new MSystemSetting();
			setting.setIdKey(key.name());
		}
		setting.setValue(value);
		session.save(setting);
		session.getTransaction().commit();
		session.beginTransaction();
    }

	public static void doConfirm(String title, String msg, ActionListener listener) {
		PDOkCancelDialog dlg = new PDOkCancelDialog(title, 420, 140) {
			@Override
			protected Object onOkClicked() {
				listener.actionPerformed(null);
				return true;
			}
			@Override
			protected Component initGuiCustom(Object... param) {
				Label lbl = new Label(msg);
				//lbl.setIcon(PDUtil.getImage("img\\icons48\\Help-48.png"));
				lbl.setFont(PDUtil.getFont(14));
				addMainComponent(lbl);
				
				return btnOk;
			}
		};
		PDDesktop.getDesktop().addWindow(dlg);
	}

	public static void handleException(Exception ex) {
		PDSimpleDialog dlg = new PDSimpleDialog("Fehler", 400, 160);
		dlg.addMainComponent(new Label(ex.getMessage()));
		PDDesktop.getDesktop().addWindow(dlg);
		ex.printStackTrace();
	}

	public static <T extends MBase> T reload(T item) {
		if (item.getId() == 0) {
			return item;
		}
		return (T)PDAppInstance.getHibernateSession().load(item.getClass(), item.getId());
	}
	
	public static <T extends MBase> T reload(T item, Class clazz) {
		return (T)PDAppInstance.getHibernateSession().load(clazz, item.getId());
	}

	public static int null2int(Integer i) {
		return i == null ? 0 : i;
	}
}