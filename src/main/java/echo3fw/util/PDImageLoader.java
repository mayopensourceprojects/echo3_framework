/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class PDImageLoader {

	private static PDImageLoader instance;

	private Map imageMap = new HashMap();

	private PDImageLoader() {
		// Empty
	}

	public static PDImageLoader getInstance() {
		if (instance == null) {
			instance = new PDImageLoader();
		}

		return instance;
	}

	public InputStream getAsInputStream(String name) throws IOException {
		return new ByteArrayInputStream(getAsByteArray(name));
	}

	public URL getAsURL(String name) {
		return getClass().getResource(name);
	}

	public byte[] getAsByteArray(String name) throws IOException {
		byte[] image = (byte[]) imageMap.get(name);

		if (image == null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			ClassLoader cl = getClass().getClassLoader();
			InputStream is = cl.getResourceAsStream(name);
			byte[] buffer = new byte[512];

			int count;
			while ((count = is.read(buffer)) > 0) {
				baos.write(buffer, 0, count);
			}

			image = baos.toByteArray();
			imageMap.put(name, image);
		}

		return image;
	}
}