/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PDFormat {

	
	private static final DateFormat defaultDateAndTimeFormat = new SimpleDateFormat("dd.MM.yyyy kk:mm");
	private static final DateFormat defaultDateAndTimeFormat2 = new SimpleDateFormat("dd.MM.yyyy, kk:mm");
	private static final DateFormat defaultDateLongFormat = new SimpleDateFormat("EEEE, MMMM, d, yyyy");
	private static final DateFormat defaultDateOnlyFormat = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat defaultDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");	
	private static final DateFormat defaultMonthOnlyFormat = new SimpleDateFormat("MMMM");
	private static final DateFormat defaultMonthYearFormat = new SimpleDateFormat("MM-yyyy");
	private static final DateFormat defaultTimeOnlyFormat = new SimpleDateFormat("HH:mm");
	
	private static NumberFormat nf2FractionDigits = NumberFormat.getNumberInstance(Locale.GERMAN);
	private static NumberFormat nfInteger = NumberFormat.getIntegerInstance(Locale.GERMAN);
	static {
		nf2FractionDigits.setMaximumFractionDigits(2);
		nf2FractionDigits.setMinimumFractionDigits(2);
	}
	
	public static String format(Object o) {
		if (o == null) {
			return "";
		} else if (o instanceof Integer) {
			return nfInteger.format(((Integer)o).intValue());
		} else if (o instanceof Number) {
			return nf2FractionDigits.format(((Number)o).doubleValue());
		} else if (o instanceof Date) {
			return defaultDateOnlyFormat.format(((Date)o));
		} else if (o instanceof Calendar) {
			Date d = ((Calendar)o).getTime();
			return defaultDateOnlyFormat.format(d);
		}
		return o.toString(); 
	}

	public static String formatLong(Date date) {
		if (date == null) {
			return "";
		}
		return defaultDateAndTimeFormat.format(date);
	}
	
	public static String formatLong2(Date date) {
		if (date == null) {
			return "";
		}
		return defaultDateAndTimeFormat2.format(date);
	}

	public static String formatTimeOnly(Calendar calendar) {
		if (calendar == null) {
			return "";
		}

		return formatTimeOnly(calendar.getTime());
	}
	
	public static String formatTimeOnly(Date date) {
		if (date == null) {
			return "";
		}
		return defaultTimeOnlyFormat.format(date);
	}
	
	public static DateFormat getDefaultDateLongFormat() {
		return defaultDateLongFormat;
	}

	public static DateFormat getDefaultDateOnlyFormat() {
		return defaultDateOnlyFormat;
	}

	public static DateFormat getDefaultDateTimeFormat() {
		return defaultDateTimeFormat;
	}
	
	public static DateFormat getDefaultMonthOnlyFormat() {
		return defaultMonthOnlyFormat;
	}

	public static DateFormat getDefaultMonthYearFormat() {
		return defaultMonthYearFormat;
	}

	public static DateFormat getDefaultTimeOnlyFormat() {
		return defaultTimeOnlyFormat;
	}

	public static NumberFormat getNumberFormat(int digits) {
		NumberFormat nf = NumberFormat.getInstance(Locale.GERMAN);
		nf.setMinimumFractionDigits(digits);
		nf.setMaximumFractionDigits(digits);
		return nf;
	}

	public static NumberFormat getNumberFormat0() {
		return nfInteger;
	}

	public static Date parseDateTime(String string) throws ParseException {
		return getDefaultDateTimeFormat().parse(string);
	}

	public static Date parseDate(String string) throws ParseException {
			return getDefaultDateOnlyFormat().parse(string);
	}

	public static Calendar parseCalendar(String string) {
		return parseCalendar(string, getDefaultDateOnlyFormat());
	}
	
	public static Calendar parseCalendar(String string, DateFormat df) {
		if (PDUtil.isEmpty(string)) {
			return null;
		}
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(string));
			return cal;
		} catch (ParseException e) {
			return null;
		}
	}
}
