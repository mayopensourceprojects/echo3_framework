/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.Extent;
import nextapp.echo.app.StreamImageReference;

public class ByteArrayImageReference extends StreamImageReference {

	private byte[] data;
	private String contentType;
	private String id = ApplicationInstance.generateSystemId();
	private Extent height;
	private Extent width;
		
	public ByteArrayImageReference(byte[] data, String contentType) {
		this(data, contentType, 48, 48);
	}

	public ByteArrayImageReference(byte[] data, String contentType, int width, int height) {
		this.data = data;
		this.contentType = contentType;
		this.width = new Extent(width);
		this.height = new Extent(height);
	}
		
	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public void render(OutputStream out) throws IOException {
		try {
			IOUtils.copy(new ByteArrayInputStream(data), out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRenderId() {
		return id;
	}
	
	@Override
	public Extent getHeight() {
		return height;
	}
	
	@Override
	public Extent getWidth() {
		return width;
	}
}
