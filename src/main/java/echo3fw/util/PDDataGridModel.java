/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.util.List;
import java.util.Vector;

import nextapp.echo.app.table.AbstractTableModel;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;

import echo3fw.PDAppInstance;
import echo3fw.table.PDPageableFactory;

public class PDDataGridModel extends AbstractTableModel {

	// Changed to protected due to Bus #23
	protected PDPageableFactory tableFactory;
	protected List<HeaderValue> dataList = new Vector<HeaderValue>();	
	protected int rowsPerPage;
	protected int totalRows;
	
	public PDDataGridModel(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	
	public Class getColumnClass(int col) {
	    return Object.class;
    }

	public int getColumnCount() {
	    return 1;
    }

	public String getColumnName(int arg0) {
	    return null;
    }

	public int getRowCount() {
	    return dataList.size();
    }

	public int getTotalRowCount() {
	    return totalRows;
    }
	
	public Object getValueAt(int col, int row) {
		return dataList.get(row);
    }

	public void setFactory(PDPageableFactory tableFactory) {
	    this.tableFactory = tableFactory;
    }
	
	public void addItem(HeaderValue headerValue) {
		dataList.add(0, headerValue);
		totalRows++;
	}
	
	public void reloadData(int position) {
	    Criteria criteria = tableFactory.getCriteria(PDAppInstance.getHibernateSession());
	    criteria.setProjection(Projections.rowCount());
	    totalRows = (Integer)criteria.uniqueResult();
	    
	    Projection projection = tableFactory.getProjectionList();
	    if (projection == null) {
	    	criteria = tableFactory.getCriteria(PDAppInstance.getHibernateSession());
	    } else {
		    criteria.setProjection(projection);
	    }
	    criteria.setMaxResults(rowsPerPage);
	    criteria.setFirstResult(position);
	    
	    dataList.clear();
	    List list = criteria.list();
	    for (Object o : list) {
	    	Object[] data = null;
	    	if (o instanceof Object[]) {
	    		data = (Object[])o;
	    	} else {
	    		data = new Object[]{o};  //convert to array
	    	}
	    	dataList.add(tableFactory.createHeaderValue(data)); 
	    }
	}
}