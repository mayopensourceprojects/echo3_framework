/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.util.Vector;

public class Countries {

	public final static Vector<String> COUNTRIES = new Vector<String>();

	static {
		COUNTRIES.add("");
		COUNTRIES.add("Albania");
		COUNTRIES.add("Andorra");
		COUNTRIES.add("Antarctica");
		COUNTRIES.add("Argentina");
		COUNTRIES.add("Australia");
		COUNTRIES.add("Austria");
		COUNTRIES.add("Bahrain");
		COUNTRIES.add("Bangladesh");
		COUNTRIES.add("Barbados");
		COUNTRIES.add("Belarus");
		COUNTRIES.add("Belgium");
		COUNTRIES.add("Belize");
		COUNTRIES.add("Bermuda");
		COUNTRIES.add("Bhutan");
		COUNTRIES.add("Bolivia");
		COUNTRIES.add("Bosnia");
		COUNTRIES.add("Brazil");
		COUNTRIES.add("Brunei");
		COUNTRIES.add("Bulgaria");
		COUNTRIES.add("Cambodia");
		COUNTRIES.add("Canada");
		COUNTRIES.add("Canary Islands");
		COUNTRIES.add("Chile");
		COUNTRIES.add("China");
		COUNTRIES.add("China (Hong Kong)");
		COUNTRIES.add("Colombia");
		COUNTRIES.add("Congo");
		COUNTRIES.add("Costa Rica");
		COUNTRIES.add("Croatia");
		COUNTRIES.add("Cuba");
		COUNTRIES.add("Cyprus");
		COUNTRIES.add("Czech Republic");
		COUNTRIES.add("Denmark");
		COUNTRIES.add("Dominica");
		COUNTRIES.add("Dominican Republic");
		COUNTRIES.add("Dutch Antilles");
		COUNTRIES.add("Ecuador");
		COUNTRIES.add("Egypt");
		COUNTRIES.add("El Salvador");
		COUNTRIES.add("Estonia");
		COUNTRIES.add("Ethiopia");
		COUNTRIES.add("Faroe Islands");
		COUNTRIES.add("Finland");
		COUNTRIES.add("France");
		COUNTRIES.add("French West Indies");
		COUNTRIES.add("Germany");
		COUNTRIES.add("Ghana");
		COUNTRIES.add("Greece");
		COUNTRIES.add("Grenada");
		COUNTRIES.add("Guam");
		COUNTRIES.add("Guatemala");
		COUNTRIES.add("Guyana");
		COUNTRIES.add("Haiti");
		COUNTRIES.add("Honduras");
		COUNTRIES.add("Hong Kong (China)");
		COUNTRIES.add("Hungary");
		COUNTRIES.add("Iceland");
		COUNTRIES.add("India");
		COUNTRIES.add("Indonesia");
		COUNTRIES.add("Iran");
		COUNTRIES.add("Ireland");
		COUNTRIES.add("Irish");
		COUNTRIES.add("Israel");
		COUNTRIES.add("Italy");
		COUNTRIES.add("Ivory Coast");
		COUNTRIES.add("Jamaica");
		COUNTRIES.add("Japan");
		COUNTRIES.add("Jordan");
		COUNTRIES.add("Kenya");
		COUNTRIES.add("Kuwait");
		COUNTRIES.add("Kyrgyzstan");
		COUNTRIES.add("Latvia");
		COUNTRIES.add("Lebanon");
		COUNTRIES.add("Liechtenstein");
		COUNTRIES.add("Luxembourgh");
		COUNTRIES.add("Madagascar");
		COUNTRIES.add("Malaysia");
		COUNTRIES.add("Maldives");
		COUNTRIES.add("Malta");
		COUNTRIES.add("Mauritius");
		COUNTRIES.add("Mexico");
		COUNTRIES.add("Mongolia");
		COUNTRIES.add("Morocco");
		COUNTRIES.add("Mozambique");
		COUNTRIES.add("Namibia");
		COUNTRIES.add("Nepal");
		COUNTRIES.add("Netherlands");
		COUNTRIES.add("New Zealand");
		COUNTRIES.add("Nicaragua");
		COUNTRIES.add("Nigeria");
		COUNTRIES.add("Norway");
		COUNTRIES.add("Pakistan");
		COUNTRIES.add("Panama");
		COUNTRIES.add("Papua New Guinea");
		COUNTRIES.add("Paraguay");
		COUNTRIES.add("Peru");
		COUNTRIES.add("Philippines");
		COUNTRIES.add("Poland");
		COUNTRIES.add("Polish");
		COUNTRIES.add("Portugal");
		COUNTRIES.add("Puerto Rico");
		COUNTRIES.add("Qatar");
		COUNTRIES.add("Regional");
		COUNTRIES.add("Romania");
		COUNTRIES.add("Russia");
		COUNTRIES.add("San Marino");
		COUNTRIES.add("Saudi Arabia");
		COUNTRIES.add("Senegal");
		COUNTRIES.add("Serbia");
		COUNTRIES.add("Singapore");
		COUNTRIES.add("Slovak Republic");
		COUNTRIES.add("Slovakia");
		COUNTRIES.add("Slovenia");
		COUNTRIES.add("South Africa");
		COUNTRIES.add("South Korea");
		COUNTRIES.add("Spain");
		COUNTRIES.add("Sri Lanka");
		COUNTRIES.add("St. Kitts and Nevis");
		COUNTRIES.add("St. Vincent and the Grenadines");
		COUNTRIES.add("Swaziland");
		COUNTRIES.add("Sweden");
		COUNTRIES.add("Switzerland");
		COUNTRIES.add("Taiwan (Republic of China)");
		COUNTRIES.add("Tanzania");
		COUNTRIES.add("Thailand");
		COUNTRIES.add("Tonga");
		COUNTRIES.add("Trinidad and Tobago");
		COUNTRIES.add("Tunisia");
		COUNTRIES.add("Turkey");
		COUNTRIES.add("U.S. Virgin Islands");
		COUNTRIES.add("Uganda");
		COUNTRIES.add("Ukraine");
		COUNTRIES.add("United Arab Emirates");
		COUNTRIES.add("United Kingdom");
		COUNTRIES.add("Unknown");
		COUNTRIES.add("Uruguay");
		COUNTRIES.add("Venezuela");
		COUNTRIES.add("Vietnam");
		COUNTRIES.add("Yemen");
		COUNTRIES.add("Zambia");
		COUNTRIES.add("Zimbabwe ");
	}

}
