/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.io.File;
import java.io.Serializable;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import javax.persistence.Entity;

import org.hibernate.EmptyInterceptor;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.Type;

import echo3fw.model.MBaseWithTitle;

/**
 * @author Angel Perez
 */
public class CledaConnector {

	private static CledaConnector instance = new CledaConnector();
	private SessionFactory sessionFactory;
	
	private CledaConnector() throws HibernateException {
		// Empty
	}

	public static CledaConnector getInstance() throws HibernateException {
		if (instance == null) {
			instance = new CledaConnector();
		}
		return instance;
	}

	public synchronized Session createSession() {
		if (sessionFactory == null) {
			Properties props = new Properties();
			props.put("hibernate.connection.pool_size", 5);
			props.put("hibernate.cglib.use_reflection_optimizer", true);
			props.put("hibernate.show_sql", false);
			String hbm2ddl = PDUtil.getProperty("db.hbm2ddl", "update");
			props.put("hibernate.hbm2ddl.auto", hbm2ddl);
			if ("true".equalsIgnoreCase(PDUtil.getProperty("db.quote_columns"))) {
				props.put("hibernate.globally_quoted_identifiers", "true");
			}

			props.put("transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
			props.put("hibernate.c3p0.idle_test_period", 1000);
			props.put("hibernate.c3p0.timeout", 1800);
			props.put("hibernate.c3p0.min_size", 3);
			props.put("hibernate.c3p0.max_size", 20);
			props.put("hibernate.c3p0.maxStatements", 50);
			props.put("hibernate.c3p0.automaticTestTable", "C3P0TestTable");
			props.put("hibernate.c3p0.acquireRetryAttempts", 30);
			props.put("hibernate.c3p0.acquireIncrement", 3);

			// database connection
			String host = "localhost";
			String port = "3306";
			
			if (!PDUtil.isEmpty(PDUtil.getProperty("db.host.env"))) {
				host = System.getenv(PDUtil.getProperty("db.host.env"));				
			} else if (!PDUtil.isEmpty(PDUtil.getProperty("db.host"))) {
				host = PDUtil.getProperty("db.host");
			}
			if (!PDUtil.isEmpty(PDUtil.getProperty("db.port.env"))) {
				port = System.getenv(PDUtil.getProperty("db.port.env"));				
			} else if (!PDUtil.isEmpty(PDUtil.getProperty("db.port"))) {
				port = PDUtil.getProperty("db.port");
			}
			String hostAndPort = host + ":" + port;
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXX " + hostAndPort);
			
			if ("MSSQL".equalsIgnoreCase(PDUtil.getProperty("db.type"))) {
				props.put("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
				props.put("hibernate.connection.url", "jdbc:sqlserver://" + hostAndPort + ";databaseName=" + PDUtil.getProperty("db.database"));
				//"jdbc:sqlserver://STARTREK\DALSQL;databaseName=SOdata			
				props.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");				
			} else if ("mariadb".equalsIgnoreCase(PDUtil.getProperty("db.type"))) {
				props.put("hibernate.connection.driver_class", "org.mariadb.jdbc.Driver");
				props.put("hibernate.connection.url", "jdbc:mariadb://" + hostAndPort + "/" + PDUtil.getProperty("db.database") + "?autoReconnect=true");
				props.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
			} else {
				props.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
				props.put("hibernate.connection.url", "jdbc:mysql://" + hostAndPort + "/" + PDUtil.getProperty("db.database") + "?autoReconnect=true");
				props.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
			}			
			
			props.put("hibernate.connection.username", PDUtil.getProperty("db.username"));
			props.put("hibernate.connection.password", PDUtil.getProperty("db.password"));
			
//			autoReconnectForPools
			
			Configuration config = new Configuration();
			config.setInterceptor(new AuditInterceptor());
			config.setProperties(props);

			// registering model
			registerModels(config, "echo3fw.model");
			registerModels(config, PDUtil.getProperty("model.path"));
			if (!PDUtil.isEmpty(PDUtil.getProperty("model.path2"))) {
				registerModels(config, PDUtil.getProperty("model.path2"));
			}
			sessionFactory = config.buildSessionFactory();
		}
		Session session = sessionFactory.openSession();
		session.setFlushMode(FlushMode.COMMIT);
		return session;
	}

	private void registerModels(Configuration config, String pckgname) {
		// see also
		// http://www.javaworld.com/javaworld/javatips/jw-javatip113.html?page=2
		File directory = null;
		URL url = null;
		try {
			String pack = '/' + pckgname.replace('.', '/');
			//System.out.println(pack);
			url = getClass().getResource(pack);
			//System.out.println(url);
			directory = new File(url.getFile());
		} catch (NullPointerException x) {
			throw new IllegalArgumentException(pckgname + " does not appear to be a valid package");
		}
		List<String> files = new Vector<String>();

		if (directory.exists()) {
			for (String s : directory.list()) {
				files.add(pckgname + "." + s);
			}
		} else {
			try {
				// It does not work with the filesystem: we must
				// be in the case of a package contained in a jar file.
				JarURLConnection conn = (JarURLConnection) url.openConnection();
				//String starts = conn.getEntryName();
				JarFile jfile = conn.getJarFile();
				Enumeration e = jfile.entries();
				while (e.hasMoreElements()) {
					ZipEntry entry = (ZipEntry) e.nextElement();
					String entryname = entry.getName();
					entryname = entryname.replace('/', '.');
					if (!entryname.startsWith(pckgname)) {
						continue;
					}
					if (!entryname.endsWith(".class")) {
						continue;
					}
					files.add(entryname);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		// Get the list of the files contained in the package
		for (String file : files) {
			// we are only interested in .class files
			if (file.endsWith(".class")) {
				// removes the .class extension
				//String className = pckgname + '.' + file.substring(0, file.length() - 6);
				String className = file.substring(0, file.length() - 6);
				try {
					Class clazz = Class.forName(className);
					if (clazz.getAnnotation(Entity.class) != null) {
						//System.out.println("Registering class: " + clazz.getCanonicalName() + "...");
						config.addAnnotatedClass(clazz);
					}
				} catch (ClassNotFoundException cnfe) {
					System.out.println("Class " + className + " mot found");
				}
			}
		}
	}

	public void dropDB() {
		// TODO Auto-generated method stub
	}

	public class AuditInterceptor extends EmptyInterceptor {
		@Override
		public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
			if (entity instanceof MBaseWithTitle) {
				MBaseWithTitle bwt = (MBaseWithTitle) entity;
				bwt.updateCachedTitle();
			}
			return false;
		}
	}
}