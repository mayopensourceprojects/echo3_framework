/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageResizer {

	public static byte[] resize(byte[] data, int targetWidth, int targetHeight, boolean higherQuality) throws IOException {
		BufferedImage img = ImageIO.read(new ByteArrayInputStream(data));
		int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		
		// If higherQuality we use the multi-step technique: start with original size, then
		// scale down in multiple passes with drawImage() until the target size is reached.
		// If not higherQuality we use the one-step technique: scale directly from original
		// size to target size with a single drawImage() call
		int w = higherQuality ? img.getWidth() : targetWidth;
		int h = higherQuality ? img.getHeight() : targetHeight;

		do {
			w = getNextDimension(w, targetWidth, higherQuality);
			h = getNextDimension(h, targetHeight, higherQuality);

			BufferedImage tmp = new BufferedImage(w, h, type);
			Graphics2D g2 = tmp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.drawImage(img, 0, 0, w, h, null);
			g2.dispose();

			img = tmp;
		} while (w != targetWidth || h != targetHeight);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "png", baos);
		baos.flush();
		byte[] imgBytes = baos.toByteArray();
		baos.close();
		
		return imgBytes;
	}
	
	/** 
	 * Scales the dimension by half until we reach the target value. This creates a better quality image.
	 * @param dimensionVal The current value of the dimension we want scale.
	 * @param target The target value of the dimension
	 * @param higherQuality True for the multi-step technique.
	 * @return Correct value for the next pass in the multi-step techique.
	 */
	private static int getNextDimension(int dimensionVal, int target, boolean higherQuality) {
		if (higherQuality && dimensionVal > target) {
			dimensionVal /= 2;
			// If dimension is smaller then we already passed target. So we have to back up.
			dimensionVal = dimensionVal < target ? target : dimensionVal;
		}
		
		return dimensionVal;
	}
}