/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.peers;

import echo3fw.gui.PDSvgImage;
import nextapp.echo.app.Component;
import nextapp.echo.app.update.ClientUpdateManager;
import nextapp.echo.app.util.Context;
import nextapp.echo.webcontainer.AbstractComponentSynchronizePeer;
import nextapp.echo.webcontainer.ServerMessage;
import nextapp.echo.webcontainer.WebContainerServlet;
import nextapp.echo.webcontainer.service.JavaScriptService;

public class PDSvgImagePeer extends AbstractComponentSynchronizePeer {

	private static final String PD_SVG_IMAGE = "PDSvgImage";

	static {
        WebContainerServlet.getServiceRegistry().add(JavaScriptService.forResource(PD_SVG_IMAGE, "echo3fw/js/Sync.PDSvgImage.js"));
    }
    
    public String getClientComponentType(boolean shortType) {
        return PD_SVG_IMAGE;
    }
    
    @Override
    public Class getComponentClass() {
        return PDSvgImage.class;
    }

    @Override
    public void init(Context context, Component component) {
        super.init(context, component);
        ServerMessage serverMessage = (ServerMessage) context.get(ServerMessage.class);
        serverMessage.addLibrary(PD_SVG_IMAGE);
        //serverMessage.addLibrary(JS_GRAPHICS);
        //serverMessage.addLibrary(MAPPER);
    }

    @Override
    public Class getInputPropertyClass(String propertyName) {
        if (PDSvgImage.PROPERTY_SVG_DATA.equals(propertyName)) {
            return byte[].class;
        }
        return null;
    };


    @Override
    public void storeInputProperty(Context context, Component component, String propertyName, int index, Object newValue) {
        ClientUpdateManager clientUpdateManager = (ClientUpdateManager) context.get(ClientUpdateManager.class);
        if (PDSvgImage.PROPERTY_SVG_DATA.equals(propertyName)) {
            clientUpdateManager.setComponentProperty(component, PDSvgImage.PROPERTY_SVG_DATA, newValue);
        }
    }
}
