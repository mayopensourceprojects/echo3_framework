/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.peers;

import echo3fw.widgets.PDRichTextArea;
import nextapp.echo.app.Button;
import nextapp.echo.app.Column;
import nextapp.echo.app.Component;
import nextapp.echo.app.ContentPane;
import nextapp.echo.app.Grid;
import nextapp.echo.app.Label;
import nextapp.echo.app.Panel;
import nextapp.echo.app.Row;
import nextapp.echo.app.SelectField;
import nextapp.echo.app.SplitPane;
import nextapp.echo.app.TextField;
import nextapp.echo.app.WindowPane;
import nextapp.echo.app.update.ClientUpdateManager;
import nextapp.echo.app.util.Context;
import nextapp.echo.extras.app.ColorSelect;
import nextapp.echo.extras.app.MenuBarPane;
import nextapp.echo.extras.webcontainer.CommonResources;
import nextapp.echo.extras.webcontainer.service.CommonService;
import nextapp.echo.webcontainer.AbstractComponentSynchronizePeer;
import nextapp.echo.webcontainer.ResourceRegistry;
import nextapp.echo.webcontainer.ServerMessage;
import nextapp.echo.webcontainer.Service;
import nextapp.echo.webcontainer.WebContainerServlet;
import nextapp.echo.webcontainer.service.JavaScriptService;

/**
 * Synchronization peer for <code>RichTextArea</code>s.
 */
public class PDRichTextAreaPeer extends AbstractComponentSynchronizePeer {

    private static final String RESOURCE_DIR = "echo3fw/js/";
    
    private static final Service PD_RICH_TEXT_AREA_SERVICE = JavaScriptService.forResources("PD.PDRichTextArea",
            new String[] { RESOURCE_DIR + "Application.PDRichTextInput.js",
                           RESOURCE_DIR + "Application.PDRichTextArea.js",
                           RESOURCE_DIR + "Sync.PDRichTextInput.js",
                           RESOURCE_DIR + "Sync.PDRichTextArea.js",
                           RESOURCE_DIR + "RemoteClient.PDRichTextArea.js" });
    
   // private static final Map LOCALE_SERVICES = new HashMap();
    
    static {
        WebContainerServlet.getServiceRegistry().add(PD_RICH_TEXT_AREA_SERVICE);
//        addLocaleService(Locale.GERMAN, "de");
        CommonResources.install();
        ResourceRegistry resources = WebContainerServlet.getResourceRegistry();
//        resources.add("Extras", "image/richtext/Icon16Alignment.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16AlignmentCenter.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16AlignmentJustify.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16AlignmentLeft.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16AlignmentRight.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Background.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Bold.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16BulletedList.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Copy.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Cut.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Delete.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Foreground.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16HorizontalRule.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Hyperlink.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Image.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Indent.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Italic.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16NumberedList.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Outdent.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Paste.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16ParagraphStyle.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16PlainText.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Redo.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16SelectAll.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Strikethrough.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Subscript.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Superscript.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Table.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16TableInsertRow.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16TableDeleteRow.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16TableInsertColumn.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16TableDeleteColumn.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16TextStyle.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Underline.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon16Undo.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon24Ok.png", ContentType.IMAGE_PNG);
//        resources.add("Extras", "image/richtext/Icon24Cancel.png", ContentType.IMAGE_PNG);
    }
    
    /**
     * Adds a locale-specific service.
     * 
     * @param locale server-side <code>Locale</code>
     * @param localeCode client-side locale code
     */
//    private static void addLocaleService(Locale locale, String localeCode) {
//        Service service = JavaScriptService.forResource("EchoExtras.PDRichTextArea." + localeCode, 
//                RESOURCE_DIR + "SyncLocale.PDRichTextArea." + localeCode + ".js");
//        WebContainerServlet.getServiceRegistry().add(service);
//        LOCALE_SERVICES.put(locale, service);
//    }
    
    /**
     * Default constructor.
     */
    public PDRichTextAreaPeer() {
        super();
        
        addRequiredComponentClass(Button.class);
        addRequiredComponentClass(ColorSelect.class);
        addRequiredComponentClass(Column.class);
        addRequiredComponentClass(ContentPane.class);
        addRequiredComponentClass(Grid.class);
        addRequiredComponentClass(Label.class);
        addRequiredComponentClass(MenuBarPane.class);
        addRequiredComponentClass(Panel.class);
        addRequiredComponentClass(Row.class);
        addRequiredComponentClass(SelectField.class);
        addRequiredComponentClass(SplitPane.class);
        addRequiredComponentClass(TextField.class);
        addRequiredComponentClass(WindowPane.class);

        addOutputProperty(PDRichTextArea.TEXT_CHANGED_PROPERTY);

        addEvent(new EventPeer(PDRichTextArea.INPUT_ACTION, PDRichTextArea.ACTION_LISTENERS_CHANGED_PROPERTY) {
        
            public boolean hasListeners(Context context, Component c) {
                return ((PDRichTextArea) c).hasActionListeners();
            }
        });
        
        addEvent(new EventPeer(PDRichTextArea.INPUT_OPERATION, PDRichTextArea.OPERATION_LISTENERS_CHANGED_PROPERTY) {
            
            public Class getEventDataClass() {
                return String.class;
            };
            
            public boolean hasListeners(Context context, Component c) {
                return ((PDRichTextArea) c).hasOperationListeners();
            }
        });
    }
    
    /**
     * @see nextapp.echo.webcontainer.ComponentSynchronizePeer#getComponentClass()
     */
    public Class getComponentClass() {
        return PDRichTextArea.class;
    }

    /**
     * @see nextapp.echo.webcontainer.ComponentSynchronizePeer#getClientComponentType(boolean)
     */
    public String getClientComponentType(boolean mode) {
        return "PD.PDRemoteRichTextArea";
    }
    
    /**
     * @see nextapp.echo.webcontainer.AbstractComponentSynchronizePeer#getInputPropertyClass(java.lang.String)
     */
    public Class getInputPropertyClass(String propertyName) {
        if (PDRichTextArea.TEXT_CHANGED_PROPERTY.equals(propertyName)) {
            return String.class;
        }
        return null;
    }
    
    /**
     * @see nextapp.echo.webcontainer.AbstractComponentSynchronizePeer#getOutputProperty(
     *      nextapp.echo.app.util.Context, nextapp.echo.app.Component, java.lang.String, int)
     */
    public Object getOutputProperty(Context context, Component component, String propertyName, int propertyIndex) {
        if (propertyName.equals(PDRichTextArea.TEXT_CHANGED_PROPERTY)) {
            PDRichTextArea rta = (PDRichTextArea) component;
            return rta.getText();
        }
        return super.getOutputProperty(context, component, propertyName, propertyIndex);
    }
    
    /**
     * @see nextapp.echo.webcontainer.ComponentSynchronizePeer#init(nextapp.echo.app.util.Context, Component)
     */
    public void init(Context context, Component component) {
        super.init(context, component);
        ServerMessage serverMessage = (ServerMessage) context.get(ServerMessage.class);
        serverMessage.addLibrary(CommonService.INSTANCE.getId());
        serverMessage.addLibrary(PD_RICH_TEXT_AREA_SERVICE.getId());
        installLocaleService(context, component);
    }

    /**
     * Installs a locale-specific service (if available) for a specific component.
     * 
     * @param context the relevant <code>Context</code>
     * @param component the component
     */
    private void installLocaleService(Context context, Component component) {
//        Locale locale = component.getRenderLocale();
//        if (LOCALE_SERVICES.containsKey(locale)) {
//            ServerMessage serverMessage = (ServerMessage) context.get(ServerMessage.class);
//            serverMessage.addLibrary(((Service) LOCALE_SERVICES.get(locale)).getId());
//        }
    }

    /**
     * @see nextapp.echo.webcontainer.ComponentSynchronizePeer#storeInputProperty(Context, Component, String, int, Object)
     */
    public void storeInputProperty(Context context, Component component, String propertyName, int propertyIndex, Object newValue) {
        if (propertyName.equals(PDRichTextArea.TEXT_CHANGED_PROPERTY)) {
            ClientUpdateManager clientUpdateManager = (ClientUpdateManager) context.get(ClientUpdateManager.class);
            clientUpdateManager.setComponentProperty(component, PDRichTextArea.TEXT_CHANGED_PROPERTY, newValue);
        }
    }
}
