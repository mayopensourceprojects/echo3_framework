/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import nextapp.echo.app.Component;
import nextapp.echo.app.ContentPane;
import nextapp.echo.app.WindowPane;

/**
 * This class is the centerpiece of the PDWorkspace framework; it provides the
 * desktop where the menu, windows, tasks etc. are displayed
 * 
 * This class is not designed for being extended; instead use a plugin-mechanism
 * (perspective) to fill it with life
 */
public abstract class PDDesktop extends ContentPane {

	protected ContentPane contentPane;

	public PDDesktop() {
	}

	// Used to keep the proper Z-Order in the windows. Rules:
	// All windows must inherit from WindowPane (or a common
	// window pane super class), two all windows must be
	// added with this method (which is indirectly called from
	// addWindow
	public void defaultAddForm(Component parent, WindowPane form) {
		int maxZindex = 20;

		for (int i = 0; i < parent.getComponentCount(); i++) {
			Component component = parent.getComponent(i);

			if (!(component instanceof WindowPane)) {
				continue;
			}
		}
		form.setZIndex(maxZindex + 1);
		parent.add(form);
	}


	public void addWindow(final Component c) {
		if (c instanceof WindowPane) {
			WindowPane wp = (WindowPane) c;
			defaultAddForm(contentPane, wp);
		} else {
			contentPane.add(c);
		}
	}

	public ContentPane getContentPane() {
	    return contentPane;
    }

	public static PDDesktop getDesktop() {
		return PDAppInstance.getActivePD().getDesktop();
	}
}