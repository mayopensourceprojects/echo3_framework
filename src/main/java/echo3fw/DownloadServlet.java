/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadServlet extends HttpServlet {

	private static Hashtable<String, Document> documents = new Hashtable<String, Document>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String docId = request.getParameter("docId");
		if (docId == null) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		} else {
			Document doc = getDocument(docId);
			if (doc == null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			} else {
				response.setHeader("Content-Disposition", "inline; filename=\"" + doc.getName() + "\"");
				response.setContentType(doc.getMimeType());
				response.setContentLength(doc.getContentLength());
				response.getOutputStream().write(doc.getContents());
			}
			
			//delete from cache after 1 minute
			Thread t = new Thread() {
				public void run() {
					try {
						Thread.sleep(60 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					documents.remove(docId);
					//System.out.println("Doc " + docId + " removed. Docs in cache: " + documents.size());
				}
			};
			t.start();
		}
	}

	public static void addDocument(Document doc) {
		documents.put(doc.getId(), doc);
	}

	private static Document getDocument(String id) {
		return documents.get(id);
	}

	public static class Document {

		private String id;
		private String name;
		private String mimeType;
		private int contentLength;
		private byte[] contents;

		public int getContentLength() {
			return contentLength;
		}

		public void setContentLength(int contentLength) {
			this.contentLength = contentLength;
		}

		public byte[] getContents() {
			return contents;
		}

		public void setContents(byte[] contents) {
			this.contents = contents;
		}

		public String getMimeType() {
			return mimeType;
		}

		public void setMimeType(String mimeType) {
			this.mimeType = mimeType;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}
}