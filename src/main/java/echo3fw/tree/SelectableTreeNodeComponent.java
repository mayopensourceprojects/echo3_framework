/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.tree;

import java.util.EventListener;

import echo3fw.util.PDUtil;
import nextapp.echo.app.CheckBox;
import nextapp.echo.app.ImageReference;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.button.ToggleButton;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.app.event.ChangeListener;
import nextapp.echo.app.event.EventListenerList;

public class SelectableTreeNodeComponent extends Row {

	private ToggleButton toggleBtn;
	private Label label;
	private Object node;
	private EventListenerList eventListenerList = new EventListenerList();

	public SelectableTreeNodeComponent(Object node, String text) {
		this.node = node;
		initGUI(text);
	}

	private void initGUI(String text) {
		label = new Label();
		add(label);

		toggleBtn = new CheckBox(text);
		toggleBtn.addActionListener(e -> toggleStateChanged());
		toggleBtn.setSelectedStateIcon(PDUtil.getImage("img/icons16/Check-Mark-16.png"));
		toggleBtn.setStateIcon(PDUtil.getImage("img/icons16/NoCheck-Mark-16.png"));

		add(toggleBtn);
	}
	
	public ToggleButton getToggleBtn() {
		return toggleBtn;
	}

	private void toggleStateChanged() {
		fireActionEvent(new ActionEvent(node, null));
	}

	public boolean isSelected() {
		return toggleBtn.isSelected();
	}

	public void setSelected(boolean selected) {
		toggleBtn.setSelected(selected);
	}

	public void setText(String text) {
		toggleBtn.setText(text);
	}

	@Override
	public void setEnabled(boolean enabled) {
		toggleBtn.setEnabled(enabled);
	}

	public String getText() {
		return toggleBtn.getText();
	}

	public void setIcon(ImageReference icon) {
		label.setIcon(icon);
	}

	public void addChangeListener(ChangeListener listener) {
		toggleBtn.addChangeListener(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		toggleBtn.removeChangeListener(listener);
	}

	public void addActionListener(ActionListener listener) {
		eventListenerList.addListener(ActionListener.class, listener);
	}

	public void removeActionListener(ActionListener listener) {
		eventListenerList.removeListener(ActionListener.class, listener);
	}

	public EventListener[] getActionListener() {
		return eventListenerList.getListeners(ActionListener.class);
	}

	public void fireActionEvent(ActionEvent evt) {
		EventListener[] eventListeners = getActionListener();

		for (int i = 0; i < eventListeners.length; i++) {
			ActionListener listener = (ActionListener) eventListeners[i];
			listener.actionPerformed(evt);
		}
	}
}