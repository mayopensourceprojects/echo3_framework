/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.tree;

import java.util.Enumeration;

import echopoint.tree.DefaultMutableTreeNode;

public class SelectableTreeNode<T> extends DefaultMutableTreeNode<T> {

	private SelectableTreeNodeComponent component;

	public SelectableTreeNode(T base) {
		super(base);
		allowsChildren = true;
		component = new SelectableTreeNodeComponent(this, base.toString());
		component.addActionListener(e -> {
			selectionToggled(SelectableTreeNode.this);
		});
	}

	private void selectionToggled(SelectableTreeNode node) {
		Enumeration enu = node.children();
		while (enu.hasMoreElements()) {
			SelectableTreeNode childNode = (SelectableTreeNode) enu.nextElement();
			childNode.setSelected(node.isSelected());
			selectionToggled(childNode);
		}
	}
	
	public boolean isSelected() {
		return component.isSelected();
	}

	public void setSelected(boolean selected) {
		component.setSelected(selected);
	}

	public void setEnabled(boolean enabled) {
		component.setEnabled(enabled);
	}

	public SelectableTreeNodeComponent getComponent() {
		return component;
	}
}