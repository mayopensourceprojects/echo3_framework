/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.model;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.CallbackException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

import echo3fw.PDAppInstance;

/**
 * @author Alejandro Salas
 */
@MappedSuperclass
public abstract class MBase implements Lifecycle {

	protected long id;

	public MBase() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean onSave(Session session) throws CallbackException {
		return false;
	}

	public boolean onUpdate(Session session) throws CallbackException {
		return false;
	}

	public boolean onDelete(Session session) throws CallbackException {
		return false;
	}

	public void onLoad(Session session, Serializable id) {
		// Empty
	}

	@Transient
	public String getIdAsString() {
		return Long.toString(id);
	}

	public String truncateString(String string, int length) {
		if (string == null) {
			return null;
		}

		length = string.length() < length ? string.length() : length;
		return string.substring(0, length);
	}

	@Override
	public int hashCode() {
		return (int)id;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof MBase)) {
			return false;
		}
		if (this.getClass() != other.getClass()) {
			return false;
		}
		if (id == 0) {
			return super.equals(other);
		}
		return id == ((MBase) other).getId();
	}

	@Deprecated
	public MBase reload() throws ObjectNotFoundException {
		if (id != 0) {
			Session session = PDAppInstance.getHibernateSession();
			Class clazz = getClass();
			clazz = clazz.getSimpleName().contains("$") ? clazz.getSuperclass() : clazz;
			return (MBase) session.load(clazz, id);
		}
		return this;
	}

	
	@Deprecated
	public static MBase loadById(Class clazz, int id) {
		return loadByField(clazz, "id", id + "");
	}

	@Deprecated
	public static MBase loadByField(Class clazz, String fieldKey, String fieldVal) {
		Session session = PDAppInstance.getHibernateSession();
		String hql = "FROM {0} AS item";

		if (fieldKey != null) {
			hql += " WHERE {1}=:fieldVal";
		}

		hql = MessageFormat.format(hql, new Object[] { clazz.getName(), fieldKey });
		Query query = session.createQuery(hql);

		if (fieldKey != null) {
			query.setString("fieldVal", fieldVal);
		}
		return (MBase) query.uniqueResult();
	}

	@Deprecated
	public static List listByField(Class clazz, String fieldKey, String fieldVal, String orderBy) {
		Session session = PDAppInstance.getHibernateSession();
		String hql = "FROM {0} AS item";

		if (fieldKey != null) {
			hql += " WHERE {1}=:fieldVal";
		}

		if (orderBy != null) {
			hql += " ORDER BY ";
			hql += orderBy;
		}

		hql = MessageFormat.format(hql, new Object[] { clazz.getName(), fieldKey });
		Query query = session.createQuery(hql);

		if (fieldKey != null) {
			query.setString("fieldVal", fieldVal);
		}
		return query.list();
	}

	@Deprecated
	public static List listByField(Class clazz) {
		return MBase.listByField(clazz, null, null, null);
	}

	@Transient
	public boolean isDeleteable() {
		return true;
	}

	@Transient
	public String getUniqueId() {
		return getClass().getSimpleName() + "@" + id;
	}
}