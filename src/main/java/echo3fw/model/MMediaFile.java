/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_pdw_media_file")
public class MMediaFile extends MBase {

	private Blob fileData;
	private Blob previewBlob;
	private Calendar date;
	private String fileName;
	private String contentType;
	private long fileSize;
	private transient byte[] data;
	private transient byte[] previewData;
	private String parentClass;
	private int parentId;
	
	public MMediaFile() {
	}

	public String getContentType() {
		return contentType;
	}

	public Calendar getDate() {
		return date;
	}

	@Transient
	public String getDateStr() {
		DateFormat timeFormat = new SimpleDateFormat("dd.MM kk:mm:ss");
		String date = timeFormat.format(getDate().getTime());
		return date;
	}

	public String getFileName() {
		return fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	
	@Override
	public String toString() {
		return fileName;
	}

	@Lob
	public Blob getFileData() {
		return fileData;
	}

	public void setFileData(Blob fileData) {
		this.fileData = fileData;
	}

	@Lob
	public Blob getPreviewData() {
		return previewBlob;
	}

	public void setPreviewData(Blob previewBlob) {
		this.previewBlob = previewBlob;
	}

	@Transient
	public byte[] getFileBytes() throws IOException, SQLException {
		if (data != null) return data;
		InputStream is = fileData.getBinaryStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int pos;
		while ((pos = is.read())!= -1) { 
			baos.write(pos);
		}
		return baos.toByteArray();
	}
	
	@Transient
	public void setFileBytes(byte[] data) {
		this.data = data;
	}
	
	@Transient
	public byte[] getPreviewBytes() throws IOException, SQLException {
		if (previewData != null) return previewData;
		InputStream is = previewBlob.getBinaryStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int pos;
		while ((pos = is.read())!= -1) { 
			baos.write(pos);
		}
		return baos.toByteArray();
	}
	
	@Transient
	public void setPreviewBytes(byte[] data) {
		this.previewData = data;
	}

	public String getParentClass() {
		return parentClass;
	}

	public void setParentClass(String parentClass) {
		this.parentClass = parentClass;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
}