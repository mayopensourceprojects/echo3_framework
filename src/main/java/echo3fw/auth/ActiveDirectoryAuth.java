/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.auth;

import static javax.naming.directory.SearchControls.SUBTREE_SCOPE;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import echo3fw.PDAppInstance;
import echo3fw.model.MUser;

//******************************************************************************
//**  ActiveDirectory
//*****************************************************************************/
/**
 * Provides static methods to authenticate users, change passwords, etc.
 *
 ******************************************************************************/

public class ActiveDirectoryAuth {

//	private static String[] userAttributes = { 
//			"distinguishedName", 
//			"cn", 
//			"name", 
//			"uid", 
//			"sn", 
//			"givenname", 
//			"memberOf",
//			"samaccountname", 
//			"userPrincipalName",
//			"isMemberOf",
//			"tokenGroups"};


	private LdapContext ctx;

	public MUser authenticate(String login, String password) {
		try{
			initConnection(login, password);
		    MUser user = getUser(login, "example.com");
		    ctx.close();
		    return user;
		} catch(Exception e){
		    e.printStackTrace();
		    return null;
		}
	}

	/**
	 * Used to authenticate a user given a username/password and domain name.
	 * Provides an option to identify a specific a Active Directory server.
	 */
	public void initConnection(String username, String password)
			throws NamingException {

		String url = "ldap://ldap.forumsys.com:389/";
		
		Hashtable<String, String> props = new Hashtable<>();		
		props.put(Context.PROVIDER_URL, url);
		props.put(Context.SECURITY_AUTHENTICATION, "simple");
		//props.put(Context.SECURITY_PRINCIPAL,  "cn=read-only-admin,dc=example,dc=com");
		props.put(Context.SECURITY_PRINCIPAL,  "uid=" + username + ",dc=example,dc=com");
		props.put(Context.SECURITY_CREDENTIALS, password);
		
		
		// Perform the search by using the URL
		props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		try {
			ctx = new InitialLdapContext(props, null);
		} catch (javax.naming.CommunicationException e) {
			throw new NamingException("Failed to connect to URL " + url);
		} catch (NamingException e) {
			throw new NamingException("Failed to authenticate " + username);
		}
	}

	public MUser getUser(String username, String domainName) {
		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SUBTREE_SCOPE);
			//controls.setReturningAttributes(new String[] { "memberOf" }); //userAttributes);
			String dc = "dc=example,dc=com";
			//dc = "ou=scientists,dc=example,dc=com";

			//String searchFilter = "(objectClass=user)";
			String searchFilter =  "(uid=" + username + ")";
			//searchFilter =  "(&)";
			//"(& (userPrincipalName=" + principalName + ")(objectClass=user))"
			
			NamingEnumeration<SearchResult> answer = ctx.search(dc, searchFilter, controls);
			if (!answer.hasMore()) {
				return null;
			}

			Attributes attr = answer.next().getAttributes();
			Attribute userAttr = attr.get("cn");
			if (userAttr == null) return null;
			
			String userID = (String) attr.get("uid").get();
			Criteria c = PDAppInstance.getHibernateSession().createCriteria(MUser.class);
			c.add(Restrictions.eq("uid", userID));
			MUser user = (MUser)c.uniqueResult();
			if (user == null) {
				user = new MUser();
				user.setUid(userID);
			}
			user.setName((String) attr.get("cn").get());
			
			String groupString = "";
			for (int i = 0; i < attr.get("objectclass").size(); i++) {
				groupString += attr.get("objectclass").get(i) + ",";
			}
			user.setGroups(groupString);
			PDAppInstance.getHibernateSession().saveOrUpdate(user);
			return user;			
		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}
	}

//	/**
//	 * Returns a list of users in the domain.
//	 */
//	public static User[] getUsers(LdapContext context) throws NamingException {
//
//		java.util.ArrayList<User> users = new java.util.ArrayList<User>();
//		String authenticatedUser = (String) context.getEnvironment().get(Context.SECURITY_PRINCIPAL);
//		if (authenticatedUser.contains("@")) {
//			String domainName = authenticatedUser.substring(authenticatedUser.indexOf("@") + 1);
//			SearchControls controls = new SearchControls();
//			controls.setSearchScope(SUBTREE_SCOPE);
//			controls.setReturningAttributes(userAttributes);
//			NamingEnumeration answer = context.search(toDC(domainName), "(objectClass=user)", controls);
//			try {
//				while (answer.hasMore()) {
//					Attributes attr = ((SearchResult) answer.next()).getAttributes();
//					Attribute user = attr.get("userPrincipalName");
//					if (user != null) {
//						users.add(new User(attr));
//					}
//				}
//			} catch (Exception e) {
//			}
//		}
//		return users.toArray(new User[users.size()]);
//	}
}