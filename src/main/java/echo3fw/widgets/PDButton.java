/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import echo3fw.util.ColorScheme;
import echo3fw.util.IImage;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Border;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.event.ActionListener;

public class PDButton extends Button {

	public PDButton(String text) {
		this();
		setText(text);
	}

	public PDButton(String text, IImage image) {
		this();
		setText(text);
		setIcon(PDUtil.getImage(image.getPath()));
	}

	public PDButton(String text, IImage image, ActionListener l) {
		this();
		setText(text);
		setIcon(PDUtil.getImage(image.getPath()));
		addActionListener(l);
	}

	public PDButton() {
		setInsets(new Insets(12, 3));
		setBackground(ColorScheme.LIGHT_BLUE);
		setTextAlignment(Alignment.ALIGN_CENTER);	
		setAlignment(Alignment.ALIGN_CENTER);	
		setFocusedBorder(new Border(1, Color.WHITE, Border.STYLE_DASHED));
		setRolloverEnabled(true);
		setIconTextMargin(new Extent(6));
		setTextPosition(Alignment.ALIGN_LEFT);	
	}
}
