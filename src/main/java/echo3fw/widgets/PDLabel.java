/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Color;
import nextapp.echo.app.Label;
import nextapp.echo.app.layout.GridLayoutData;

public class PDLabel extends Label {

	public static final int PLAIN = 0;
	public static final int HEADER_1 = 1;
	public static final int HEADER_2 = 2;
	public static final int HEADER_3 = 3;
	public static final int FIELD_LABEL = 5;
	public static final int FIELD_VALUE = 6;
	public static final int FIELD_VALUE_BORDERED = 11;

	public PDLabel(int style) {
		this(null, style, false);
	}

	public PDLabel(String text) {
		this(text, PLAIN, false);
	}

	public PDLabel(String text, boolean alignRight) {
		this(text, PLAIN, alignRight);
	}

	public PDLabel(String text, int style) {
		this(text, style, false);
	}


	public PDLabel(String text, int style, boolean alignRight) {
		super(text);
		setForeground(new Color(80, 80, 80)); //Color.DARKGRAY);

		switch (style) {
		case HEADER_1:
			setFont(PDUtil.getFontBold(20));
			break;
		case HEADER_2:
			setFont(PDUtil.getFont(18));
			break;
		case HEADER_3:
			setFont(PDUtil.getFont(16));
			break;
		case FIELD_LABEL:
//			setInsets(new Insets(5, 0));
			setFont(PDUtil.getFontBold(12));			
			break;
		case FIELD_VALUE:
//			setInsets(new Insets(5, 0));
			setFont(PDUtil.getFont(12));
			break;
		case FIELD_VALUE_BORDERED:
			setFont(PDUtil.getFont(12));
			setForeground(ColorScheme.DARK_GREY);
			setBackground(Color.WHITE);
			//setBorder(new Border(1, Color.LIGHTGRAY, Border.STYLE_DOTTED));
			break;
		}

		if (alignRight) {
			GridLayoutData gld = new GridLayoutData();
			gld.setAlignment(new Alignment(Alignment.RIGHT, 0));
			setLayoutData(gld);
		}
		
		setVisible(true);
	}

	public void setText(Number text) {
		super.setText(text == null ? "" : text + "");
	}

	@Override
	public void setText(String text) {
		if (text == null)
			text = "";
		super.setText(text);
	}
}
