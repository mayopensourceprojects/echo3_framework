/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EventListener;

import echo3fw.util.PDFormat;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Button;
import nextapp.echo.app.Column;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.app.event.EventListenerList;

public class PDTimeSpinner extends Row {

	private static final DateFormat DATE_FORMAT = PDFormat.getDefaultTimeOnlyFormat();
	private boolean editable;
	private Label lblView;
	private PDTextField txtValue;
	private Calendar model;
	private EventListenerList eventListenerList = new EventListenerList();
	private PDDateField txdDate;
	
	public PDTimeSpinner(boolean editable) {
		this.editable = editable;
		initGUI();
	}

	public void addActionListener(ActionListener listener) {
		eventListenerList.addListener(ActionListener.class, listener);
	}

	public void delActionListener(ActionListener listener) {
		eventListenerList.removeListener(ActionListener.class, listener);
	}

	protected void displayHour() {
		String s = model == null ? "" : DATE_FORMAT.format(model.getTime());
		if (!editable) {
			lblView.setText(s);
		} else {
			txtValue.setText(s);
		}
	}

	public void fireActionEvent(ActionEvent evt) {
		EventListener[] eventListeners = getActionListener();

		for (int i = 0; i < eventListeners.length; i++) {
			ActionListener listener = (ActionListener) eventListeners[i];
			listener.actionPerformed(evt);
		}
	}

	public EventListener[] getActionListener() {
		return eventListenerList.getListeners(ActionListener.class);
	}

	public Calendar getModel() {
		return model;
	}

	private void initGUI() {
		if (!editable) {
			lblView = new Label("");
			add(lblView);

			return;
		}

		setCellSpacing(new Extent(2));

		txdDate = new PDDateField();
		add(txdDate);
		
		Column col = new Column();
		add(col);

		Button btnUp = new Button(PDUtil.getImage("img/icons/spin_up.gif"));
		btnUp.addActionListener(e -> spnHourUpClicked());
		btnUp.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnUp.setWidth(new Extent(8));
		btnUp.setHeight(new Extent(6));
		col.add(btnUp);

		Button btnDown = new Button(PDUtil.getImage("img/icons/spin_down.gif"));
		btnDown.addActionListener(e -> spnHourDownClicked());
		btnDown.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnDown.setWidth(new Extent(8));
		btnDown.setHeight(new Extent(6));
		col.add(btnDown);

		txtValue = new PDTextField();
		txtValue.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		txtValue.setWidth(new Extent(40));
		txtValue.setKeyAction(true);
		txtValue.addActionListener(e -> txtValueChanged());
		add(txtValue);

		col = new Column();
		add(col);

		btnUp = new Button(PDUtil.getImage("img/icons/spin_up.gif"));
		btnUp.addActionListener(e -> spnMinsUpClicked());
		btnUp.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnUp.setWidth(new Extent(8));
		btnUp.setHeight(new Extent(6));
		col.add(btnUp);

		btnDown = new Button(PDUtil.getImage("img/icons/spin_down.gif"));
		btnDown.addActionListener(e -> spnMinsDownClicked());
		btnDown.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnDown.setWidth(new Extent(8));
		btnDown.setHeight(new Extent(6));
		col.add(btnDown);
	}

	public void setModel(Calendar model) {
		this.model = model;
		txdDate.setDate(model.getTime());
		displayHour();
	}

	protected void spnHourDownClicked() {
		if (model == null)
			model = Calendar.getInstance();

		if (model.get(Calendar.HOUR_OF_DAY) == 0) {
			model.set(Calendar.HOUR_OF_DAY, 23);
		} else {
			model.add(Calendar.HOUR_OF_DAY, -1);
		}
		displayHour();
		fireActionEvent(new ActionEvent(this, null));
	}

	protected void spnHourUpClicked() {
		if (model == null)
			model = Calendar.getInstance();

		if (model.get(Calendar.HOUR_OF_DAY) == 23) {
			model.set(Calendar.HOUR_OF_DAY, 0);
		} else {
			model.add(Calendar.HOUR_OF_DAY, 1);
		}
		displayHour();
		fireActionEvent(new ActionEvent(this, null));
	}

	protected void spnMinsDownClicked() {
		if (model == null)
			model = Calendar.getInstance();

		if (model.get(Calendar.MINUTE) == 0) {
			model.set(Calendar.MINUTE, 59);
		} else {
			model.add(Calendar.MINUTE, -1);
		}
		displayHour();
		fireActionEvent(new ActionEvent(this, null));
	}

	protected void spnMinsUpClicked() {
		if (model == null)
			model = Calendar.getInstance();

		if (model.get(Calendar.MINUTE) == 59) {
			model.set(Calendar.MINUTE, 0);
		} else {
			model.add(Calendar.MINUTE, 1);
		}
		displayHour();
		fireActionEvent(new ActionEvent(this, null));
	}

	protected void txtValueChanged() {
		try {
			if (txtValue.getText().length() == 0) {
				// user emptied the field, set value to null
				model = null;
			} else {
				if (model == null)
					model = Calendar.getInstance();
				Date date = DATE_FORMAT.parse(txtValue.getText());
				model.setTime(date);
			}
			fireActionEvent(new ActionEvent(this, null));
		} catch (Exception e) {
			// do nothing, the model stays at the previous value
			// just display what was before
			//displayHour();
			model = null;
		}
	}
}
