/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Border;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.TextField;
import nextapp.echo.app.Border.Side;
import nextapp.echo.app.event.ActionListener;

public class PDTextField extends TextField {

	public static final String PROPERTY_KEY_ACTION = "keyAction";
	
	public PDTextField() {
		initGUI();
	}
	
	public PDTextField(String renderId) {
		initGUI();
	}

	public PDTextField(int maxLength) {
		initGUI();
		setMaximumLength(maxLength);
	}

	protected void initGUI() {
		setKeyAction(false);
		setFont(PDUtil.getFont(14));
		setInsets(new Insets(3, 1, 3, 0));
		setWidth(new Extent(70));
		setAlignment(Alignment.ALIGN_LEFT);
		setEditable(true);
		
	}

	public void setKeyAction(boolean actionEnabled) {
	    set(PROPERTY_KEY_ACTION, actionEnabled);
    }
	
	public boolean isKeyAction() {
		return (Boolean)get(PROPERTY_KEY_ACTION);
    }
	
	@Override
	public void setEditable(boolean editable) {
		super.setEditable(editable);
		if (editable) {
			Side[] sides = new Side[4];
			sides[0] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[2] = new Side(1, ColorScheme.GREY, Border.STYLE_SOLID);
			sides[3] = new Side(3, ColorScheme.GREY, Border.STYLE_SOLID);
			setBorder(new Border(sides));
			setForeground(Color.BLACK);		
		} else {
			Side[] sides = new Side[4];
			sides[0] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[2] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
			sides[3] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
			setBorder(new Border(sides));
			setForeground(new Color(50, 50, 50));		
		}
	}
	
    public void setWidth(int width) {
    	super.setWidth(new Extent(width));
    }

    public void addActionListener(ActionListener l) {
    	setKeyAction(true);
    	super.addActionListener(l);
    }
}
