/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.util.List;

import nextapp.echo.app.list.DefaultListModel;

public class PDComboModel<T> extends DefaultListModel {

	public PDComboModel() {		
	}

	public PDComboModel(T[] values) {
		super(values);
	}

	public void add(String caption, T value) {
		MyListEntry entry = new MyListEntry();
		entry.caption = caption;
		entry.value = value;
		super.add(entry);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void add(Object o) {
		if (o == null) {
			return;
		}
		MyListEntry entry = new MyListEntry();
		if (o instanceof String) {
			entry.value = (T)o;
			entry.caption = o.toString();
		} else {
			entry.value = (T)o;
			entry.caption = entry.value.toString();
		}
		super.add(entry);
	}
	
	class MyListEntry {
		T value;
		String caption;
		
		public String toString() {
			return caption;
		}
	}

	@SuppressWarnings("unchecked")
	public MyListEntry getEntryIndex(Object item) {
        if (item != null) {
            for (int i = 0; i < size(); i++) {
            	MyListEntry thisEntry = (MyListEntry)get(i);
            	if (thisEntry != null && item.equals(thisEntry.value)) {
                    return thisEntry;
                }
            }
        }
		return null;
	}

	public void setValues(List<T> allValues) {
		removeAll();
        for (int i = 0; i < allValues.size(); ++i) {
            add(allValues.get(i));
        }
	}

	public void addAll(Object[] values) {
        for (int i = 0; i < values.length; ++i) {
            add(values[i]);
        }		
	}
}
