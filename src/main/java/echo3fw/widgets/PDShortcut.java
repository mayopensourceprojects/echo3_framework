/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import echo3fw.model.MShortcut;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Component;
import nextapp.echo.app.ContentPane;
import nextapp.echo.app.Extent;

/**
 * A <code>Component</code> which renders its contents in a floating, movable
 * window.
 * <p>
 * <b>NOTE:</b> A <code>WindowPane</code> may only be added to a
 * <code>ContentPane</code>.
 */
public class PDShortcut extends Component {

	public static final String PROPERTY_POSITION_X = "positionX";
	public static final String PROPERTY_POSITION_Y = "positionY";
	public static final String PROPERTY_ACTION_EVENT = "action";
	public static final String PROPERTY_TITLE = "title";
	public static final String PROPERTY_ICON = "icon";

	private MShortcut shortcut;

	public PDShortcut(MShortcut shortcut, String img) {
		this.shortcut = shortcut;
		String s = shortcut.getName();
		set(PROPERTY_TITLE, s);
		set(PROPERTY_ICON, PDUtil.getImage(img));
		set(PROPERTY_POSITION_X, new Extent(shortcut.getPositionX()));
		set(PROPERTY_POSITION_Y, new Extent(shortcut.getPositionY()));
	}

	@Override
	public boolean isValidChild(Component component) {
		return getComponentCount() == 0;
	}

	@Override
	public boolean isValidParent(Component parent) {
		return parent instanceof ContentPane;
	}

	@Override
	public void processInput(String inputName, Object inputValue) {
    	if (PROPERTY_POSITION_X.equals(inputName)) {
    		shortcut.setPositionX(((Extent) inputValue).getValue());
        } else if (PROPERTY_POSITION_Y.equals(inputName)) {
        	shortcut.setPositionY(((Extent) inputValue).getValue());
    		if (shortcut.getPositionX() < 40 && shortcut.getPositionY() < 68) {  //compare with PDShortcut.js line 138
    			//setVisible(false);
    			//PDAppInstance.getHibernateSession().delete(shortcut);
    		} else {
    			//PDAppInstance.getHibernateSession().saveOrUpdate(shortcut);  //store the new position
    		}        	
        } else if (PROPERTY_ACTION_EVENT.equals(inputName)) {
        	shortcut.executeTask();
        }
    }   
}
