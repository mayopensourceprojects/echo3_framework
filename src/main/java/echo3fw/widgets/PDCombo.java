/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.util.List;
import java.util.Vector;

import echo3fw.PDAppInstance;
import echo3fw.dao.DaoUser;
import echo3fw.model.MBase;
import echo3fw.model.MMnemonic;
import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import echo3fw.widgets.PDComboModel.MyListEntry;
import nextapp.echo.app.Border;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.SelectField;
import nextapp.echo.app.Border.Side;
import nextapp.echo.app.list.ListModel;

public class PDCombo<T> extends SelectField {

	public PDCombo() {
		super(new PDComboModel<T>());
		init2();
	}

	public PDCombo(T[] values) {
		this(arrayToList(values));
	}

	public PDCombo(List<T> values) {
		this(values, null);
	}

	public PDCombo(T[] values, String emptyEntry) {
		this(arrayToList(values), emptyEntry);
	}

	public PDCombo(List<T> values, String emptyEntry) {
		super(new PDComboModel<T>());
		setValues(values, emptyEntry);
		init2();
	}

	public void setValues(List<T> values) {
		setValues(values, null);		
	}

	public void setValues(List<T> values, String emptyEntry) {
		PDComboModel<T> model = getModel();
		model.removeAll();
		if (emptyEntry != null) {
			model.add(emptyEntry + " ", null);
		}
		if (values != null) {
			for (T o : values) {
				if (o == null) continue;
				model.add(o);
			}
		}
		setSelectedIndex(0);
	}


	private void init2() {
		setHeight(new Extent(25));
		setFont(PDUtil.getFont(14));
		setDisabledBackground(Color.WHITE);
		setDisabledForeground(ColorScheme.DARK_GREY);
		
		Side[] sides = new Side[4];
		sides[0] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
		sides[1] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
		sides[2] = new Side(1, Color.BLACK, Border.STYLE_SOLID);
		sides[3] = new Side(3, Color.BLACK, Border.STYLE_SOLID);
		setBorder(new Border(sides));
		
		sides = new Side[4];
		sides[0] = new Side(0, Color.WHITE, Border.STYLE_SOLID);
		sides[1] = new Side(0, Color.WHITE, Border.STYLE_SOLID);
		sides[2] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
		sides[3] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
		setDisabledBorder(new Border(sides));
	}

	// allow empty entries in the combo - a "null" entry would throw an exception
	@SuppressWarnings("unchecked")
	@Override
	public T getSelectedItem() {
		if (super.getModel().size() == 0) {
			return null;
		}
		Object o = super.getSelectedItem();
		if (PDUtil.isEmpty(o)) {
			return null;
		}
		return (T)((MyListEntry)o).value;
	}
	
    @SuppressWarnings("unchecked")
	public PDComboModel<T> getModel() {
        return (PDComboModel<T>)super.getModel();
    }    

    @Override
    public void setModel(ListModel lm) {
    	if (!(lm instanceof PDComboModel)) {
        	throw new IllegalArgumentException ("Model must be of type PDComboModel");
    	}
    	super.setModel(lm);
    }   
    
    @Override
    public void setSelectedItem(Object item) {
    	if (item == null) {
    		setSelectedIndex(0);
    	} else {
    		MyListEntry entry = getModel().getEntryIndex(item);
    		super.setSelectedItem(entry);
    	}
    }
    
    private static <TT> List<TT> arrayToList(TT[] array) {
    	List<TT> list = new Vector<TT>();
    	for (TT o : array) {
    		list.add(o);
    	}
    	return list;
    	
    }
    
	public void storeSelection(Class contextClazz, Class modelClass) {
		String context = contextClazz.getName();
		MMnemonic mnemomic = DaoUser.findMnemonic(context, modelClass);
		if (mnemomic == null) {
			mnemomic = new MMnemonic();
			mnemomic.setContext(context);
			mnemomic.setModelClass(modelClass.getName());
			//mnemomic.setUser(PDAppInstance.getCurrentUser());
		}
		Object selected = getSelectedItem();
		if (selected instanceof MBase) {
			mnemomic.setModelId((int)((MBase)selected).getId());
		} else if (selected instanceof Enum) {
			mnemomic.setEnumName(((Enum)selected).name());
		} else {
			mnemomic.setModelId(-1);
		}
		PDAppInstance.getHibernateSession().saveOrUpdate(mnemomic);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public void restoreSelection(Class contextClass, Class modelClass) {
		String context = contextClass.getName();
		MMnemonic mnemomic = DaoUser.findMnemonic(context, modelClass);
		if (mnemomic == null) return;
		try {
			if (modelClass.isAssignableFrom(MBase.class)) {
				T model = (T)MBase.loadById(modelClass, mnemomic.getModelId());
				setSelectedItem(model);
			} else if (mnemomic.getEnumName() != null) {
				Enum e = Enum.valueOf(modelClass, mnemomic.getEnumName());
				setSelectedItem(e);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}
}
