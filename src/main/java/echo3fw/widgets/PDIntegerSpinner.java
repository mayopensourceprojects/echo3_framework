/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.util.List;
import java.util.Vector;

import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Border;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Column;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Row;
import nextapp.echo.app.Border.Side;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;

public class PDIntegerSpinner extends Row {

	private PDNumericField txtValue;
	private List<ActionListener> actionListeners = new Vector<ActionListener>();
	public Button btnUp;
	public Button btnDown;
	private Integer maxValue;
	private Integer minValue = 0;
	public Column col;
	
	public PDIntegerSpinner() {
		initGUI();
	}
	
	public void setWidth(int width) {
		txtValue.setWidth(new Extent(width));
	}
	
	private void initGUI() {
		setCellSpacing(new Extent(6));
		
		txtValue = new PDNumericField(0);
		txtValue.setNumber(0);
		txtValue.setWidth(new Extent(80));
		txtValue.setFont(PDUtil.getFont(14));
		add(txtValue);
		
		col = new Column();
		col.setCellSpacing(new Extent(4));
		add(col);

		btnUp = new Button(PDUtil.getImage("img/icons16/up.gif"));
		btnUp.addActionListener(e -> spnUpClicked());
		btnUp.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnUp.setWidth(new Extent(12));
		btnUp.setHeight(new Extent(7));
		col.add(btnUp);

		btnDown = new Button(PDUtil.getImage("img/icons16/down.gif"));
		btnDown.addActionListener(e -> spnDownClicked());
		btnDown.setAlignment(new Alignment(Alignment.CENTER, Alignment.DEFAULT));
		btnDown.setWidth(new Extent(12));
		btnDown.setHeight(new Extent(7));
		col.add(btnDown);
		
		setEditable(true);
	}

	protected void spnDownClicked() {
		if (!txtValue.isEditable()) return;
		
		long value = getValue() - 1;
		if (minValue != null && value < minValue) {
			return;
		}
		txtValue.setNumber(value);
		fireActionPerformed();
	}


	protected void spnUpClicked() {
		if (!txtValue.isEditable()) return;
		
		long value = getValue() + 1;
		if (maxValue != null && value > maxValue) {
			return;
		}
		txtValue.setNumber(value);
		fireActionPerformed();
	}
	
	public long getValue() {
		return (long)txtValue.getNumber();
	}

	public int getIntValue() {
		return (int)txtValue.getNumber();
	}

	public void setValue(long value) {
		txtValue.setNumber(value);
	}
	
	public void addActionListener(ActionListener listener) {
		txtValue.setKeyAction(true);
		txtValue.addActionListener(listener);		
		actionListeners.add(listener);
	}
	
	private void fireActionPerformed() {
		ActionEvent e = new ActionEvent(this, "");
		for (ActionListener listener : actionListeners) {
			listener.actionPerformed(e);
		}
	}
	
	@Override
	public void setBorder(Border b) {
		txtValue.setBorder(b);
	}
	
	public PDNumericField getTxtValue() {
		return txtValue;
	}

	public void setEditable(boolean editable) {
		txtValue.setEditable(editable);
		btnUp.setVisible(editable);
		btnDown.setVisible(editable);

		Side[] sides = new Side[4];
		if (editable) {
			sides[0] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[2] = new Side(1, Color.BLACK, Border.STYLE_SOLID);
			sides[3] = new Side(3, Color.BLACK, Border.STYLE_SOLID);
			setForeground(Color.BLACK);		
		} else {
			sides[0] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[2] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
			sides[3] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
			setForeground(ColorScheme.DARK_GREY);		
		}
		setBorder(new Border(sides));
	}

	public void setMin(Integer minValue) {
		this.minValue = minValue;
	}

	public void setMax(Integer maxValue) {
		this.maxValue = maxValue;
	}
}
