/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.ImageReference;
import nextapp.echo.app.RadioButton;
import nextapp.echo.app.button.ButtonGroup;
import nextapp.echo.app.event.ActionListener;

public class PDButtonGroup<T> extends ButtonGroup {

	public PDButtonGroup() {		
	}

    public PDButtonGroup(Component parent, T[] values) {
    	for (T value : values) {
    		parent.add(createButton(value));
    	}
	}

	public T getSelectedValue() {
    	for (PDRadioButton<T> rdo : getPDButtons()) {
            if (rdo.isSelected()) {
            	return rdo.getValue();
            }
        }
    	return null;
    }


    public PDRadioButton<T> createButton(String caption) {
    	PDRadioButton<T> btn = new PDRadioButton<T>(caption);
    	btn.setGroup(this);
    	return btn;
    }

    public PDRadioButton<T> createButton(T value) {
    	return createButton(value, value + "");
    }

    public PDRadioButton<T> createButton(T value, String caption) {
    	PDRadioButton<T> btn = new PDRadioButton<T>(caption);
    	btn.setGroup(this);
    	btn.setValue(value);
    	return btn;
    }

    public PDRadioButton<T> createButton(T value, ImageReference icon) {
    	PDRadioButton<T> btn = new PDRadioButton<T>(icon);
    	btn.setGroup(this);
    	btn.setValue(value);
    	return btn;
    }

    public void setSelectedValue(T value) {
    	if (value != null) {
	    	for (PDRadioButton<T> rdo : getPDButtons()) {
    			if (value.equals(rdo.getValue())) {
    				rdo.setSelected(true);
    				return;
    			}
	        }
    	}
    	getButtons()[0].setSelected(true);
    }

    public void selectedFirst() {
    	if (getButtons().length > 0) {
    		getButtons()[0].setSelected(true);
    	}
    }
    
	public void addActionListener(ActionListener listener) {
    	for (PDRadioButton<T> rdo : getPDButtons()) {            
    		rdo.addActionListener(listener);
    	}
	}
	
	public PDRadioButton<T>[] getPDButtons() {
		RadioButton[] btnSource = getButtons();
		PDRadioButton<T>[] btnTarget = new PDRadioButton[btnSource.length];
		for (int i = 0; i < btnTarget.length; i++) {
			btnTarget[i] = (PDRadioButton<T>)btnSource[i];
		}
		return btnTarget;
	}
	
	@Override
	public void addButton(RadioButton radioButton) {
		if (!(radioButton instanceof PDRadioButton)) {
			throw new IllegalArgumentException("Only PDRadioButton allowed");
		}
		super.addButton(radioButton);
	}

	public void setEnabled(boolean enabled) {
    	for (PDRadioButton<T> rdo : getPDButtons()) {            
    		rdo.setEnabled(enabled);
    		rdo.setForeground(enabled ? Color.BLACK : Color.LIGHTGRAY);
    	}		
	}

	public void clear() {
		for (RadioButton rdo : getButtons()) {
			removeButton(rdo);
		}		
	}
}
