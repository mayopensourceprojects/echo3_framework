/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import echo3fw.gui.PDMessageBox;
import echo3fw.model.MMediaFile;

import echopoint.ContainerEx;
import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.Label;
import nextapp.echo.app.TaskQueueHandle;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.filetransfer.app.UploadSelect;
import nextapp.echo.filetransfer.app.event.UploadEvent;
import nextapp.echo.filetransfer.app.event.UploadListener;
import nextapp.echo.filetransfer.model.Upload;
import nextapp.echo.filetransfer.model.UploadProcess;

public class PDUpload extends ContainerEx {

	private UploadSelect uploadSelect;
	private TaskQueueHandle taskQueueHandle;
	private ApplicationInstance appInstance;
	private MMediaFile file;
	private List<ActionListener> listeners = new Vector<ActionListener>();
	
	public PDUpload() {
		appInstance = ApplicationInstance.getActive();
		taskQueueHandle = appInstance.createTaskQueue();
		initGUI();
	}
	
	public void initGUI() {
		removeAll();
		uploadSelect = new UploadSelect();
		uploadSelect.addUploadListener(new UploadListener() {
			public void uploadComplete(UploadEvent e) {
				appInstance.enqueueTask(taskQueueHandle, new MyUploadFinished(e));
			}
			@Override
			public void uploadSend() {
			}
		});
		add(uploadSelect);
	}

	class MyUploadFinished implements Runnable {
		private Upload upload;
		
		MyUploadFinished(UploadEvent uploadEvent) {
			upload = uploadEvent.getUpload();
		}
			
		public void run() {
			try {
				if (!isValidFile(upload.getFileName())) {
					PDMessageBox.msgBox("Warnung", "Invalid file format", 210, 110);
					removeAll();
					initGUI();
					return;
				}
				byte[] b = new byte[(int)upload.getSize()];

				file = null;
				try {
					upload.getInputStream().read(b, 0, (int)upload.getSize());
					file = new MMediaFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (file == null) return;
				
				file.setFileBytes(b);
//				file.setFileData(new BlobImpl(b));
				file.setFileName(upload.getFileName());
				file.setContentType(upload.getContentType());
				file.setFileSize(upload.getSize());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (upload.getInputStream() != null) {
						upload.getInputStream().close();
					}
	            } catch (IOException e) {
		            e.printStackTrace();
	            }	            
	            removeAll();
	            //initGUI();
	            
	            add(new Label("File is uploaded!"));
			}
			
			UploadProcess uploadProcess = new UploadProcess("Done");
			uploadProcess.setStatus(100);
			uploadSelect.setUploadProcess(uploadProcess);
			ActionEvent e = new ActionEvent(this, "");
			for (ActionListener l : listeners) {
				l.actionPerformed(e);
			}
		}
	}

	public MMediaFile getFile() {
		return file;
	}
	
	public void addActionListener(ActionListener listener) {
		listeners.add(listener);
    }

	protected boolean isValidFile(String fileName) {
		return true;
	}	
}
