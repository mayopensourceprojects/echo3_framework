/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import java.util.Date;

import echo3fw.util.ColorScheme;
import echopoint.jquery.DateField;
import nextapp.echo.app.Border;
import nextapp.echo.app.Color;
import nextapp.echo.app.Border.Side;

public class PDDateField extends DateField {

	public PDDateField() {
		setDateFormat("dd.MM.yyyy");
		setEditable(true);
	}

	public PDDateField(String renderId) {
		this();
		setRenderId(renderId);
	}

	@Override
	public void setDate(Date date) {
//		if (date == null) {
//			return; //XXX this is a bug!
//		}
		super.setDate(date);
	}

	@Override
	public void setEditable(boolean enabled) {
		//setBackground(Color.WHITE);
		
		super.setEnabled(enabled);
		Side[] sides = new Side[4];
		if (enabled) {
			sides[0] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[2] = new Side(0, Color.BLACK, Border.STYLE_SOLID);
			sides[3] = new Side(3, Color.BLACK, Border.STYLE_SOLID);
			setForeground(Color.BLACK);
		} else {
			sides[0] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[1] = new Side(0, Color.DARKGRAY, Border.STYLE_SOLID);
			sides[2] = new Side(0, Color.DARKGRAY, Border.STYLE_DOTTED);
			sides[3] = new Side(1, Color.DARKGRAY, Border.STYLE_DOTTED);
			setForeground(ColorScheme.DARK_GREY);
		}
		setBorder(new Border(sides));
	}
}
