/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.widgets;

import echo3fw.util.PDUtil;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.Grid;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;
import nextapp.echo.app.layout.GridLayoutData;

public class PDGrid extends Grid {

	private final static Color colorBlue = new Color(221, 235, 247);
	
	public PDGrid(int cols) {
		super(cols);
		setInsets(new Insets(4));
	}

	public <T extends Component> T addEditor(T c) {
		add(c);
		return c;
	}

	public Component add(Component c, Alignment alignment) {
		GridLayoutData gld = (GridLayoutData)c.getLayoutData();
		if (gld == null) {
			gld = new GridLayoutData();
			c.setLayoutData(gld);
		}
		gld.setAlignment(alignment);
		add(c);
		return c;
	}

	public <T extends Component> T addFill(T c) {
		GridLayoutData gld = (GridLayoutData)c.getLayoutData();
		if (gld == null) {
			gld = new GridLayoutData();
			c.setLayoutData(gld);
		}
		gld.setColumnSpan(GridLayoutData.SPAN_FILL);
		add(c);	
		return c;
	}

	public PDLabel addLabel(String text) {
		PDLabel lbl = new PDLabel(text, PDLabel.FIELD_LABEL);
		add(lbl, Alignment.ALIGN_TOP);
		return lbl;
	}

	public PDLabel addLabelValue(String text) {
		PDLabel lbl = new PDLabel(text, PDLabel.FIELD_VALUE);
		add(lbl, Alignment.ALIGN_TOP);
		return lbl;
	}

	public Label addEmpty() {
		Label lbl = new Label();
		add(lbl);
		return lbl;
	}

	public Label add(boolean left, boolean blue, boolean bold, String text) {
		GridLayoutData gld = new GridLayoutData();
		if (left) {
			gld.setAlignment(Alignment.ALIGN_LEFT);
		} else {
			gld.setAlignment(Alignment.ALIGN_RIGHT);
		}
		if (blue) {
			gld.setBackground(colorBlue);
		}
		Label lbl = new Label(text);
		lbl.setForeground(new Color(80, 80, 80));
		lbl.setLayoutData(gld);
		if (bold) {
			lbl.setFont(PDUtil.getFontBold(14));
		} else {
			lbl.setFont(PDUtil.getFont(12));
		}
		add(lbl);
		return lbl;
	}
}
