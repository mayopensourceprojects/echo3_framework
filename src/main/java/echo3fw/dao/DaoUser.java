/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import echo3fw.PDAppInstance;
import echo3fw.model.MMnemonic;
import echo3fw.model.MShortcut;
import echo3fw.model.MUser;
import echo3fw.util.PDUtil;

/**
 * The DAO to find/load/save MUser instances
 * 
 * @author Christof May
 */
public class DaoUser implements IDAO {


	public static MUser findUserById(int userId) {
		Criteria criteria = PDAppInstance.getHibernateSession().createCriteria(MUser.class);
		criteria.add(Restrictions.idEq(userId));
		return (MUser)criteria.uniqueResult();
	}
	
	
	public static MMnemonic findMnemonic(String context, Class modelClass) {
		Session session = PDAppInstance.getHibernateSession();
		Criteria criteria = session.createCriteria(MMnemonic.class);
		criteria.add(Restrictions.eq("context", context));
		criteria.add(Restrictions.eq("modelClass", modelClass.getName()));
		//XXX criteria.add(Restrictions.eq("user", PDAppInstance.getCurrentUser()));
		return (MMnemonic)criteria.uniqueResult();
	}

	/**
	 * Reachable user means all the users assigned to projects the projectAdmin is owner of
	 */
	public static Criteria findReachableUsers(MUser projectAdmin, String login) {
		
		Session session = PDAppInstance.getHibernateSession();
		Criteria criteria = session.createCriteria(MUser.class);
		
//		if (projectAdmin != null) {
//			DetachedCriteria userRoles = DetachedCriteria.forClass(MUserRole.class, "ta");
//			userRoles.add(Restrictions.eq("userRef", projectAdmin));
//			//assignments.add(Restrictions.eq("owner", true));
//			userRoles.setProjection(Projections.property("ta.role"));
//		}
		
//		DetachedCriteria dc = DetachedCriteria.forClass(MTenantAssignment.class);
//		dc.add(Subqueries.propertyIn("tenant", userRoles));
//		dc.createAlias("user", "u");
//		dc.setProjection(Projections.property("u.login"));
//		List l = dc.getExecutableCriteria(session).list();
//		criteria.add(Restrictions.in("login", l));
		if (!PDUtil.isEmpty(login)) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.like("firstName", login + "%"));
			disjunction.add(Restrictions.like("lastName", login + "%"));
			disjunction.add(Restrictions.like("login", login + "%"));
			criteria.add(disjunction);
		}		
		criteria.addOrder(Order.asc("cachedTitle"));
		return criteria;
	}

	public static MUser findUserByLogin(String login) {
		Criteria criteria = PDAppInstance.getHibernateSession().createCriteria(MUser.class);
		criteria.add(Restrictions.eq("login", login));
		return (MUser)criteria.uniqueResult();
	}
	
	public static List<MShortcut> findShortcuts(MUser user) {
		Criteria criteria = PDAppInstance.getHibernateSession().createCriteria(MShortcut.class);
		criteria.add(Restrictions.eq("owner", user));
		return criteria.list();
	}

	public static List<MUser> findAllUsers() {
		Criteria criteria = PDAppInstance.getHibernateSession().createCriteria(MUser.class);
		criteria.addOrder(Order.asc("lastName"));
		criteria.addOrder(Order.asc("firstName"));
		return criteria.list();
    }	
}
