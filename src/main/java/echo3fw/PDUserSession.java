/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.io.Serializable;
import java.util.Calendar;

import echo3fw.auth.ActiveDirectoryAuth;
import echo3fw.dao.DaoUser;
import echo3fw.model.MUser;
import echo3fw.util.PDFormat;

public class PDUserSession implements Serializable {

	protected MUser user;
	protected DaoUser daoUser;
	
	public PDUserSession() {
		daoUser = new DaoUser();
	}

	public void doLogin(String login, String password) {		
		ActiveDirectoryAuth auth = new ActiveDirectoryAuth();		
		user = auth.authenticate(login, password);
		System.out.println(PDFormat.formatLong(Calendar.getInstance().getTime()) + " ::: " + login + " ::: " + (user != null ? "OK" : "Err"));
	}

	public void doLogout() {
		user = null;
	}

	public boolean isLoggedIn() {
		return user != null;
	}

	public MUser getUser() {
		return user;
	}
	
	public void init() {
		//override if applicable
	}
}