/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.internal;

import echo3fw.util.PDUtil;
import echo3fw.widgets.PDLabel;
import echopoint.ContainerEx;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Border;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Column;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.FillImage;
import nextapp.echo.app.Grid;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.app.layout.RowLayoutData;

/**
 * @author Christof May
 */
public class PDExpandableFilter extends ContainerEx {

	private Grid grid;
	private Button btnExpand;
	private Button btnSearch;
	private Button btnReset;
	private ActionListener expandListener;
	private boolean isInitialized = false;
	
	public PDExpandableFilter(ActionListener expandListener) {
		this.expandListener = expandListener;
		initGUI();
	}

	private void initGUI() {
		final Row row = new Row();
		add(row);

		grid = new Grid(2);
		grid.setInsets(new Insets(3, 3, 2, 3));
		RowLayoutData rld = new RowLayoutData();
		rld.setBackgroundImage(new FillImage(PDUtil.getImage("img/semitrans4.png")));
		rld.setAlignment(new Alignment(Alignment.TOP, Alignment.TOP));
		rld.setInsets(new Insets(5));
		grid.setLayoutData(rld);
		row.add(grid);

		Column colCommands = new Column();
		colCommands.setBackground(Color.ORANGE);
		rld = new RowLayoutData();
		rld.setAlignment(new Alignment(Alignment.TOP, Alignment.TOP));
		colCommands.setLayoutData(rld);
		row.add(colCommands);

		btnExpand = new Button("F\ni\nl\nt\ne\nr");
		btnExpand.setLineWrap(true);
		btnExpand.setBackground(Color.ORANGE);
		btnExpand.setForeground(Color.DARKGRAY);
		btnExpand.setInsets(new Insets(6, 12, 0, 0));
		btnExpand.setBorder(new Border(new Extent(1), Color.DARKGRAY, Border.STYLE_DOTTED));
		btnExpand.setHeight(new Extent(110));
		btnExpand.setWidth(new Extent(14));
		btnExpand.setRolloverEnabled(false);
		btnExpand.addActionListener(e -> {
			btnReset.setVisible(!btnReset.isVisible());
			btnSearch.setVisible(!btnSearch.isVisible());
			expandListener.actionPerformed(e);
		});
		colCommands.add(btnExpand);

		btnReset = new Button(PDUtil.getImage("img/undo.gif"));
		btnReset.setBorder(PDUtil.emptyBorder());
		btnReset.setBackground(Color.ORANGE);
		btnReset.setRolloverEnabled(false);
		btnReset.setVisible(false);
		colCommands.add(btnReset);

		btnSearch = new Button(PDUtil.getImage("img/generado.gif"));
		btnSearch.setBorder(PDUtil.emptyBorder());
		btnSearch.setBackground(Color.ORANGE);
		btnSearch.setRolloverEnabled(false);
		btnSearch.setInsets(new Insets(0, 0, 0, 5));
		btnSearch.setVisible(false);
		colCommands.add(btnSearch);

	}

	public void addFilter(String caption, Component filterComponent) {
		grid.add(new PDLabel(caption, PDLabel.FIELD_LABEL));
		grid.add(filterComponent);
	}
	
	@Override
	public void setWidth(Extent width) {
		grid.setWidth(new Extent(width.getValue() - 10));
	}

	public void setResetListener(ActionListener resetListener) {
		btnReset.addActionListener(resetListener);		
    }
	
	public void setOkListener(ActionListener okListener) {
		btnSearch.addActionListener(okListener);
		isInitialized = true;
    }

	public boolean isInitialized() {
		return isInitialized;
    }

	public Grid getGrid() {
    	return grid;
    }
}
