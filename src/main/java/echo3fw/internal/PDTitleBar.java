/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.internal;

import echo3fw.gui.PDMasterDataView;
import echo3fw.gui.PDMessageBox;
import echo3fw.util.HeaderValue;
import echo3fw.util.PDUtil;
import echopoint.Strut;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Grid;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionListener;

/**
 * 
 * 
 * @author Christof May
 * @date 25.01.2009
 */
public class PDTitleBar extends Grid {

	protected Button btnCancel;
	protected Button btnDelete;
	protected Button btnEdit;
	protected Button btnSave; 
	protected Label btnTitle;
	protected Row commandRow;
	protected boolean deleteable;
	protected HeaderValue headerValues;
	protected ActionListener titleChangeListener;
	protected boolean editable;
	protected PDMasterDataView parentForm;

	public PDTitleBar(PDMasterDataView parentForm, HeaderValue headerValues, int cols) {
		super(cols);
		this.parentForm = parentForm;
		this.headerValues = headerValues;
		this.editable = true;
		this.deleteable = true;
		initGUI();
	}

	public PDTitleBar(PDMasterDataView parentForm, HeaderValue headerValues, boolean editable, boolean deleteable) {
		super(3);
		this.parentForm = parentForm;
		this.headerValues = headerValues;
		this.editable = editable;
		this.deleteable = deleteable;
		initGUI();
	}

	public Button addCommand(String text, String image, String toolTipText, String renderId) {
		renderId = getClass().getSimpleName() + headerValues.rowIndex + renderId;
		Button btn = new Button(text);
		btn.setBorder(PDUtil.emptyBorder());
		btn.setInsets(new Insets(3, 0, 0, 0));
		btn.setHeight(new Extent(16));
		btn.setRolloverEnabled(false);
		btn.setForeground(Color.DARKGRAY);
		btn.setBackground(getBackground());
		if (image != null) {
			btn.setIcon(PDUtil.getImage(image));
		} else {
			btn.setInsets(new Insets(0, 3, 0, 0));
		}
		if (toolTipText != null) {
			btn.setToolTipText(toolTipText);
		}
		if (text != null) {
			btn.setText(text);
			btn.setFont(PDUtil.getFont(9));
			btn.setIconTextMargin(new Extent(2));
		}
		commandRow.add(btn);
		return btn;
	}

	protected void addCommands() {
		//override if applicable
	}

	protected void btnDeleteClicked() {
		PDMessageBox.confirmDeletion().addActionListener(e -> btnDeleteClickedBis());
	}

	protected void btnDeleteClickedBis() {
		parentForm.deleteItem();
	}

	public void setEditing(boolean editing) {
		btnEdit.setVisible(!editing);
		btnCancel.setVisible(editing);
		btnSave.setVisible(editing);
	}
	
	protected void initEditingCommands() {
		// extend if needed
	}

	protected void initGUI() {		
		setBorder(PDUtil.emptyBorder());
		
		setWidth(new Extent(100, Extent.PERCENT));
		setInsets(new Insets(0, 1, 0, 1));
		setColumnWidth(0, new Extent(1));

		btnTitle = new Label(PDUtil.getImage("img/arrowleft.gif"));
		btnTitle.setRenderId(getClass().getSimpleName() + "" + headerValues.id);
		btnTitle.setIconTextMargin(new Extent(0));
		btnTitle.setFont(PDUtil.getFont(11));
		btnTitle.setForeground(Color.BLACK);
		btnTitle.setLineWrap(false);
		
		add(btnTitle);
		if (headerValues != null) {
			setTitle(headerValues.title);
		}

		commandRow = new Row();
		commandRow.setAlignment(new Alignment(Alignment.RIGHT, Alignment.DEFAULT));
		add(commandRow);

		initEditingCommands();
		addCommands();		
		addEditCommands();		

		if (deleteable) {
			commandRow.add(new Strut(6, 0));
			btnDelete = addCommand(null, "img/deletee.gif", "Löschen", "delete");
			btnDelete.addActionListener(e -> btnDeleteClicked());
		}
	}

	protected void btnEditClicked() {
		setEditing(true);
		parentForm.startEditing();
	}

	protected void btnSaveClicked() {
		boolean ok = parentForm.saveEditing();
		if (ok) {
			setEditing(false);
		}
	}

	protected void btnCancelClicked() {
		parentForm.cancelEditing();
		setEditing(false);
	}

	protected void addEditCommands() {
		if (editable) {
			commandRow.add(new Strut(11, 0));

			btnEdit = addCommand(null, "img/edit.gif", "Bearbeiten", "edit");
			btnEdit.addActionListener(e -> btnEditClicked());
			
			btnCancel = addCommand(null, "img/undo.gif", "Abbrechen", "cancel");
			btnCancel.addActionListener(e -> btnCancelClicked());
			btnCancel.setVisible(false);

			btnSave = addCommand(null, "img/save.gif", "Speichern", "save");
			btnSave.addActionListener(e -> btnSaveClicked());
			btnSave.setVisible(false);
		}
	}

	public void addSaveListener(ActionListener saveListener) {
		btnSave.addActionListener(saveListener);
	}

	public void addCancelListener(ActionListener cancelListener) {
		btnCancel.addActionListener(cancelListener);
	}

	public void setTitle(String title) {
		if (btnTitle != null && title != null) {
			if (title.length() > 55) {
				title = title.substring(0, 52) + "...";
			}
			btnTitle.setText(title);
		}
		if (headerValues != null) {
			headerValues.title = title;
		}
	}

	public void setTitleChangeListener(ActionListener titleChangeListener) {
		this.titleChangeListener = titleChangeListener;
	}
	
	public void setBackground(Color c) {
		super.setBackground(c);
		if (commandRow != null) {
			for (Component cp : commandRow.getComponents()) {
				cp.setBackground(c);
			}
		}
		if (btnTitle != null) {
			btnTitle.setBackground(c);
		}
	}
}