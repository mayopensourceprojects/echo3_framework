package echo3fw.gui;

import echo3fw.PDEventBus;
import echo3fw.events.IUpdateListener;
import echo3fw.model.IOrdered;
import echo3fw.util.PDUtil;
import echopoint.ContainerEx;
import echopoint.Strut;
import echopoint.able.Positionable;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.ImageReference;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.extras.app.ReorderHandle;

public abstract class PDDragableRow<T extends IOrdered> extends Row {

	private Button btnInfo;
	private Button btnName;
	private ReorderHandle reorderHandle;
	private IUpdateListener<T> updateListener;
	private boolean isSelectable;
	private T entity;
	

	public PDDragableRow(T entity) {
		this(entity, true, true);
	}

	public PDDragableRow(T entity, boolean isSelectable, boolean isReorderable) {
		this.isSelectable = isSelectable;
		this.entity = entity;
		
		setInsets(new Insets(3));

		if (isReorderable) {
			reorderHandle = new ReorderHandle();
			reorderHandle.setEnabled(isSelectable);
			reorderHandle.setIcon(PDUtil.getImage("img/icons16/Share-02-16.png"));
			add(reorderHandle);
		}

		add(new Strut(8, 0));
		
		btnName = new Button();
		btnName.setFont(PDUtil.getFont(14));
		add(btnName);

		ContainerEx ctn = new ContainerEx();
		ctn.setRight(new Extent(20));
		ctn.setWidth(new Extent(80));
		ctn.setHeight(new Extent(25));
		ctn.setPosition(Positionable.ABSOLUTE);
		add(ctn);

		ContainerEx ctn2 = new ContainerEx();
		ctn2.setTop(new Extent(-10));
		ctn.add(ctn2);

		btnInfo = new Button();
		btnInfo.setFont(PDUtil.getFont(14));
		btnInfo.setAlignment(Alignment.ALIGN_RIGHT);
		ctn2.add(btnInfo);

		if (isSelectable) {
			ActionListener actionListener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for (Component c : getParent().getComponents()) {
						((PDDragableRow<T>) c).setSelected(false);
					}
					setSelected(true);
					PDEventBus.getInstance().notifySelection(entity);
					onSelection();
				}
			};
			btnName.addActionListener(actionListener);
			btnInfo.addActionListener(actionListener);
		} else {
			btnName.setEnabled(false);
			btnInfo.setEnabled(false);
		}

		updateListener = new IUpdateListener<T>() {
			@Override
			public void updated(T updatedEntity) {
				updateValue(updatedEntity);
			}
		};
		PDEventBus.getInstance().addUpdateListener(entity, updateListener);

		updateValue(entity);
	}
	
	public T getEntity() {
		return entity;
	}
	
	protected void onSelection() {		
	}

	public void setName(String name) {
		btnName.setText(name);
	}

	public void setInfo(String info) {
		btnInfo.setText(info);
	}

	public void setIcon(ImageReference icon) {
		if (reorderHandle != null) {
			reorderHandle.setIcon(icon);
		}
	}

	public void setSelected(boolean selected) {
		if (!isSelectable) return;
		setBackground(selected ? Color.LIGHTGRAY : Color.WHITE);
	}

	public abstract void updateValue(T requirement);
}
