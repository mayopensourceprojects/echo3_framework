/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.List;
import java.util.Vector;

import echo3fw.model.MBase;
import echo3fw.util.ICrud;
import nextapp.echo.app.Component;

	
public abstract class PDTabCrud extends PDTab implements ICrud {

	private List<ICrud> tabs;
	
	public PDTabCrud() {
		tabs = new Vector<ICrud>();
		initGUI2();		
	}
	
	public void applyToModel() {
		for (ICrud tab : tabs) {
			tab.applyToModel();
		}
	}

	protected abstract void initGUI2();

	public Component getFocusComponent() {
		if (tabs.size() > 0) {
			return tabs.get(0).getFocusComponent();
		}
		return null;
	}

	public void readFromModel(MBase model) {
		for (ICrud tab : tabs) {
			tab.readFromModel(model);
		}
	}

	public void setEditing(boolean isEditing) {
		for (ICrud tab : tabs) {
			tab.setEditing(isEditing);
		}
	}

	public String getError() {
		for (ICrud tab : tabs) {
			String error = tab.getError();
			if (error != null) {
				//setSelectedIndex(pos);
				return error;
			}
		}
		return null;
	}
	
    public void addTab2(String caption, ICrud tab) {
    	tabs.add(tab);
    	addTab(caption, (Component)tab);
    }
}