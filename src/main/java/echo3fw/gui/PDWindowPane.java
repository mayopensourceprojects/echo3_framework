/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import echo3fw.PDAppInstance;
import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.FillImage;
import nextapp.echo.app.FillImageBorder;
import nextapp.echo.app.Insets;
import nextapp.echo.app.WindowPane;

/**
 * A general purpose floating window 
 */
public class PDWindowPane extends WindowPane {

	
	public PDWindowPane() {
		initGUI();
	}

	private void initGUI() {
		setClosable(true);
		setWidth(new Extent(500));
		setHeight(new Extent(450));
		setInsets(new Insets(6));
		setIconInsets(new Insets(7, 3, 0, 0));
		setTitleFont(PDUtil.getFontBold(14));
		setTitleForeground(Color.WHITE);
		setTitleHeight(new Extent(28));
		setTitleBackground(ColorScheme.GREY);
		setTitleInsets(new Insets(25, 5, 0, 0));

		FillImageBorder border = null;
		if (((PDAppInstance) ApplicationInstance.getActive()).isIE()) {
			//IE6 does not support semitransparent images... :(
			//show a simple frame instead
			border = new FillImageBorder(Color.DARKGRAY, new Insets(2, 1, 2, 2), new Insets(2, 0, 2, 2));
		} else {
			border = new FillImageBorder(null, new Insets(10), new Insets(6));
			border.setFillImage(FillImageBorder.TOP_LEFT, new FillImage(PDUtil.getImage(
					"img/border/BorderTopLeft.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.TOP, new FillImage(PDUtil.getImage(
					"img/border/BorderTop.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.TOP_RIGHT, new FillImage(PDUtil.getImage(
					"img/border/BorderTopRight.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.LEFT, new FillImage(PDUtil.getImage(
					"img/border/BorderLeft.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.RIGHT, new FillImage(PDUtil.getImage(
					"img/border/BorderRight.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.BOTTOM_LEFT, new FillImage(PDUtil.getImage(
					"img/border/BorderBottomLeft.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.BOTTOM, new FillImage(PDUtil.getImage(
					"img/border/BorderBottom.png"), null, null, FillImage.REPEAT));
			border.setFillImage(FillImageBorder.BOTTOM_RIGHT, new FillImage(PDUtil.getImage(
					"img/border/BorderBottomRight.png"), null, null, FillImage.REPEAT));
		}
		setBorder(border);
	}
}
