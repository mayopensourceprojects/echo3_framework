/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import echo3fw.util.ColorScheme;
import echo3fw.util.PDUtil;
import nextapp.echo.app.Border;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.FillImageBorder;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Border.Side;
import nextapp.echo.extras.app.BorderPane;
import nextapp.echo.extras.app.TabPane;
import nextapp.echo.extras.app.layout.TabPaneLayoutData;

public class PDTab extends TabPane {

	public PDTab() {
		initGUI();
	}
	
	protected void initGUI() {
		//setBorder(PDUtil.getGreyBorder());
		setInsets(new Insets(6, 8, 6, 0));
		//setBorderType(TabPane.BORDER_TYPE_ADJACENT_TO_TABS);
		
		setFont(PDUtil.getFont(14));
		setBorder(new Border(0, Color.BLUE, Border.STYLE_SOLID));
		Side[] sides = new Side[4];
		sides[0] = new Side(1, Color.LIGHTGRAY, Border.STYLE_SOLID);
		sides[1] = new Side(5, Color.BLACK, Border.STYLE_SOLID);
		sides[2] = new Side(2, Color.BLUE, Border.STYLE_SOLID);
		sides[3] = new Side(4, Color.LIGHTGRAY, Border.STYLE_SOLID);
		setTabActiveBorder(new Border(sides));
		setTabInactiveBorder(new Border(sides));
		setTabActiveBackground(ColorScheme.LIGHT_BLUE);
		
	}

	public void addTab(String tabTitle, Component tabContent) {
		TabPaneLayoutData tpld = new TabPaneLayoutData();
		tpld.setTitle(tabTitle);
		tabContent.setLayoutData(tpld);
		add(tabContent);
	}
	
	public void addTabWithInset(String tabTitle, Component tabContent) {
		BorderPane pane = new BorderPane();
		pane.setBorder(new FillImageBorder(Color.TRANSPARENT, new Insets(0), new Insets(9)));
		pane.add(tabContent);
		addTab(tabTitle, pane);
	}
}