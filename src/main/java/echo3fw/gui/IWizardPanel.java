/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import nextapp.echo.app.Component;

public interface IWizardPanel {

	public default void applyToModel() {
	}

	public default void applyToModel2() throws Exception {
	}

	public default boolean doBackAction() {
		return true;
	}

	public default boolean doNextAction() {
		return true;
	}

	public default int getBackButtonWidth() {
		return 80;
	}

	public default String getBackCaption() {
		return "Back";
	}

	public default String getError() {
		return null;
	}

	public default Component getFocusComponent() {
		return null;
	}

	public default int getNextButtonWidth() {
		return 80;
	}

	public default String getNextCaption() {
		return "Next";
	}

	public default boolean isApplicable() {
		return true;
	}

	public default void readFromModel() {
	}
}
