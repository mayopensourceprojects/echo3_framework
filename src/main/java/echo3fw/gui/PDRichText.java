/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.lang.reflect.Method;
import java.util.HashMap;

import echo3fw.PDAppInstance;
import echo3fw.util.PDUtil;
import echo3fw.widgets.PDRichTextArea;
import echopoint.ContainerEx;
import echopoint.HtmlLabel;
import echopoint.able.Positionable;
import echopoint.able.Scrollable;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Border;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.SplitPane;
import nextapp.echo.app.layout.SplitPaneLayoutData;
import nextapp.echo.extras.app.RichTextArea;
import nextapp.echo.extras.app.ToolTipContainer;
import nextapp.echo.extras.app.TransitionPane;

public class PDRichText extends SplitPane {

	private PDRichTextArea richTextArea;
	private Button btn;
	private Button btnSave;
	private Button btnCancel;
	private Object targetObject;
	private String targetMethod;
	private HtmlLabel lblHtml;
	private ContainerEx ctnHtml;
	private Label lblCaption;
	private String emptyText;
	
	
	public PDRichText(String targetMethod, String title) {
		this(targetMethod, title, "...");
	}

	public PDRichText(String targetMethod, String title, String emptyText) {
		super(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM);
		
		this.targetMethod = targetMethod.substring(0, 1).toUpperCase() + targetMethod.substring(1);
		this.emptyText = emptyText;
		
		lblCaption = new Label();
		if (title != null) {
			setSeparatorPosition(new Extent(28));
			lblCaption.setFont(PDUtil.getFontBold(16));
			lblCaption.setForeground(Color.WHITE);
			lblCaption.setText(title);
			SplitPaneLayoutData spld = new SplitPaneLayoutData();
			spld.setInsets(new Insets(0, 6, 0, 0));
			lblCaption.setLayoutData(spld);
			
			setSeparatorHeight(new Extent(3));
			setSeparatorColor(Color.WHITE);
		} else {
			setSeparatorPosition(new Extent(0));
		}
		add(lblCaption);
		
		ctnHtml = new ContainerEx();
		ctnHtml.setHeight(new Extent(97, Extent.PERCENT));
		add(ctnHtml);
		
		ContainerEx ctnHtml2 = new ContainerEx();		
		ctnHtml2.setTop(new Extent(6));
		ctnHtml2.setInsets(new Insets(0, 0, 0, 0));
		ctnHtml2.setHeight(new Extent(100, Extent.PERCENT));
		ctnHtml2.setScrollBarPolicy(Scrollable.AUTO);
		ctnHtml2.setBackground(Color.WHITE);
		ctnHtml.add(ctnHtml2);
		
		lblHtml = new HtmlLabel();		
		lblHtml.setFont(PDUtil.getFontLight(14));
		lblHtml.setStyleName("htmlEditor");
		ctnHtml2.add(lblHtml);

		ContainerEx ctnHtmlButton = new ContainerEx();
		ctnHtmlButton.setPosition(Positionable.ABSOLUTE);
		ctnHtmlButton.setWidth(new Extent(97, Extent.PERCENT));
		ctnHtmlButton.setHeight(new Extent(100, Extent.PERCENT));
		ctnHtmlButton.setTop(new Extent(0));
		ctnHtml.add(ctnHtmlButton);

		btn = new Button();
		btn.setWidth(new Extent(100, Extent.PERCENT));
		btn.setHeight(new Extent(200, Extent.PX));
		btn.addActionListener(e -> showMode(true));
		btn.setTextAlignment(new Alignment(Alignment.TOP, Alignment.LEFT));
		ctnHtmlButton.add(btn);
		
		//
		btnSave = new Button(PDUtil.getImage("img/buttons/Save-24.png"));
		btnSave.addActionListener(e -> {
			updateValue();
			showMode(false);
		});
		btnCancel = new Button(PDUtil.getImage("img/buttons/Undo-24.png"));
		btnCancel.addActionListener(e -> {
			richTextArea.setText(lblHtml.getText());
			showMode(false);
		});

		initRichText();
	}

	public void showMode(boolean editing) {
		PDRichText.this.removeAll();
		add(lblCaption);
		
		if (editing) {
			ContainerEx ctn = new ContainerEx();
			ctn.setHeight(new Extent(92, Extent.PERCENT));
			ctn.setWidth(new Extent(96, Extent.PERCENT));
			ctn.setOutsets(new Insets(6));
			PDRichText.this.add(ctn);

			TransitionPane pane = new TransitionPane();
			ctn.add(pane);

			pane.add(richTextArea);

			ContainerEx ctnHelp = new ContainerEx();
			ctnHelp.setPosition(Positionable.ABSOLUTE);
			ctnHelp.setAlignment(Alignment.ALIGN_RIGHT);
			ctnHelp.setRight(new Extent(12));
			ctnHelp.setTop(new Extent(3));
			ctn.add(ctnHelp);
			
			ToolTipContainer toolTipContainer = new ToolTipContainer();
			toolTipContainer.add(new Label(PDUtil.getImage("img/help.gif")));
			
			HtmlLabel lblHelp = new HtmlLabel("This is a <b>help</b>!");
			lblHelp.setBackground(Color.YELLOW);
			lblHelp.setInsets(new Insets(6));
			lblHelp.setBorder(new Border(1, Color.BLACK, Border.STYLE_SOLID));
			toolTipContainer.add(lblHelp);
			
			Row row = new Row();
			row.setCellSpacing(new Extent(6));
			ctnHelp.add(row);
			
			row.add(btnSave);
			row.add(btnCancel);
			row.add(toolTipContainer);

			
			PDAppInstance.getActivePD().setFocusedComponent(richTextArea);
		} else {			
			add(ctnHtml);
		}
	}

	public void initRichText() {

		richTextArea = new PDRichTextArea();
		//richTextArea.setbInsets(new Insets(6));
		HashMap<String, Boolean> features = new HashMap<>();
		features.put(RichTextArea.FEATURE_MENU, true);
		features.put(RichTextArea.FEATURE_TOOLBAR, true);
		features.put(RichTextArea.FEATURE_UNDO, true);
		features.put(RichTextArea.FEATURE_CLIPBOARD, true);
		//features.put(RichTextArea.FEATURE_BOLD, true);
		//features.put(RichTextArea.FEATURE_BULLETED_LIST, true);
		//features.put(RichTextArea.FEATURE_HORIZONTAL_RULE, true);
		features.put(RichTextArea.FEATURE_HYPERLINK, true);
		//features.put(RichTextArea.FEATURE_INDENT, true);
		//features.put(RichTextArea.FEATURE_ITALIC, true);
		//features.put(RichTextArea.FEATURE_LIST, true);
		features.put(RichTextArea.FEATURE_IMAGE, true);
		//features.put(RichTextArea.FEATURE_NUMBERED_LIST, true);
		//features.put(RichTextArea.FEATURE_PARAGRAPH_STYLE, true);
		//features.put(RichTextArea.FEATURE_STRIKETHROUGH, true);
		//features.put(RichTextArea.FEATURE_TABLE, true);
		features.put(RichTextArea.FEATURE_UNDERLINE, true);
		richTextArea.setFeatures(features);
		// setBackground(editable ? Color.WHITE : new Color(244, 244, 244));
	}

	public void setValue(Object targetObject) {
		this.targetObject = targetObject;
		if (targetObject == null) {
			return;
		}
		String text = null;
		try {
			Method m = targetObject.getClass().getMethod("get" + targetMethod);
			text = (String) m.invoke(targetObject);
		} catch (Exception e) {
			text = e.getMessage();
			e.printStackTrace();
		}
		if (PDUtil.isEmpty(text)) {
			updateLabel(emptyText);
		} else {
			updateLabel(text);
		}
		richTextArea.setText(text);
	}
	
	public void updateValue() {
		String text = richTextArea.getText();
		if ("<p></p>".equalsIgnoreCase(text)) {
			text = "";
		}
		text = text.replaceAll("<br></p>", "</p>");
		text = text.replaceAll("\n", "");
		try {
			//Class<MBase> clazz = ((MBase)targetObject).getStaticClass();
			//targetObject = new DaoFeature().reload(clazz, (MBase)targetObject); 
			
			Method m = targetObject.getClass().getMethod("set" + targetMethod, String.class);
			m.invoke(targetObject, text);
			PDAppInstance.getHibernateSession().update(targetObject);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		updateLabel(text);
	}

	public String getText() {
		return richTextArea.getText();
	}
	
	public void updateLabel(String text) {
		lblHtml.setText(text);
	}
}
