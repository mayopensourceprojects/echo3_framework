/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.EventListener;

import echo3fw.PDAppInstance;
import echopoint.PushButton;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Color;
import nextapp.echo.app.Column;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Row;
import nextapp.echo.app.SplitPane;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.app.event.EventListenerList;
import nextapp.echo.app.layout.SplitPaneLayoutData;

/**
 * An OK/Cancel dialog for general purpose
 * 
 * @author Alejandro Salas 
 * Created on Feb 15, 2007
 */
public abstract class PDOkCancelDialog extends PDWindowPane {

	private EventListenerList eventListenerList = new EventListenerList();
	protected Component pnlMainContainer;
	protected PushButton btnOk;
	protected PushButton btnCancel;
	protected Row rowCommands;
	protected Row rowLeftFooter;

	
	public PDOkCancelDialog(String title, int width, int height, Object... param) {
		setTitle(title);
		setModal(true);
		setWidth(new Extent(width));
		setHeight(new Extent(height));
		initGUI();
		
		Component focusComponent = initGuiCustom(param);
		PDAppInstance.getActivePD().setFocusedComponent(focusComponent);
	}

	public void addActionListener(ActionListener listener) {
		eventListenerList.addListener(ActionListener.class, listener);
	}

	public void addMainComponent(Component component) {
		pnlMainContainer.add(component);
	}

	protected void btnCancelClicked() {
		setVisible(false);
	}

	protected void btnOkClicked() {
		Object newObject = onOkClicked();
		if (newObject == null) return;
		fireActionEvent(new ActionEvent(newObject, null));
		setVisible(false);
	}

	protected abstract Object onOkClicked();
	
	public void fireActionEvent(ActionEvent evt) {
		EventListener[] eventListeners = getOkCancelListener();
		for (int i = 0; i < eventListeners.length; i++) {
			ActionListener listener = (ActionListener) eventListeners[i];
			listener.actionPerformed(evt);
		}
	}

	public EventListener[] getOkCancelListener() {
		return eventListenerList.getListeners(ActionListener.class);
	}

	private void initGUI() {
		
		SplitPane splitFooterAndBody = new SplitPane(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP);
		splitFooterAndBody.setSeparatorPosition(new Extent(28));
		add(splitFooterAndBody);

		SplitPane splitFooterLeftRight = new SplitPane(SplitPane.ORIENTATION_HORIZONTAL_RIGHT_LEFT);
		splitFooterLeftRight.setBackground(Color.WHITE);
		splitFooterLeftRight.setSeparatorPosition(new Extent(300));
		splitFooterAndBody.add(splitFooterLeftRight);
		
		rowCommands = new Row();
		rowCommands.setCellSpacing(new Extent(6));
		rowCommands.setAlignment(new Alignment(Alignment.RIGHT, Alignment.DEFAULT));
		rowCommands.setBackground(Color.WHITE);
		rowCommands.setInsets(new Insets(0, 3, 12, 5));
		SplitPaneLayoutData spld = new SplitPaneLayoutData();
		spld.setOverflow(SplitPaneLayoutData.OVERFLOW_HIDDEN);
		spld.setBackground(Color.WHITE);
		rowCommands.setLayoutData(spld);
		splitFooterLeftRight.add(rowCommands);

		rowLeftFooter = new Row();
		rowLeftFooter.setBackground(Color.WHITE);
		rowLeftFooter.setInsets(new Insets(12, 4, 0, 5));
		splitFooterLeftRight.add(rowLeftFooter);
		

		btnCancel = new PushButton("Cancel");
		btnCancel.addActionListener(e -> btnCancelClicked());
		rowCommands.add(btnCancel);

		btnOk = new PushButton("OK");
		btnOk.setRenderId("btnOK");
		btnOk.addActionListener(e -> btnOkClicked());
		rowCommands.add(btnOk);
		
		pnlMainContainer = getMainContainer();
		if (pnlMainContainer.getLayoutData() == null) {
			spld = new SplitPaneLayoutData();
			spld.setInsets(new Insets(6));
			pnlMainContainer.setLayoutData(spld);
		}
		splitFooterAndBody.add(pnlMainContainer);
	}

	protected abstract Component initGuiCustom(Object... param);
	
	protected Component getMainContainer() {
		Column col = new Column();
		col.setCellSpacing(new Extent(6));
		return col;
	}

	public void removeActionListener(ActionListener listener) {
		eventListenerList.removeListener(ActionListener.class, listener);
	}
}