/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

import echo3fw.PDAppInstance;
import echo3fw.internal.PDTitleBar;
import echo3fw.model.MBase;
import echo3fw.model.MBaseWithTitle;
import echo3fw.table.PDNavigationBar;
import echo3fw.table.PDPageableFactory;
import echo3fw.util.ColorScheme;
import echo3fw.util.HeaderValue;
import echo3fw.util.ICrud;
import echo3fw.util.PDDataGridModel;
import echo3fw.util.PDUtil;
import echo3fw.widgets.PDTextField;
import echopoint.Strut;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Column;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;
import nextapp.echo.app.FillImage;
import nextapp.echo.app.Grid;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.SplitPane;
import nextapp.echo.app.Table;
import nextapp.echo.app.layout.SplitPaneLayoutData;
import nextapp.echo.app.table.TableCellRenderer;

/**
 * This class is a high-level component to display editable data in a convenient
 * way. Features include: - Built-in support to display a pageable list of
 * expandable sections - Very light-way loading of large data sets - Built-in
 * slidable filter - Configurable toolbar
 * 
 * @author Christof May
 */
public abstract class PDMasterDataView<T extends MBaseWithTitle> extends PDWindowPaneSidekick {

	public static enum DISPLAY_MODE {
		SIDE_SCROLL, TOP_SCROLL;
	}

	protected Column colHeaderAndTable;
	protected DISPLAY_MODE displayMode = DISPLAY_MODE.SIDE_SCROLL;
	protected Column headerComponent;
	protected boolean ignoreSliderUpdate = false;
	protected String lastFilterText; // cache the last filter text; this avoids filtering for the same filter text twice
	protected boolean showQuickFilter = false;
	protected SplitPane splitBodyAndFooter;
	protected SplitPane splitListAndDetails;
	protected PDTextField txtQuickSearch;
	protected Table table;
	protected PDDataGridModel tableModel2;
	protected PDPageableFactory tableFactory;
	protected Grid footerGrid;
	protected int tabIndex;
	protected ICrud editor;
	protected Component colHeaderContainer;
	protected MBase currentModel;
	protected SplitPane splitHeaderAndEditor;
	protected SplitPane splitTopRowAndBody;
	protected Row topRow;
	protected PDNavigationBar navigationBar;
	protected int rowsPerPage = 20;
	protected SplitPaneLayoutData layoutDataLeftColumn;	
	protected PDTitleBar titleBar;
	protected Row rowFooterCenter;
	protected Row rowFooterRight;

	
	public PDMasterDataView() {
	}

	public PDMasterDataView(String title, boolean showQuickFilter) {
		this(title, showQuickFilter, DISPLAY_MODE.SIDE_SCROLL, null);
	}

	public PDMasterDataView(String title, boolean showQuickFilter, MBase baseModel) {
		this(title, showQuickFilter, DISPLAY_MODE.SIDE_SCROLL, baseModel);
	}

	public PDMasterDataView(String title, boolean showQuickFilter, DISPLAY_MODE displayMode) {
		this(title, showQuickFilter, displayMode, null);
	}

	public PDMasterDataView(String title, boolean showQuickFilter, DISPLAY_MODE displayMode, Object baseModel) {
		this.displayMode = displayMode;
		assignBaseModel(baseModel);
		init(title, showQuickFilter);
	}

	protected void assignBaseModel(Object baseModel) {		
		//override if applicable
	}

//	public void addFilter(String caption, Component filterComponent) {
//		if (!sidebar.isInitialized()) {
//			ActionListener okListener = new ActionListener() {
//				public void actionPerformed(ActionEvent arg0) {
//					loadData();
//				}
//			};
//			sidebar.setOkListener(okListener);
//			ActionListener resetListener = new ActionListener() {
//				public void actionPerformed(ActionEvent arg0) {
//					resetFilter();
//					loadData();
//				}
//			};
//			sidebar.setResetListener(resetListener);
//		}
//		layoutDataLeftColumn.setInsets(new Insets(24, 6, 6, 6));
//		sidebar.addFilter(caption, filterComponent);
//	}

	protected void addHeaderComponent(Component child) {
		headerComponent.add(child);
	}

	public Button addToolButton(String text, String image, String renderId) {
		return addToolButton(text, image, false, renderId);
	}

	protected Label addToolLabel(String text) {
		Label lbl = new Label(text);
		lbl.setForeground(Color.WHITE);
		getToolbar().add(lbl);
		return lbl;
	}

	protected void initTableModel() {
		tableModel2 = new PDDataGridModel(rowsPerPage);
	}
	
	protected void initTable() {
		initTableModel();
		tableModel2.setFactory(tableFactory);		
		table = new Table(tableModel2);
		table.setBackground(Color.WHITE);
		table.setSelectionEnabled(true);
		table.setSelectionBackground(ColorScheme.LIGHT_BLUE);
		table.setInsets(new Insets(6, 2, 6, 2));
		table.setDefaultRenderer(Object.class, new TableCellRenderer() {
			public Component getTableCellRendererComponent(Table table, Object value, int col, int row) {
				HeaderValue headerValue = (HeaderValue)value;
				if (displayMode == DISPLAY_MODE.SIDE_SCROLL) {
					return tableFactory.getTableCellComponent(headerValue);
				}
				return tableFactory.getHeaderComponent(headerValue);
            }			
		});
		table.addActionListener(e -> readFromModel());
	}

	protected abstract PDPageableFactory getFactory();

	public String getFilterText() {
		if (txtQuickSearch == null)
			return null;
		return txtQuickSearch.getText();
	}

	protected Column getHeaderComponent() {
		return headerComponent;
	}

	protected void init(String title, boolean showQuickFilter) {
		setTitle(title);
		this.showQuickFilter = showQuickFilter;
		initGUI();
		initGUI2();
		loadData();
	}

	protected final void initGUI() {

		splitBodyAndFooter = new SplitPane(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP);
		splitBodyAndFooter.setSeparatorPosition(new Extent(23)); // hight of the footer
		add(splitBodyAndFooter);

		footerGrid = new Grid(3);
		footerGrid.setInsets(new Insets(5));
		footerGrid.setBackground(Color.WHITE);
		footerGrid.setWidth(new Extent(100, Extent.PERCENT));
		SplitPaneLayoutData spld = new SplitPaneLayoutData();
		spld.setOverflow(SplitPaneLayoutData.OVERFLOW_HIDDEN);
		spld.setBackground(Color.WHITE);
		footerGrid.setLayoutData(spld);
		splitBodyAndFooter.add(footerGrid);

		tableFactory = getFactory();
		
		rowsPerPage = tableFactory.getRowsPerPage();
		navigationBar = new PDNavigationBar(rowsPerPage);
		navigationBar.addActionListener(e -> loadData(false));
		footerGrid.add(navigationBar);

		rowFooterCenter = new Row();
		spld = new SplitPaneLayoutData();
		spld.setAlignment(Alignment.ALIGN_CENTER);
		rowFooterCenter.setLayoutData(spld);
		footerGrid.add(rowFooterCenter);

		rowFooterRight = new Row();
		rowFooterRight.setCellSpacing(new Extent(6));
		spld = new SplitPaneLayoutData();
		spld.setAlignment(Alignment.ALIGN_RIGHT);
		rowFooterRight.setLayoutData(spld);
		footerGrid.add(rowFooterRight);

		// left side of the footer bar
		if (showQuickFilter) {
			splitBodyAndFooter.setSeparatorPosition(new Extent(25)); // hight of the footer

			Label lblFilter = new Label("Schnellsuche");
			lblFilter.setForeground(Color.WHITE);
			lblFilter.setFont(PDUtil.getFont(12));
			rowFooterCenter.add(lblFilter);

			txtQuickSearch = new PDTextField(getClass().getSimpleName() + "txtQuickSearch");
			txtQuickSearch.setBackgroundImage(new FillImage(PDUtil.getImage("img/textfield_bg.gif"), null, null, FillImage.NO_REPEAT));
			txtQuickSearch.setBackground(Color.WHITE); //colorScheme.getBackgroundDark());
			txtQuickSearch.setWidth(new Extent(198));
			txtQuickSearch.setHeight(new Extent(18));
			//txtQuickSearch.setBorder(PDUtil.emptyBorder());
			txtQuickSearch.setInsets(new Insets(28, 0, 0, 0));
			// txtQuickSearch.setActionCausedOnChange(true);
			txtQuickSearch.addActionListener(e -> {
				if (getFilterText().equals(lastFilterText)) {
					return;
				}
				loadData();
				lastFilterText = getFilterText();
			});
			rowFooterCenter.add(txtQuickSearch);
		}

		// Header Row
		splitTopRowAndBody = new SplitPane(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM);
		splitTopRowAndBody.setSeparatorPosition(new Extent(0));
		splitTopRowAndBody.add(topRow = new Row());
		splitBodyAndFooter.add(splitTopRowAndBody);

		initTable();
		
		editor = tableFactory.getEditor();
		layoutDataLeftColumn = new SplitPaneLayoutData();
		layoutDataLeftColumn.setOverflow(SplitPaneLayoutData.OVERFLOW_HIDDEN);
		layoutDataLeftColumn.setInsets(new Insets(6, 6, 6, 6));  

		if (displayMode == DISPLAY_MODE.SIDE_SCROLL) {
			colHeaderAndTable = new Column();
			headerComponent = new Column();
			colHeaderAndTable.add(headerComponent);
			colHeaderAndTable.add(table);

			splitListAndDetails = new SplitPane(SplitPane.ORIENTATION_HORIZONTAL_LEFT_RIGHT);
			splitListAndDetails.setSeparatorPosition(new Extent(260));
			splitListAndDetails.setSeparatorColor(Color.DARKGRAY);
			splitListAndDetails.setSeparatorWidth(new Extent(2));
			splitTopRowAndBody.add(splitListAndDetails);

			colHeaderAndTable.setLayoutData(layoutDataLeftColumn);

			splitListAndDetails.add(colHeaderAndTable);

			splitHeaderAndEditor = new SplitPane(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM);
			splitHeaderAndEditor.setSeparatorPosition(new Extent(30));
			colHeaderContainer = new Column();
			splitHeaderAndEditor.add(colHeaderContainer);

			splitListAndDetails.add(splitHeaderAndEditor);			
		} else {
			splitListAndDetails = new SplitPane(SplitPane.ORIENTATION_HORIZONTAL_LEFT_RIGHT);
			splitListAndDetails.setSeparatorPosition(new Extent(0));
			splitListAndDetails.setSeparatorWidth(new Extent(0));
			splitTopRowAndBody.add(splitListAndDetails);

			headerComponent = new Column();
			splitListAndDetails.add(headerComponent);

			splitHeaderAndEditor = new SplitPane(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM);
			splitHeaderAndEditor.setSeparatorPosition(new Extent(244));
			splitHeaderAndEditor.setSeparatorColor(Color.DARKGRAY);
			splitHeaderAndEditor.setSeparatorHeight(new Extent(1));
			splitListAndDetails.add(splitHeaderAndEditor);

			table.setWidth(new Extent(100, Extent.PERCENT));
			table.setLayoutData(layoutDataLeftColumn);
			splitHeaderAndEditor.add(table);
		}
		spld = new SplitPaneLayoutData();
		spld.setInsets(new Insets(6));
		((Component) editor).setLayoutData(spld);
		splitHeaderAndEditor.add((Component) editor);
	}

	protected void initGUI2() {
	}

	protected void loadData() {
		loadData(true);
	}

	/*
	 * Override this method if you do not use byCriteria
	 */
	protected void loadData(boolean resetPosition) {
		tableModel2.reloadData(navigationBar.getCurrentPos());
		tableModel2.fireTableDataChanged();
		table.getSelectionModel().setSelectedIndex(0, true);
		readFromModel();
		if (resetPosition) {
			navigationBar.setCurrentPage(0);
		}
		navigationBar.updateNavigation(tableModel2.getTotalRowCount(), 0);
	}
	
	public int getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(int tabIndex) {
		this.tabIndex = tabIndex;
	}

	public void startEditing() {
		editor.setEditing(true);
		Component c = editor.getFocusComponent();
		if (c != null) {
			PDAppInstance.getActive().setFocusedComponent(c);
		}
		if (titleBar != null) {
			titleBar.setEditing(true);
		}
	}

	public boolean saveEditing() {
		String error = editor.getError();
		if (error != null) {
			PDMessageBox.msgBox("Eingabefehler", error, 300, 150);
			return false;
		}
		editor.applyToModel();
		refreshTitle();
		PDAppInstance.getHibernateSession().update(currentModel);
		editor.setEditing(false);
		return true;
	}

	public void cancelEditing() {
		editor.readFromModel(currentModel);
		editor.setEditing(false);
		//navigationBar.updateNavigationLabel();
	}

	public void readFromModel() {
		final int selectedRow = table.getSelectionModel().getMinSelectedIndex();
		if (colHeaderContainer != null && displayMode == DISPLAY_MODE.SIDE_SCROLL) {
			colHeaderContainer.removeAll();
		}
		if (selectedRow < 0 || tableModel2.getRowCount() == 0) {
			((Component)editor).setVisible(false);
		} else {
			((Component)editor).setVisible(true);
			HeaderValue headerValue = (HeaderValue)tableModel2.getValueAt(0, selectedRow) ;
			currentModel = tableFactory.getModel(headerValue);
			editor.readFromModel(currentModel);
			if (displayMode == DISPLAY_MODE.SIDE_SCROLL) {
				colHeaderContainer.add(new Strut(0, 9));
				titleBar = tableFactory.getHeaderComponent(headerValue);
				colHeaderContainer.add(titleBar);
			}
			editor.setEditing(false);
		}
		navigationBar.updateNavigationLabel(selectedRow);
	}

	public void refreshTitle() {
		if (!(currentModel instanceof MBaseWithTitle)) {
			return;
		}

		((MBaseWithTitle) currentModel).updateCachedTitle();
		int row = table.getSelectionModel().getMinSelectedIndex();
		HeaderValue hv = (HeaderValue) tableModel2.getValueAt(0, row);
		hv.title = ((MBaseWithTitle) currentModel).getCachedTitle();
		tableModel2.fireTableCellUpdated(0, row);
		if (displayMode == DISPLAY_MODE.SIDE_SCROLL) {
			titleBar.setTitle(hv.title);
		}
	}
	
	protected void addNewItem(HeaderValue headerValue) {
		tableModel2.addItem(headerValue);		
		tableModel2.fireTableRowsInserted(0, 1);
		table.getSelectionModel().setSelectedIndex(0, true);
		navigationBar.updateNavigation(tableModel2.getTotalRowCount(), 0);
	}

	protected void addNewItem(MBaseWithTitle item) {
		item.updateCachedTitle();
		Object[] rowData = new Object[2];
		rowData[0] = item.getId();
		rowData[1] = item.getCachedTitle();		
		addNewItem(tableFactory.createHeaderValue(rowData));
	}

	public boolean deleteItem() {
		Session session = PDAppInstance.getHibernateSession();
		try {
			session.delete(currentModel);
			session.flush();
			session.getTransaction().commit();
			loadData();	    
		} catch (Exception e) {
			session.getTransaction().rollback();
			String msg = "Datensatz konnte nicht gelöscht werden. Grund:\n\n";
			if (e instanceof ConstraintViolationException) {
				String err = e.getCause().getMessage();
				err = err.substring(err.indexOf("busstop30") + 12);
				err = err.substring(0, err.indexOf("CONSTRAINT") - 3);
				
				System.out.println(err);
				msg += "Abhängiger Datensatz in Tabelle '" + err + "'";
			} else {
				msg += e.getMessage(); 
			}
			PDMessageBox.msgBox("BusStop", msg, 320, 160);
			return false;
		} finally {
			session.beginTransaction();
		}
		return true;
    }

	public MBase getCurrentModel() {
	    return currentModel;
    }
}