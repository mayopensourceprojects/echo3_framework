/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import echo3fw.PDAppInstance;
import echo3fw.internal.PDFooterButton;
import echo3fw.util.ColorScheme;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Color;
import nextapp.echo.app.Component;
import nextapp.echo.app.ContentPane;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;
import nextapp.echo.app.Row;
import nextapp.echo.app.SplitPane;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;
import nextapp.echo.app.event.EventListenerList;
import nextapp.echo.app.layout.SplitPaneLayoutData;
import nextapp.echo.extras.app.TransitionPane;

/**
 * A wizard-like window, where the user can accomplish tasks step-by-step
 * in a guide way
 * 
 * @author Christof May
 */
public class PDWizard extends PDWindowPane {

	private EventListenerList eventListenerList = new EventListenerList();
	
	protected PDFooterButton btnBack;
	protected PDFooterButton btnNext;
	protected List<IWizardPanel> panels = new ArrayList<IWizardPanel>();
	protected int pageIndex = 0;
	protected ContentPane container;
	protected TransitionPane footerTransitionPane;


	protected void addPanel(IWizardPanel pnl) {
		panels.add(pnl);
	}
	
	protected void initGUI() {
		setWidth(new Extent(650));
		setHeight(new Extent(400));
		setResizable(false);

		SplitPane split = new SplitPane(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP);
		split.setSeparatorPosition(new Extent(24));
		add(split);

		SplitPane footerSplit = new SplitPane(SplitPane.ORIENTATION_HORIZONTAL);
		footerSplit.setSeparatorPosition(new Extent(400));
		footerSplit.setBackground(ColorScheme.GREY);
		split.add(footerSplit);

		
		footerTransitionPane = new TransitionPane();
		footerTransitionPane.setType(TransitionPane.TYPE_CAMERA_PAN_LEFT);		
		footerSplit.add(footerTransitionPane);
		
		Row footerRow = new Row();
		footerRow.setAlignment(new Alignment(Alignment.RIGHT, Alignment.DEFAULT));
		footerRow.setCellSpacing(new Extent(6));
		footerRow.setInsets(new Insets(0, 1, 12, 1));
		SplitPaneLayoutData spld = new SplitPaneLayoutData();
		spld.setOverflow(SplitPaneLayoutData.OVERFLOW_HIDDEN);
		footerRow.setLayoutData(spld);
		footerSplit.add(footerRow);

		btnBack = new PDFooterButton("Zurück");
		btnBack.addActionListener(e -> btnBackClicked());
		footerRow.add(btnBack);

		btnNext = new PDFooterButton("Weiter");
		btnNext.addActionListener(e -> btnNextClicked());
		footerRow.add(btnNext);

		container = new ContentPane();
		container.setInsets(new Insets(6, 9, 3, 6));
		split.add(container);
	}

	protected void btnNextClicked() {
		IWizardPanel page = panels.get(pageIndex);

		if (!page.doNextAction()) {
			return;
		}
		
		page.applyToModel();
		String error = page.getError();
		if (error != null) {
			showError(error);
			return;
		}
		try {
			page.applyToModel2();
		} catch (Exception e) {
			showError(e.toString());
			return;
		}

		while (++pageIndex < panels.size()) {
			showPage();
			return;
		}
		setVisible(false);
		
		// As far as I know the window closing event could be fired with the 'X' icon. Not necessarily means it ended successfully
		fireWindowClosing();
		fireActionEvent(new ActionEvent(this, null));
	}

	protected void btnBackClicked() {
		if (!panels.get(pageIndex).doBackAction()) {
			return;
		}
		
		while (--pageIndex >= 0) {
			if (panels.get(pageIndex).isApplicable()) {
				break;
			}
		}

		// The while loop allows for pageIndex = -1, so we have to reset just in case.
		pageIndex = pageIndex < 0 ? 0 : pageIndex;
		showPage(false);
	}

	protected void showPage() {
		showPage(true);
	}

	protected void showPage(boolean readFromModel) {
		container.removeAll();

		footerTransitionPane.removeAll();
		footerTransitionPane.add(new Label());
		
		IWizardPanel page = panels.get(pageIndex);
		while (!page.isApplicable()){
			page = panels.get(++pageIndex);
		}
			
		container.add((Component)page);

		if (page.getNextCaption() == null) {
			btnNext.setVisible(false);
		} else {
			btnNext.setVisible(true);
			btnNext.setText(page.getNextCaption());
			btnNext.setWidth(new Extent(page.getNextButtonWidth()));
		}
		if (page.getBackCaption() == null) {
			btnBack.setVisible(false);
		} else {
			btnBack.setVisible(true);
			btnBack.setText(page.getBackCaption());
			btnBack.setWidth(new Extent(page.getBackButtonWidth()));
		}

		if (readFromModel) {
			page.readFromModel();
		}
		
		PDAppInstance.getActivePD().setFocusedComponent(page.getFocusComponent());

	}

	public void addActionListener(ActionListener listener) {
		eventListenerList.addListener(ActionListener.class, listener);
	}
	
	public void removeActionListener(ActionListener listener) {
		eventListenerList.removeListener(ActionListener.class, listener);
	}
	
	public EventListener[] getFinishListener() {
		return eventListenerList.getListeners(ActionListener.class);
	}
	
	public void fireActionEvent(ActionEvent evt) {
		EventListener[] eventListeners = getFinishListener();
		for (int i = 0; i < eventListeners.length; i++) {
			ActionListener listener = (ActionListener) eventListeners[i];
			listener.actionPerformed(evt);
		}
	}
	
	public void showError(String error) {
		footerTransitionPane.removeAll();
		Label lblError = new Label("   " + error);
		lblError.setForeground(Color.WHITE);
		lblError.setFormatWhitespace(true);
		footerTransitionPane.add(lblError);
	}
}
