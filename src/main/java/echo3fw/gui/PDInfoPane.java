/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.List;
import java.util.Random;

import echo3fw.util.PDUtil;
import echopoint.ContainerEx;
import echopoint.able.Scrollable;
import nextapp.echo.app.Alignment;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.ImageReference;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Label;

/**
 * A panel which shows some structured information
 * May be used in the background for assisting the user, or in 
 * the context of a slide-show
 * 
 * @author Christof May
 */
public class PDInfoPane extends ContainerEx {

	protected final static String LEFT = "left";
	protected final static String RIGHT = "right";
	protected final static int INSET_RANDOM = 0;
	protected final static int INSET_RANDOM_PLUS_X = 1;
	protected final static int INSET_MINIMUM = 2;

	protected Label lblTitle;
	protected Label lblDescription;
	protected Label lblText;
	protected Label lblAttributes;
	protected Button btnArrow;
	
	public PDInfoPane() {
		this(INSET_RANDOM, false);
	}

	public PDInfoPane(int position) {
		this(position, false);
	}

	public PDInfoPane(int position, boolean scrollableText) {
		initGUI(position, scrollableText);
	}

	private void initGUI(int position, boolean scrollableText) {
		int x = 40;
		int y = 50;
		if (position == INSET_RANDOM) {
			x = 80 + new Random().nextInt(80);
			y += new Random().nextInt(60);
		} else if (position == INSET_RANDOM_PLUS_X) {
			x = 170 + new Random().nextInt(80);
			y += new Random().nextInt(60);
		}

		setInsets(new Insets(x, y, 0, 0));
		lblTitle = new Label();
		//lblTitle.setInsets(new Insets(0, 0, 0, 3));
		lblTitle.setForeground(Color.DARKGRAY);
		lblTitle.setFont(PDUtil.getFontBold(22));
		add(lblTitle);

		lblDescription = new Label();
		//lblDescription.setOutsets(new Insets(0, 6, 0, 0));
		//lblDescription.setInsets(new Insets(3, 3, 0, 3));
		lblDescription.setLineWrap(true);
		//lblDescription.setWidth(new Extent(450));
		lblDescription.setForeground(Color.WHITE);
		lblDescription.setBackground(Color.DARKGRAY);
		lblDescription.setFont(PDUtil.getFontBold(18));
		add(lblDescription);

		lblText = new Label();
		lblText.setTextAlignment(new Alignment(Alignment.LEADING, Alignment.TOP));
		//lblText.setOutsets(new Insets(0, -5, 0, 0));
		//lblText.setInsets(new Insets(0, 0, 0, 0));
		lblText.setLineWrap(true);
		//lblText.setWidth(new Extent(450));
		lblText.setForeground(Color.DARKGRAY);
		lblText.setFont(PDUtil.getFont(16));

		if (scrollableText) {
			//lblText.setOutsets(new Insets(0, 3, 0, 0));
			ContainerEx scrollContainer = new ContainerEx();
			scrollContainer.setScrollBarPolicy(Scrollable.AUTO);
			scrollContainer.setWidth(new Extent(600));
			scrollContainer.setHeight(new Extent(250));
			add(scrollContainer);
			scrollContainer.add(lblText);
		} else {
			add(lblText);
		}

		lblAttributes = new Label();
		//lblAttributes.setInsets(new Insets(0, 0, 0, 0));
		lblAttributes.setLineWrap(true);
		//lblAttributes.setWidth(new Extent(320));
		lblAttributes.setForeground(Color.DARKGRAY);
		lblAttributes.setFont(PDUtil.getFontLight(14));
		add(lblAttributes);
	}

	public void setText(String text) {
		//lblText.setText(PDUtil.getXHTML(text));
	}


	public void setAttributes(List<String> attributes) {
		String s = "<span xmlns:xhtml=\"http://www.w3.org/1999/xhtml\"> <xhtml:p><xhtml:ul>";
		for (String att : attributes) {
			s += "<xhtml:li>" + att + "</xhtml:li>";
		}
		s += "</xhtml:ul></xhtml:p> </span>";
		//XhtmlFragment xhtml = new XhtmlFragment(s);
		//lblAttributes.setText(xhtml);
	}

	protected String getImageTag(String img, String alignment) {
		ImageReference rir = PDUtil.getImage(img);
		Label lbl = new Label(rir);
		//lbl.setWidth(new Extent(0));
		//lbl.setHeight(new Extent(0));
		add(lbl);

		//XXX use ImageManager instead
		String s = "<xhtml:img  align=\"" + alignment
				+ "\" vspace=\"5\" hspace=\"10\" src=\"/Showroom/?serviceId=Echo.StreamImage&amp;imageuid="
				+ rir.getRenderId() + "\" />";
		return s;
	}

	protected void addArrowButton(String caption) {
		btnArrow = new Button(caption);
		btnArrow.setIcon(PDUtil.getImage("orangearrow.png"));
		btnArrow.setTextPosition(new Alignment(Alignment.CENTER, Alignment.BOTTOM));
		btnArrow.setInsets(new Insets(210, 0, 0, 0));
		btnArrow.setForeground(Color.ORANGE);
		btnArrow.setBackground(null);
		btnArrow.setWidth(new Extent(200));
		btnArrow.setFont(PDUtil.getFontBold(14));
		btnArrow.addActionListener(e -> btnArrowClicked());
		add(btnArrow);
	}

	protected void btnArrowClicked() {
		// overwrite if needed
	}

}
