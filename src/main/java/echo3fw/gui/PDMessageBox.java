/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import echo3fw.PDDesktop;
import nextapp.echo.app.Component;
import nextapp.echo.app.Label;

/**
 * A model box displaying a simple message
 * 
 * @author Christof May
 */
public class PDMessageBox extends PDOkCancelDialog {

	public static PDMessageBox confirmDeletion(String msg) {
		PDMessageBox box = new PDMessageBox("LicenseCentral", msg, 300, 165);
		PDDesktop.getDesktop().addWindow(box);
		return box;
	}

	public static PDMessageBox confirmDeletion() {
		String msg = "Do you really want to delete?";
		return confirmDeletion(msg);
	}

	public static void msgBox(String title, String text, int w, int h) {
		try {
			PDMessageBox box = new PDMessageBox(title, text, w, h);		
			box.btnCancel.setVisible(false);
			PDDesktop.getDesktop().addWindow(box);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Label lblText;

	public PDMessageBox(String title, String msg, int w, int h) {
		super(title, w, h);
		lblText.setText(msg);
		setResizable(false);
		setModal(true);
	}
	
	@Override
	protected Component initGuiCustom(Object... param) {
		lblText = new Label();
		lblText.setLineWrap(true);
		addMainComponent(lblText);
		return btnOk;
	}

	@Override
    protected Object onOkClicked() {
	    return true;
    }
}