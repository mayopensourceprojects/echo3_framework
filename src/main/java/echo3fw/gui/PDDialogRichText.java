/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import nextapp.echo.app.Component;
import nextapp.echo.extras.app.RichTextArea;

/**
 * A modal editor for rich text
 * 
 * @author Christof May
 */
public class PDDialogRichText extends PDOkCancelDialog {

	private boolean editable;
	private RichTextArea richTextArea;
	private String newText;

	public PDDialogRichText(String title, boolean editable) {
		super(title, 617, 390);
		this.editable = editable;
	}

	protected Component getMainContainer() {
		richTextArea = new RichTextArea();
		return richTextArea;
	}
	
	public String getText() {
		return newText;
	}

	@Override
	protected Component initGuiCustom(Object... param) {
		if (!editable) {
			btnOk.setVisible(false);
			btnCancel.setText("OK");
		}
		richTextArea.setEnabled(editable);
		return richTextArea;
	}

	public void setText(String text) {
		richTextArea.setText(text);
	}

	@Override
    protected Object onOkClicked() {
		if (editable) {
			newText = richTextArea.getText();
		}
	    return newText;
    }
}