/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.EventListener;

import org.hibernate.Session;

import echo3fw.PDAppInstance;
import echo3fw.PDEventBus;
import echo3fw.model.IOrdered;
import nextapp.echo.app.event.ChangeEvent;
import nextapp.echo.app.event.ChangeListener;
import nextapp.echo.extras.app.Reorder;

public class PDReOrder extends Reorder {

	public PDReOrder() {
		addChangeListener(e -> listReordered(e));
	}
	
	@Override
    public void setOrder(int[] newValue) {
		if (newValue == null) return;
		super.setOrder(newValue);
		for (EventListener cl : getEventListenerList().getListeners(ChangeListener.class)) {
			((ChangeListener)cl).stateChanged(new ChangeEvent(newValue));
		}
    }
	
	private void listReordered(ChangeEvent e) {
		Session session = PDAppInstance.getHibernateSession();
		int[] idx = (int[])e.getSource();
		for (int i = 0; i < idx.length; i++) {
			PDDragableRow row = (PDDragableRow)getComponent(idx[i]);
			IOrdered entity =  row.getEntity();
			if (entity.getPosition() == 0 || entity.getPosition() != i) {
				entity.setPosition(i);
				session.update(entity);
			}
		}
	}
	
	public PDDragableRow<IOrdered> addRow(IOrdered entity) {
		PDDragableRow<IOrdered> row = new PDDragableRow<IOrdered>(entity) {
			@Override
			public void updateValue(IOrdered entity) {
				setName(entity.toString());
			}
			@Override
			protected void onSelection() {
				PDEventBus.getInstance().notifySelection(entity);
			}
		};
		row.setInfo("");
		add(row);
		return row;
	}
}