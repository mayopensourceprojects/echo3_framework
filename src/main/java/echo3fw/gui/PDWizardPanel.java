/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import echo3fw.model.MBase;
import echo3fw.widgets.PDLabel;
import nextapp.echo.app.Column;
import nextapp.echo.app.Component;
import nextapp.echo.app.Extent;

/**
 * A panel which is placed inside the PDWizard 
 * 
 * @author Christof May
 */
public abstract class PDWizardPanel extends Column implements IWizardPanel {

	protected String backLabel;
	protected String nextLabel;
	protected PDLabel lblTitle;
	protected PDLabel lblInfo;

	public PDWizardPanel() {
		this(null, "Back", "Next");
	}

	public PDWizardPanel(String backLabel, String nextLabel) {
		this(null, backLabel, nextLabel);
	}
	
	public PDWizardPanel(String title, String backLabel, String nextLabel) {
		this.backLabel = backLabel;
		this.nextLabel = nextLabel;
		setCellSpacing(new Extent(6));
		lblTitle = new PDLabel(title, PDLabel.HEADER_2);
		add(lblTitle);
		
		lblInfo = new PDLabel(PDLabel.FIELD_VALUE);
		lblInfo.setLineWrap(true);
		lblInfo.setVisible(false);
		add(lblInfo);
	}

	protected void setTitle(String text) {
		lblTitle.setText(text);
	}
	
	protected void setInfo(String text) {
		lblInfo.setText(text);
		lblInfo.setVisible(true);
	}
	
	public String getNextCaption() {
		return nextLabel;
	}

	public String getBackCaption() {
		return backLabel;
	}
	
	public void setEditing(boolean isEditing) {
	}
	
	public Component getFocusComponent() {
		return null;
	}

	public void readFromModel(MBase model) {
		//not used here
	}
}
