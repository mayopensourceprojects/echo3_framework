/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import java.util.List;
import java.util.Vector;

import echo3fw.util.PDUtil;
import echo3fw.widgets.PDTextField;
import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.Border;
import nextapp.echo.app.Button;
import nextapp.echo.app.Color;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Insets;
import nextapp.echo.app.Row;
import nextapp.echo.app.event.ActionEvent;
import nextapp.echo.app.event.ActionListener;

public class PDEditableText extends Row {

	private Button btn;
	private PDTextField txt;
	private List<ActionListener> listeners = new Vector<ActionListener>();
	private Button btnOk;

	public PDEditableText() {
		this(12);
	}

	public PDEditableText(int textsize) {
		setCellSpacing(new Extent(4));
		
		btn = new Button();
		btn.setFont(PDUtil.getFont(textsize));
		btn.addActionListener(e -> startEditing(false));
		btn.setBorder(new Border(1, new Color(182, 182, 182), Border.STYLE_SOLID ));
		btn.setInsets(new Insets(6, 1));
		add(btn);
		
		txt = new PDTextField();
		txt.setFont(PDUtil.getFont(textsize));
		txt.setVisible(false);
		txt.setInsets(new Insets(3));
		txt.addActionListener(e -> finishEditing());
		add(txt);
		
		btnOk = new Button(PDUtil.getImage("img/confirm16.gif"));
		btnOk.setVisible(false);
		btnOk.setInsets(new Insets(0, 0, 6, 0));
		btnOk.addActionListener(e -> finishEditing());
		add(btnOk);
	}

	public void startEditing(boolean reset) {
		txt.setVisible(true);
		int size = txt.getFont().getSize().getValue();
		
		//txt.setWidth(new Extent(40 + 11 * txt.getText().length() * size / 16));
		btn.setVisible(false);
		btnOk.setVisible(true);
		if (reset) {
			txt.setText("");
		}
		ApplicationInstance.getActive().setFocusedComponent(txt);
	}

	private void finishEditing() {
		txt.setVisible(false);
		btn.setVisible(true);
		btnOk.setVisible(false);
		setText(txt.getText());
		ActionEvent ae = new ActionEvent(this, "");
		for (ActionListener l : listeners) {
			l.actionPerformed(ae);					
		}
	}
	
	public void setText(String text) {
		if (PDUtil.isEmpty(text)) {
			btn.setText("...");
		} else {
			btn.setText(text);
		}
		txt.setText(text);
	}

	public void setWidth(int width) {
		btn.setWidth(new Extent(width));
		txt.setWidth(new Extent(width));
	}
	
	public void addActionListener(ActionListener listener) {
		listeners.add(listener);		
	}

	public String getText() {
		return txt.getText();
	}
	
	public PDTextField getTextComponent() {
		return txt;
	}

	public Button getButtonComponent() {
		return btn;
	}

	@Override
    public void setEnabled(boolean newValue) {
        btn.setEnabled(newValue);
    }
}
