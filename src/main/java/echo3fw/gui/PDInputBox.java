/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw.gui;

import echo3fw.PDDesktop;
import nextapp.echo.app.Component;
import nextapp.echo.app.TextField;

/**
 * A model box displaying a simple message
 * 
 * @author Christof May
 */
public class PDInputBox extends PDOkCancelDialog {

	public static PDInputBox inputBox(String title, String msg) {
		PDInputBox box = new PDInputBox(title, msg, 300, 150);		
		PDDesktop.getDesktop().addWindow(box);
		return box;
    }

	private String value;
	private TextField txt;

	public PDInputBox(String title, String msg, int w, int h) {
		super(title, w, h);
		setModal(true);
	}

	@Override
	protected Component initGuiCustom(Object... param) {
		txt = new TextField();
		addMainComponent(txt);
		return txt;
	}		
		
	public void setValue(String value) {
		txt.setText(value);
    }

	public String getValue() {
		return value;
	}

	@Override
    protected Object onOkClicked() {
		value = txt.getText();
	    return value;
    }
}
