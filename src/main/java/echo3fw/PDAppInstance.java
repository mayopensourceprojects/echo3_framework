/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.net.URL;
import java.util.Calendar;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import echo3fw.model.MBrowserSession;
import echo3fw.model.MUser;
import nextapp.echo.app.ApplicationInstance;
import nextapp.echo.app.TaskQueueHandle;
import nextapp.echo.app.Window;
import nextapp.echo.webcontainer.ClientProperties;
import nextapp.echo.webcontainer.ContainerContext;
import nextapp.echo.webcontainer.WebContainerServlet;

public abstract class PDAppInstance extends ApplicationInstance {

	private static Session testSession;  //just for testing
	
	protected transient PDUserSession userSession;
	protected transient Window window;
	protected transient PDDesktop desktop;
	public transient boolean isIE;

	public PDAppInstance() {
	}

	public PDAppInstance(String sessionKey) {
		HttpServletRequest req = WebContainerServlet.getActiveConnection().getRequest();
		//getUserSession().setUser((MUser) req.getSession().getAttribute(PDUserSession.USER_MAIN_PANEL));
	}

	protected abstract PDUserSession instantiateNewUserSession();

	public static PDUserSession getActiveUserSession() {
		PDAppInstance applicationInstance = (PDAppInstance) ApplicationInstance.getActive();
		return applicationInstance.getUserSession();
	}

	public PDUserSession getUserSession() {
		if (userSession == null) {
			userSession = instantiateNewUserSession();
		}
		return userSession;
	}
	
	public static Session getHibernateSession() {
		if (testSession != null) {
			return testSession;
		}
		ContainerContext context = (ContainerContext) PDAppInstance.getActivePD().getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
		PDHibernateFactory factory = (PDHibernateFactory)context.getSession().getAttribute(PDServlet.HIBERNATE_FACTORY);
		return factory.getSession();
	}	

	public static MBrowserSession getBrowserSession() {		
		PDAppInstance active = PDAppInstance.getActivePD();		
		if (active == null) return null;
		ContainerContext context = (ContainerContext)active.getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
		MBrowserSession browserSession = (MBrowserSession)context.getSession().getAttribute(PDServlet.BROWSER_SESSION);
		if (browserSession == null) {
			ClientProperties props = context.getClientProperties();
			browserSession = new MBrowserSession();
			browserSession.setUserAgent((String)props.get(ClientProperties.NAVIGATOR_USER_AGENT));
			browserSession.setRemoteHost((String)props.get(ClientProperties.REMOTE_HOST));
			browserSession.setUtcOffset((Integer)props.get(ClientProperties.UTC_OFFSET));
			browserSession.setScreenWidth((Integer)props.get(ClientProperties.SCREEN_WIDTH));
			browserSession.setScreenHight((Integer)props.get(ClientProperties.SCREEN_HEIGHT));
			browserSession.setStartupTime(Calendar.getInstance());
			getHibernateSession().save(browserSession);
			context.getSession().setAttribute(PDServlet.BROWSER_SESSION, browserSession);
		}
		return browserSession;
	}	

	PDDesktop getDesktop() {
		return desktop;
	}

	protected abstract String getTitle();

	@Override
	public Window init() {
		try {
			ContainerContext ctx = (ContainerContext) getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
			ClientProperties props = ctx.getClientProperties();
			isIE = Boolean.TRUE.equals(props.get(ClientProperties.BROWSER_INTERNET_EXPLORER));

			//getBrowserSession(); //init the session
		} catch (Exception e) {
			e.printStackTrace();
		}
		//userSession.init();
		
		window = new Window();
		window.setTitle(getTitle());
		restart();
		return window;
	}

	public boolean isIE() {
		return isIE;
	}

	public void restart() {
		PDEventBus.getInstance().reset();
		desktop = newDesktop();
		window.setContent(desktop);
	}
	
	public abstract PDDesktop newDesktop();

	public static PDAppInstance getActivePD() {
		return (PDAppInstance)getActive();
	}

	public abstract URL getConfigPath();
	
    public abstract String getAppName();

	public void setTaskHandler(TaskQueueHandle taskQueue, int ms) {
		ContainerContext ctx = (ContainerContext) getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
		ctx.setTaskQueueCallbackInterval(taskQueue, ms);
	}

	public String getCookie(String cookieName) {
		ContainerContext ctx = (ContainerContext)getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
		Cookie[] cookies = ctx.getCookies();
		for (Cookie cookie : cookies) {
			if (cookieName.equals(cookie.getName())) {
				return cookie.getValue();
			}
		}
	    return null;
    }

	public void setCookie(String cookieName, String value) {
		  Cookie userCookie = new Cookie(cookieName, value);
		  ContainerContext ctx = (ContainerContext)getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
		  userCookie.setMaxAge(60 * 60 * 24 * 360);  //expires after 360 days
		  ctx.addCookie(userCookie);
    }

	public static MUser getCurrentUser() {
		return PDAppInstance.getActivePD().getUserSession().getUser();
	}


}