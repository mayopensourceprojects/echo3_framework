/** 
 * The MIT License (MIT)
 * Copyright (c) 2020 Christof May
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package echo3fw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import echo3fw.dao.DaoUser;
import echo3fw.model.MShortcut;
import echo3fw.model.MUser;
import echo3fw.util.PDUtil;
import echo3fw.widgets.PDShortcut;
import echopoint.ContainerEx;
import nextapp.echo.app.Component;
import nextapp.echo.app.ContentPane;
import nextapp.echo.app.Extent;
import nextapp.echo.app.Label;
import nextapp.echo.app.WindowPane;

public class PDDefaultWorkplace extends PDWorkplace {

	private ContentPane contentPane;
	private Label lblRecycleBin;
	private List<WindowPane> windowList;

	public PDDefaultWorkplace() {
		contentPane = new ContentPane();
	}

	@Override
	public void loadItems(ContentPane pane) {
		this.contentPane = pane;

		ContainerEx cntRecycle = new ContainerEx();
		cntRecycle.setTop(new Extent(20));
		cntRecycle.setLeft(new Extent(20));
		cntRecycle.setWidth(new Extent(25));
		cntRecycle.setHeight(new Extent(25));
		contentPane.add(cntRecycle);

		lblRecycleBin = new Label(PDUtil.getImage("img/recycle_bin.gif"));
		cntRecycle.add(lblRecycleBin);

		refreshShortcuts();
		addWindows();
	}

	public void saveWindows() {
		windowList = new ArrayList<WindowPane>();

		Component[] components = contentPane.getComponents();
		for (int i = 0; i < components.length; i++) {
			if (components[i] instanceof WindowPane) {
				windowList.add((WindowPane) components[i]);
			}
		}
	}

	public void addWindows() {
		if (windowList == null) {
			return;
		}

		// Ordering by Z-Index so that all windows appear with exactly the same stacking
		Collections.sort(windowList, new Comparator<WindowPane>() {
			public int compare(WindowPane win1, WindowPane win2) {
				return win1.getZIndex() - win2.getZIndex();
			}
		});
		
		for (WindowPane windowPane : windowList) {
			 PDDesktop.getDesktop().addWindow(windowPane);
		}
		windowList = null;
	}

	public void refreshShortcuts() {
		// remove all shortcuts
		for (Component c : contentPane.getComponents()) {
			if (c instanceof PDShortcut) {
				contentPane.remove(c);
			}
		}

		MUser user = PDAppInstance.getCurrentUser();
		if (user == null) {
			return;
		}

		List<MShortcut> shortcuts = DaoUser.findShortcuts(user);
		for (MShortcut mShortcut : shortcuts) {
			addShortcut(mShortcut);
		}
	}

	public void addShortcut(final MShortcut mShortcut) {
		PDShortcut shortcut = new PDShortcut(mShortcut, "img/shortcut1.gif");
		contentPane.add(shortcut);
	}

	@Override
	public String getName() {
		return "Default";
	}

	@Override
	public String getIcon() {
		return "img/workplaceDefault.png";
	}

	@Override
	public String getSelectedIcon() {
		return "img/workplaceDefaultSelected.png";
	}
}
